﻿using Dapper.Contrib.Extensions;
using System;

namespace StoreApp.DataAccessLayer.Entities.Base
{
    public abstract class BaseEntity
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationData { get; set; }
        public bool IsRemoved { get; set; }
    }
}
