﻿using Dapper.Contrib.Extensions;
using StoreApp.DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.DataAccessLayer.Entities
{
    public class PrintingEdition:BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Enums.Enums.Currency Currency { get; set; }
        public Enums.Enums.ProductType Type { get; set; }
        [Write(false)]
        [NotMapped]
        public List<string> AuthorNames { get; set; }
    }
}
