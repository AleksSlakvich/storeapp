﻿using Dapper.Contrib.Extensions;
using StoreApp.DataAccessLayer.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.DataAccessLayer.Entities
{
    public class AuthorInPrintingEdition : BaseEntity
    {
        [Computed]
        public Author Author { get; set; }
        [ForeignKey("Author")]
        public long AuthorId { get; set; }
        [Computed]
        public PrintingEdition PrintingEdition { get; set; }
        [ForeignKey("PrintingEdition")]
        public long PrintingEditionId { get; set; }
    }
}
