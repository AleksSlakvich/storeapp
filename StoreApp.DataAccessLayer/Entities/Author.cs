﻿using Dapper.Contrib.Extensions;
using StoreApp.DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.DataAccessLayer.Entities
{
    public class Author: BaseEntity
    {
        public string Name { get; set; }

        [NotMapped]
        [Write(false)]
        public List<string> ProductTitles { get; set; }
    }
}
