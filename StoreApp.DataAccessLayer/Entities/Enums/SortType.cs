﻿namespace StoreApp.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum SortType
        {
            ASC = 1,
            DESC = 2
        }
    }
}
