﻿namespace StoreApp.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum OrderStatus
        {
            Paid = 1,
            Unpaid = 2
        }
    }
}
