﻿namespace StoreApp.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum UserStatus
        {
            Active = 1,
            Blocked = 2
        }
    }
}
