﻿namespace StoreApp.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum ProductType
        {
            Book = 1,
            Journal = 2,
            Newspaper = 3
        }
    }
}
