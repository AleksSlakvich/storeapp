﻿namespace StoreApp.DataAccessLayer.Entities.Constants
{
    public partial class Constants
    {
        public class Roles
        {
            public const string Admin = "Admin";
            public const string User = "User";
        }
    }
}
