﻿using StoreApp.DataAccessLayer.Entities.Base;

namespace StoreApp.DataAccessLayer.Entities
{
    public class Payment:BaseEntity
    {
        public string TransactionId { get; set; }
    }
}
