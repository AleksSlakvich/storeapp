﻿using Microsoft.AspNetCore.Identity;
using static StoreApp.DataAccessLayer.Entities.Enums.Enums;

namespace StoreApp.DataAccessLayer.Entities
{
    public class ApplicationUser : IdentityUser<long>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsRemoved { get; set; }
        public UserStatus Status { get; set; }
        public string Image { get; set; }
    }
}
