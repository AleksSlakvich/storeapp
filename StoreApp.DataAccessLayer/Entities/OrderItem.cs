﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.DataAccessLayer.Entities
{
    public class OrderItem:Base.BaseEntity
    {
        public decimal Amount { get; set; }
        public Enums.Enums.Currency Currency { get; set; }
        public long Count { get; set; }
        [Computed]
        public Order Order { get; set; }
        [ForeignKey("Order")]
        public long OrderId { get; set; }
        [Computed]
        public PrintingEdition PrintingEdition { get; set; }
        [ForeignKey("PrintingEdition")]
        public long PrintingEditionId { get; set; }
    }
}
