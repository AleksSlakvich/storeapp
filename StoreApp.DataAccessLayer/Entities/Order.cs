﻿using Dapper.Contrib.Extensions;
using StoreApp.DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.DataAccessLayer.Entities
{
    public class Order : BaseEntity
    {
        public string Description { get; set; }
        public Enums.Enums.OrderStatus Status { get; set; }
        [Computed]
        public ApplicationUser User { get; set; }
        [ForeignKey("User")]
        public long UserId { get; set; }
        [Computed]
        public Payment Payment { get; set; }
        [ForeignKey("Payment")]
        public long PaymentId { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public decimal OrderAmount { get; set; }
    }
}
