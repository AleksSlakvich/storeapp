﻿using Microsoft.AspNetCore.Identity;
using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Constants;
using StoreApp.DataAccessLayer.Entities.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Initialization
{
    public class DataBaseInitialization
    {
        private ApplicationContext _context;

        public DataBaseInitialization(ApplicationContext context)
        {
            _context = context;
        }
        public async Task<bool> SeedData(UserManager<ApplicationUser> userManager)
        {
            var result = false;
            await SeedUsers(userManager);
            await SeedAuthors();
            await SeedPrintingEditions();
            await SeedAuthorInPrintingEditions();
            result = true;

            return result;
        }

        public async Task SeedUsers(UserManager<ApplicationUser> userManager)
        {
            string[] roles = new string[] { Constants.Roles.Admin, Constants.Roles.User };
            foreach (string role in roles)
            {
                if (!_context.Roles.Any(r => r.Name == role))
                {
                    _context.Roles.Add(new IdentityRole<long>() { Name = role, NormalizedName = role });
                }
            }

            if (userManager.FindByNameAsync("user1").Result is null)
            {
                var user1 = new ApplicationUser();
                user1.UserName = "user1";
                user1.Email = "user1@localhost";
                user1.FirstName = "Nancy";
                user1.LastName = "Davolio";
                user1.Status = Enums.UserStatus.Active;
                await userManager.CreateAsync(user1, "SeCur3_Password");
                await userManager.AddToRoleAsync(user1, Constants.Roles.User);
                user1.IsRemoved = false;
            }

            if (userManager.FindByNameAsync("user2").Result is null)
            {
                var user4 = new ApplicationUser();
                user4.UserName = "Admin777";
                user4.Email = "admin888555@localhost";
                user4.FirstName = "Maks";
                user4.LastName = "Korsian";
                user4.Status = Enums.UserStatus.Active;
                await userManager.CreateAsync(user4, "SeCuRiTy_Pa$$W0rd777888");
                await userManager.AddToRoleAsync(user4, Constants.Roles.Admin);
                user4.IsRemoved = false;
            }

            await _context.SaveChangesAsync();
        }

        public async Task SeedAuthors()
        {
            if (!_context.Authors.Where(author => author.Name == "Donald Trump").Any())
            {
                _context.Authors.Add(new Author
                {
                    Name = "Donald Trump",
                    CreationData = DateTime.Now
                });
            }

            if (!_context.Authors.Where(author => author.Name == "Aleksandr Pushkin").Any())
            {
                _context.Authors.Add(new Author
                {
                    Name = "Aleksandr Pushkin",
                    CreationData = DateTime.Now
                });
            }

            await _context.SaveChangesAsync();
        }

        public async Task SeedPrintingEditions()
        {
            if (!_context.PrintingEditions.Where(printingEdition => printingEdition.Title == "E.Onegin").Any())
            {
                _context.PrintingEditions.Add(new PrintingEdition
                {
                    Title = "E.Onegin",
                    CreationData = DateTime.Now,
                    Price = 100,
                    Type = Enums.ProductType.Book,
                    Currency = Enums.Currency.CHF,
                    Description = "First Book"
                });
            }

            if (!_context.PrintingEditions.Where(printingEdition => printingEdition.Title == "Super Business").Any())
            {
                _context.PrintingEditions.Add(new PrintingEdition
                {
                    Title = "Super Business",
                    CreationData = DateTime.Now,
                    Price = 235,
                    Type = Enums.ProductType.Journal,
                    Currency = Enums.Currency.JPY,
                    Description = "Second Book"
                });
            }

            await _context.SaveChangesAsync();
        }

        public async Task SeedAuthorInPrintingEditions()
        {
            var author = _context.Authors.Where(item => item.Name == "Aleksandr Pushkin")
                                         .FirstOrDefault();
            var book = _context.PrintingEditions.Where(item => item.Title == "E.Onegin")
                                                .FirstOrDefault();

            _context.AuthorInPrintingEditions.Add(new AuthorInPrintingEdition
            {
                CreationData = DateTime.Now,
                Author = author,
                PrintingEdition = book,
                IsRemoved = false
            });

            author = _context.Authors.Where(item => item.Name == "Donald Trump")
                                     .FirstOrDefault();
            book = _context.PrintingEditions.Where(item => item.Title == "Super Business")
                                            .FirstOrDefault();

            _context.AuthorInPrintingEditions.Add(new AuthorInPrintingEdition
            {
                CreationData = DateTime.Now,
                Author = author,
                PrintingEdition = book,
                IsRemoved = false
            });

            await _context.SaveChangesAsync();
        }
    }
}
