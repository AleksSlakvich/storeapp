﻿using Microsoft.Extensions.DependencyInjection;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.DataAccessLayer.Repositories.EFRepositories;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using dapperAuthorRepository = StoreApp.DataAccessLayer.Repositories.DapperRepositories.AuthorRepository;
using dapperAuthorInPrintingEditionRepository = StoreApp.DataAccessLayer.Repositories.DapperRepositories.AuthorInPrintingEditionRepository;
using dapperPrintingEditionRepository = StoreApp.DataAccessLayer.Repositories.DapperRepositories.PrintingEditionRepository;
using dapperOrderItemRepository = StoreApp.DataAccessLayer.Repositories.DapperRepositories.OrderItemRepository;
using dapperOrderRepository = StoreApp.DataAccessLayer.Repositories.DapperRepositories.OrderRepository;
using dapperPaymentRepository = StoreApp.DataAccessLayer.Repositories.DapperRepositories.PaymentRepository;

namespace StoreApp.DataAccessLayer.Initialization
{
    public class RepositoryInitializer
    {
        public static void InitializeEF(IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddTransient<IAuthorInPrintingEditionRepository, AuthorInPrintingEditionRepository>();
            services.AddTransient<IPrintingEditionRepository, PrintingEditionRepository>();
            services.AddTransient<IOrderItemRepository, OrderItemRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
        }

        public static void InitializeDapper(IServiceCollection services)
        {
            services.AddSingleton<IAuthorRepository, dapperAuthorRepository>();
            services.AddSingleton<IAuthorInPrintingEditionRepository, dapperAuthorInPrintingEditionRepository>();
            services.AddSingleton<IPrintingEditionRepository, dapperPrintingEditionRepository>();
            services.AddSingleton<IOrderItemRepository, dapperOrderItemRepository>();
            services.AddSingleton<IOrderRepository, dapperOrderRepository>();
            services.AddSingleton<IPaymentRepository, dapperPaymentRepository>();
        }
    }
}
