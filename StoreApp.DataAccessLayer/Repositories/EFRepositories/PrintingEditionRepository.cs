﻿using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;

namespace StoreApp.DataAccessLayer.Repositories.EFRepositories
{
    public class PrintingEditionRepository : BaseEFRepository<PrintingEdition>, IPrintingEditionRepository
    {
        public PrintingEditionRepository(ApplicationContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

    }
}
