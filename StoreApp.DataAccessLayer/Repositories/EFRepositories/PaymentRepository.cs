﻿using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;

namespace StoreApp.DataAccessLayer.Repositories.EFRepositories
{
    public class PaymentRepository: BaseEFRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(ApplicationContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }
    }
}
