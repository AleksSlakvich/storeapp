﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Extensions;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.EFRepositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserRepository(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }
        public async Task<IdentityResult> CreateAsync(ApplicationUser user, string password)
        {
            return await _userManager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> DeleteAsync(ApplicationUser user)
        {
            return await _userManager.DeleteAsync(user);
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            return await _userManager.UpdateAsync(user);
        }

        public async Task<ApplicationUser> FindByIdAsync(long userid)
        {
            var user = await _userManager.FindByIdAsync(userid.ToString());
            return user;
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            return user;
        }

        public async Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldpass, string newpass)
        {
            return await _userManager.ChangePasswordAsync(user, oldpass, newpass);
        }

        public async Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task <string> GetRoleAsync(ApplicationUser user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);

            return userRoles.ToList().FirstOrDefault();
        }

        public async Task SignInAsync(ApplicationUser user)
        {
            await _signInManager.SignInAsync(user, isPersistent: false);
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<SignInResult> CheckPasswordSignInAsync(ApplicationUser user, string password)
        {
            var result = await _signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure: false);
            return result;
        }

        public async Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string token, string newpassword)
        {
            return await _userManager.ResetPasswordAsync(user, token, newpassword);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task PasswordSignInAsync(ApplicationUser user, string password)
        {
            await _signInManager.PasswordSignInAsync(user, password, isPersistent: false, lockoutOnFailure: false);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string token)
        {
            var result = await _userManager.ConfirmEmailAsync(user, token);
            return result;
        }

        public async Task<bool> IsEmailConfirmedAsync(ApplicationUser user)
        {
            return await _userManager.IsEmailConfirmedAsync(user);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(ApplicationUser user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<IdentityResult> SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset time)
        {
            user.Status = Enums.UserStatus.Blocked;
            return await _userManager.SetLockoutEndDateAsync(user, time);
        }

        public async Task<ResponseModel<List<ApplicationUser>>> GetUsersAsync(FilterUserModel filterUserModel)
        {
            var allUsers = _userManager.Users.Where(user => !user.IsRemoved).Where(user => !user.UserName.Contains("Admin")).AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterUserModel.SearchFilter))
            {
                var serchString = filterUserModel.SearchFilter.ToLower();
                allUsers = allUsers.Where(s => s.FirstName.ToLower().Contains(serchString) ||
                                          s.LastName.ToLower().Contains(serchString) ||
                                          s.Email.ToLower().Contains(serchString));
            }

            if (filterUserModel.UserStatusSort[0] == Enums.UserStatus.Active && filterUserModel.UserStatusSort.Count == 1)
            {
                allUsers = allUsers.Where(s => s.LockoutEnd <= DateTime.UtcNow || s.LockoutEnd == null);
            }
            if (filterUserModel.UserStatusSort[0] == Enums.UserStatus.Blocked && filterUserModel.UserStatusSort.Count == 1)
            {
                allUsers = allUsers.Where(s => s.LockoutEnd > DateTime.UtcNow);
            }
            if (filterUserModel.UserStatusSort.Count > 1)
            {
                allUsers = allUsers.OrderByDescending(s => s.Id);
            }
            allUsers = FilterSortExtension<ApplicationUser>.FilterSort(allUsers, filterUserModel.SortType, filterUserModel.SortColumn);

            var count = await allUsers.CountAsync();
            var index = (filterUserModel.PageIndex - 1) * filterUserModel.PageSize;
            var items = allUsers.Skip((filterUserModel.PageIndex - 1) * filterUserModel.PageSize < 0 ? 0 : index)
                                          .Take(filterUserModel.PageSize).ToList();

            return new ResponseModel<List<ApplicationUser>>(items, count);
        }
    }
}
