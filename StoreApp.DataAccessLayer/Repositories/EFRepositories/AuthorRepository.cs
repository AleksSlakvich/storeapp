﻿using Microsoft.EntityFrameworkCore;
using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorRepository : BaseEFRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(ApplicationContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<List<Author>> GetAllAuthorsAsync()
        {
            var allAuthors = _dbContext.Authors.Where(author => !author.IsRemoved).ToListAsync();
            return await allAuthors;
        }
    }
}
