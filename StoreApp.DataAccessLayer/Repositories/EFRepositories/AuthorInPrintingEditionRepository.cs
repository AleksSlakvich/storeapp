﻿using Microsoft.EntityFrameworkCore;
using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Extensions;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorInPrintingEditionRepository : BaseEFRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository
    {
        public AuthorInPrintingEditionRepository(ApplicationContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<IEnumerable<AuthorInPrintingEdition>> GetByAuthorIdAsync(long id)
        {
            var result = await _dbContext.Set<AuthorInPrintingEdition>().Where(x => x.Author.Id == id).ToListAsync();
            return result;
        }

        public async Task<IEnumerable<AuthorInPrintingEdition>> GetByPrintingEditionIdAsync(long id)
        {
            var result = await _dbContext.Set<AuthorInPrintingEdition>().Where(x => x.PrintingEdition.Id == id).Include(item => item.Author).ToListAsync();
            return result;
        }

        public async Task<ResponseModel<List<Author>>> GetAuthorsAsync(FilterAuthorModel filterAuthorModel)
        {
            var notGroupedAuthors = from author in _dbContext.Authors
                           join authorInProduct in _dbContext.AuthorInPrintingEditions on author.Id equals authorInProduct.AuthorId
                           into authorInProducts
                           from authorInPE in authorInProducts.DefaultIfEmpty()
                           select new
                           {
                               AuthorId = author.Id,
                               Name = author.Name,
                               ProductTitles = authorInPE.PrintingEdition.Title
                           };

            var authors = notGroupedAuthors.GroupBy(auth => auth.AuthorId).Select(author => new Author
            {
                Id = author.Key,
                Name = author.Select(element => element.Name).FirstOrDefault(),
                ProductTitles = notGroupedAuthors.Where(auth => auth.AuthorId == author.Key).Select(element => element.ProductTitles).ToList()
            });

            if (!string.IsNullOrWhiteSpace(filterAuthorModel.SearchFilter))
            {
                var searchString = filterAuthorModel.SearchFilter.ToLower();
                authors = authors.Where(item => item.Name.ToLower().Contains(searchString));
            }

            authors = FilterSortExtension<Author>.FilterSort(authors, filterAuthorModel.SortType, filterAuthorModel.SortColumn);
            var count = await authors.CountAsync();
            var index = (filterAuthorModel.PageIndex - 1) * filterAuthorModel.PageSize;
            var items = authors.Skip(
                    (filterAuthorModel.PageIndex - 1) * filterAuthorModel.PageSize < 0 ? 0 : index)
                    .Take(filterAuthorModel.PageSize).ToList();
            return new ResponseModel<List<Author>>(items, count);
        }

        public async Task<ResponseModel<List<PrintingEdition>>> GetPrintingEditionsAsync(FilterPrintingEditionModel filterPrintingEditionModel)
        {
            var allAuthorsInProduct = _dbContext.AuthorInPrintingEditions.Where(author => !author.IsRemoved)
                                                                         .Include(item => item.Author)
                                                                         .Include(item => item.PrintingEdition)
                                                                         .AsQueryable();

            var products = allAuthorsInProduct.GroupBy(printEdit => printEdit.PrintingEdition.Id)
                                              .Select(printEdit => new PrintingEdition
                                              {
                                                  Id = printEdit.Key,
                                                  Title = printEdit.Select(element => element.PrintingEdition.Title).FirstOrDefault(),
                                                  Description = printEdit.Select(element => element.PrintingEdition.Description).FirstOrDefault(),
                                                  Currency = printEdit.Select(element => element.PrintingEdition.Currency).FirstOrDefault(),
                                                  Type = printEdit.Select(element => element.PrintingEdition.Type).FirstOrDefault(),
                                                  Price = printEdit.Select(element => element.PrintingEdition.Price).FirstOrDefault(),
                                                  AuthorNames = printEdit.Select(element => element.Author.Name).ToList()
                                              });

            if (!string.IsNullOrWhiteSpace(filterPrintingEditionModel.SearchFilter))
            {
                var searcString = filterPrintingEditionModel.SearchFilter.ToLower();
                products = products.Where(s => s.Title.ToLower().Contains(searcString));
            }

            products = FilterSortExtension<PrintingEdition>.FilterSort(products, filterPrintingEditionModel.SortType, filterPrintingEditionModel.SortColumn);

            var sortTypes = new List<Enums.ProductType>();

            foreach (Enums.ProductType type in (Enums.ProductType[])Enum.GetValues(typeof(Enums.ProductType)))
            {
                sortTypes.Add(type);
            }

            var resultCollection = sortTypes.Except(filterPrintingEditionModel.PrintingEditionCategoriesSort);

            foreach (var type in resultCollection)
            {
                products = products.Where(s => s.Type != type);
            }

            products = products.Where(s => s.Price >= filterPrintingEditionModel.PrintingEditionMinPrice &&
                                                            s.Price <= filterPrintingEditionModel.PrintingEditionMaxPrice);
            var count = await products.CountAsync();
            var items = products.Skip((filterPrintingEditionModel.PageIndex - 1) * filterPrintingEditionModel.PageSize).
                                 Take(filterPrintingEditionModel.PageSize).ToList();

            return new ResponseModel<List<PrintingEdition>>(items, count);
        }
    }
}
