﻿using Microsoft.EntityFrameworkCore;
using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Extensions;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.EFRepositories
{
    public class OrderRepository : BaseEFRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<ResponseModel<List<Order>>> GetOrdersAsync(FilterOrderModel filterOrderModel)
        {
            var allOrders = _dbContext.Orders.Where(order => !order.IsRemoved)
                                .Include(item => item.Payment)
                                .Include(p => p.User)
                                .Include(item => item.OrderItems)
                                .ThenInclude(item => item.PrintingEdition).AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterOrderModel.SearchFilter))
            {
                allOrders = allOrders.Where(s => s.User.FirstName.ToLower().Contains(filterOrderModel.SearchFilter.ToLower()) ||
                                            s.User.LastName.ToLower().Contains(filterOrderModel.SearchFilter.ToLower()) ||
                                            s.User.Email.ToLower().Contains(filterOrderModel.SearchFilter.ToLower()));
            }

            if (filterOrderModel.OrderStatusSort[0] == Enums.OrderStatus.Paid && filterOrderModel.OrderStatusSort.Count == 1)
            {
                allOrders = allOrders.Where(s => s.Status == Enums.OrderStatus.Paid);
            }
            if (filterOrderModel.OrderStatusSort[0] == Enums.OrderStatus.Unpaid && filterOrderModel.OrderStatusSort.Count == 1)
            {
                allOrders = allOrders.Where(s => s.Status == Enums.OrderStatus.Unpaid);
            }
            if (filterOrderModel.OrderStatusSort.Count > 1)
            {
                allOrders = allOrders.OrderByDescending(s => s.Id);
            }
            if (filterOrderModel.SortColumn != Enums.ColumnType.Email && filterOrderModel.SortColumn != Enums.ColumnType.LastName)
            {
                allOrders = FilterSortExtension<Order>.FilterSort(allOrders, filterOrderModel.SortType, filterOrderModel.SortColumn);
            }
            if (filterOrderModel.SortType == Enums.SortType.ASC && (filterOrderModel.SortColumn == Enums.ColumnType.Email ||
                filterOrderModel.SortColumn == Enums.ColumnType.LastName))
            {
                allOrders = allOrders.OrderBy(s => s.User.GetType().GetProperty(filterOrderModel.SortColumn.ToString()).GetValue(s.User));
            }
            if (filterOrderModel.SortType == Enums.SortType.DESC && (filterOrderModel.SortColumn == Enums.ColumnType.Email ||
                filterOrderModel.SortColumn == Enums.ColumnType.LastName))
            {
                allOrders = allOrders.OrderByDescending(s => s.User.GetType().GetProperty(filterOrderModel.SortColumn.ToString()).GetValue(s.User));
            }

            var count = await allOrders.CountAsync();
            var index = (filterOrderModel.PageIndex - 1) * filterOrderModel.PageSize;
            var items = allOrders.Skip((filterOrderModel.PageIndex - 1) * filterOrderModel.PageSize < 0 ? 0 : index).
                        Take(filterOrderModel.PageSize).ToList();

            return new ResponseModel<List<Order>>(items, count);
        }
    }
}
