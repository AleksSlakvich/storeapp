﻿using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using StoreApp.DataAccessLayer.Repositories.Interfaces.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderRepository : IBaseEFRepository<Order>
    {
        Task<ResponseModel<List<Order>>> GetOrdersAsync(FilterOrderModel filterOrderModel);
    }
}
