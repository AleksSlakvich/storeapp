﻿using Microsoft.AspNetCore.Identity;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IUserRepository
    {
        Task<IdentityResult> CreateAsync(ApplicationUser user, string password);
        Task<IdentityResult> DeleteAsync(ApplicationUser user);
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
        Task<ApplicationUser> FindByIdAsync(long userid);
        Task<ApplicationUser> FindByEmailAsync(string email);
        Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldpass, string newpass);
        Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role);
        Task<string> GetRoleAsync(ApplicationUser user);
        Task SignInAsync(ApplicationUser user);
        Task SignOutAsync();
        Task<SignInResult> CheckPasswordSignInAsync(ApplicationUser user, string password);
        Task PasswordSignInAsync(ApplicationUser user, string password);
        Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string token);
        Task<bool> IsEmailConfirmedAsync(ApplicationUser user);
        Task<string> GenerateEmailConfirmationTokenAsync(ApplicationUser user);
        Task<ResponseModel<List<ApplicationUser>>> GetUsersAsync(FilterUserModel filterUserModel);
        Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string token, string newpassword);
        Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user);
        Task<IdentityResult> SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset time);
    }
}
