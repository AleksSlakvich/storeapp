﻿using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.Interfaces.Base
{
    public interface IBaseEFRepository<T>
    {
        Task<T> GetByIdAsync(long id);
        Task<bool> CreateAsync(T item);
        Task<bool> UpdateAsync(T item);
        Task<bool> DeleteAsync(T item);
        Task<bool> RemoveAsync(T item);
    }
}
