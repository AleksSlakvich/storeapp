﻿using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.Interfaces.Base
{
    public interface IBaseDapperRepository<T>
    {
        Task<bool> RemoveAsync(T item);
        Task<T> GetByIdAsync(long id);
        Task<bool> CreateAsync(T item);
        Task<bool> UpdateAsync(T item);
        Task<bool> DeleteAsync(T item);
    }
}
