﻿using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Interfaces.Base;

namespace StoreApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IPrintingEditionRepository : IBaseEFRepository<PrintingEdition>
    {

    }
}
