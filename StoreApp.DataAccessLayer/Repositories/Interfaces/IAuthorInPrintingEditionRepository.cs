﻿using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using StoreApp.DataAccessLayer.Repositories.Interfaces.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorInPrintingEditionRepository: IBaseEFRepository<AuthorInPrintingEdition>
    {
        Task<IEnumerable<AuthorInPrintingEdition>> GetByAuthorIdAsync(long id);
        Task<IEnumerable<AuthorInPrintingEdition>> GetByPrintingEditionIdAsync(long id);
        Task<ResponseModel<List<Author>>> GetAuthorsAsync(FilterAuthorModel filterAuthorModel);
        Task<ResponseModel<List<PrintingEdition>>> GetPrintingEditionsAsync(FilterPrintingEditionModel filterPrintingEditionModel);
    }
}
