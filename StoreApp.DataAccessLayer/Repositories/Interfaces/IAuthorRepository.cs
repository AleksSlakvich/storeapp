﻿using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Interfaces.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorRepository: IBaseEFRepository<Author>
    {
        Task<List<Author>> GetAllAuthorsAsync();
    }
}
