﻿using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using StoreApp.DataAccessLayer.Entities.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces.Base;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.Base
{
    public class BaseDapperRepository<TEntity> : IBaseDapperRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly IConfiguration _configuration;
        private string _connectionString;

        public BaseDapperRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SqlConnection SqlConnection()
        {
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
            return new SqlConnection(_connectionString);
        }

        public IDbConnection CreateConnection()
        {
            var connection = SqlConnection();
            connection.Open();
            return connection;
        }

        public async Task<bool> RemoveAsync(TEntity item)
        {
            var result = false;
            using (var connection = CreateConnection())
            {
                result = await connection.DeleteAsync(item);
            }

            return result;
        }

        public async Task<TEntity> GetByIdAsync(long id)
        {
            using (var connection = CreateConnection())
            {
                var result = await connection.GetAsync<TEntity>(id);
                return result;
            }
        }

        public async Task<bool> CreateAsync(TEntity item)
        {
            var result = 0;
            using (var connection = CreateConnection())
            {
                try
                {
                    result = await connection.InsertAsync(item);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            if (result == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateAsync(TEntity item)
        {
            var result = false;

            using (var connection = CreateConnection())
            {
                result = await connection.UpdateAsync(item);
            }
            return result;
        }

        public async Task<bool> DeleteAsync(TEntity item)
        {
            var result = false;
            item.IsRemoved = true;
            using (var connection = CreateConnection())
            {
                result = await connection.UpdateAsync(item);
            }
            return result;
        }
    }
}
