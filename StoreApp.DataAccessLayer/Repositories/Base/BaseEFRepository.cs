﻿using Microsoft.EntityFrameworkCore;
using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces.Base;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.Base
{
    public class BaseEFRepository<TEntity> : IBaseEFRepository<TEntity>
        where TEntity : BaseEntity
    {
        protected ApplicationContext _dbContext;

        public BaseEFRepository(ApplicationContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<TEntity> GetByIdAsync(long id)
        {
            var result = await _dbContext.Set<TEntity>().FindAsync(id);
            return result;
        }

        public async Task<bool> CreateAsync(TEntity item)
        {
            await _dbContext.Set<TEntity>().AddAsync(item);
            var result = await _dbContext.SaveChangesAsync();
            if (result == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateAsync(TEntity item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
            var result = await _dbContext.SaveChangesAsync();
            if (result == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> DeleteAsync(TEntity item)
        {
            _dbContext.Entry(item).Entity.IsRemoved = true;
            var result = await _dbContext.SaveChangesAsync();
            if (result == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> RemoveAsync(TEntity item)
        {
            _dbContext.Set<TEntity>().Remove(item);
            var result = await _dbContext.SaveChangesAsync();
            if (result == 0)
            {
                return false;
            }
            return true;
        }
    }
}
