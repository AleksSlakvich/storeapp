﻿using Dapper;
using Microsoft.Extensions.Configuration;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class OrderRepository : BaseDapperRepository<Order>, IOrderRepository
    {
        public OrderRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<ResponseModel<List<Order>>> GetOrdersAsync(FilterOrderModel filterOrderModel)
        {
            var allOrders = new List<Order>();
            var count = 0;
            var totalQuery = new StringBuilder();

            var queryAllOrderItems = new StringBuilder(
                 $@"SELECT [oi].[Id], [oi].[Count], 
                   [oi].[Currency], [oi].[OrderId], [oi].[PrintingEditionId], 
                   [pe].[Id], 
                   [pe].[Title], [pe].[Type], 
                   [o].[Id], [o].[CreationData], OrderAmount, Status,
                   userId, FirstName, LastName, Email  
                   FROM[OrderItems] AS [oi] 
                        INNER JOIN [PrintingEditions] AS [pe] ON [oi].[PrintingEditionId] = [pe].[Id] 
                        INNER JOIN (
                                    SELECT DISTINCT [t].* 
                                    FROM (
                                            SELECT [o0].[Id], [o0].[OrderAmount], [o0].[Status], [o0].[CreationData],
                                            [u].[Id] as userId, [u].[FirstName], [u].[LastName], [u].[Email] 
                                            FROM [Orders] AS [o0] 
                                                INNER JOIN [Payments] AS [p] ON [o0].[PaymentId] = [p].[Id] 
                                                INNER JOIN [AspNetUsers] AS [u] ON [o0].[UserId] = [u].[Id] 
                                            WHERE ([o0].[IsRemoved] = 0) ");

            var filterQuery = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(filterOrderModel.SearchFilter))
            {
                string searchString = filterOrderModel.SearchFilter;

                filterQuery.Append($"AND ((((CHARINDEX('{searchString}', LOWER([u].[FirstName])) > 0) OR ('{searchString}' = N'')) OR" +
                    $" ((CHARINDEX('{searchString}', LOWER([u].[LastName])) > 0) OR ('{searchString}' = N'')))" +
                    $" OR ((CHARINDEX('{searchString}', LOWER([u].[Email])) > 0) OR ('{searchString}' = N''))) ");
            }
            if (filterOrderModel.OrderStatusSort[0] == Enums.OrderStatus.Paid && filterOrderModel.OrderStatusSort.Count == 1)
            {
                filterQuery.Append($"AND ([o0].[Status] = 1) ");
            }
            if (filterOrderModel.OrderStatusSort[0] == Enums.OrderStatus.Unpaid && filterOrderModel.OrderStatusSort.Count == 1)
            {
                filterQuery.Append($"AND ([o0].[Status] = 0) ");
            }
            queryAllOrderItems.Append(filterQuery);
            queryAllOrderItems.Append($"ORDER BY ");
            var endQueryAllOrderItems = new StringBuilder();
            endQueryAllOrderItems.Append($"ORDER BY ");

            if (filterOrderModel.OrderStatusSort.Count > 1)
            {
                queryAllOrderItems.Append($"");
            }

            if (filterOrderModel.SortColumn != Enums.ColumnType.LastName && filterOrderModel.SortColumn != Enums.ColumnType.Email)
            {
                queryAllOrderItems.Append($"[o0].[{filterOrderModel.SortColumn.ToString()}] {filterOrderModel.SortType.ToString()} ");
                endQueryAllOrderItems.Append($"[o].[{filterOrderModel.SortColumn.ToString()}] {filterOrderModel.SortType.ToString()} ");
            }

            if (filterOrderModel.SortColumn == Enums.ColumnType.LastName || filterOrderModel.SortColumn == Enums.ColumnType.Email)
            {
                queryAllOrderItems.Append($"[u].[{filterOrderModel.SortColumn.ToString()}] {filterOrderModel.SortType.ToString()}, [o0].[Id] ");
                endQueryAllOrderItems.Append($"[o].[{filterOrderModel.SortColumn.ToString()}] {filterOrderModel.SortType.ToString()}, [o].[Id] ");
            }

            if (filterOrderModel.PageIndex == 0)
            {
                filterOrderModel.PageIndex = 1;
            }
            var skipNumber = (filterOrderModel.PageIndex - 1) * filterOrderModel.PageSize;

            var pageSize = filterOrderModel.PageSize;

            queryAllOrderItems.Append($"OFFSET {skipNumber} ROWS FETCH NEXT {pageSize} ROWS ONLY" +
                $" ) AS [t] " +
                $") AS [o] ON [oi].[OrderId] = [o].[Id] ");
            queryAllOrderItems.Append(endQueryAllOrderItems);

            var queryCount = new StringBuilder();
            queryCount.Append(" SELECT COUNT(DISTINCT o0.Id) FROM Orders as o0 " +
                             "INNER JOIN [AspNetUsers] AS [u] ON o0.UserId = [u].[Id] " +
                             "WHERE o0.IsRemoved = 0 ");
            queryCount.Append(filterQuery);
            totalQuery = totalQuery.Append(queryAllOrderItems);
            totalQuery.Append(queryCount);

            using (var connection = CreateConnection())
            {
                var query = totalQuery.ToString();
                using (var multi = await connection.QueryMultipleAsync(query))
                {
                    var ordersDictionary = new Dictionary<long, Order>();

                    var allorders = multi.Read<OrderItem, PrintingEdition, Order, ApplicationUser, Order>((orderItem, product, order, user) =>
                    {
                        order.User = user;
                        orderItem.Order = order;
                        orderItem.PrintingEdition = product;
                        Order newOrder;

                        if (!ordersDictionary.TryGetValue(order.Id, out newOrder))
                        {
                            newOrder = order;
                            newOrder.OrderItems = new List<OrderItem>();
                            ordersDictionary.Add(newOrder.Id, newOrder);
                        }
                        newOrder.OrderItems.Add(orderItem);
                        newOrder.User = user;
                        return newOrder;
                    },
                    splitOn: "Id, Id, Id, userId").Distinct().ToList();
                    allOrders = allorders;
                    count = multi.Read<int>().First();
                }
            }
            return new ResponseModel<List<Order>>(allOrders, count);
        }
    }
}
