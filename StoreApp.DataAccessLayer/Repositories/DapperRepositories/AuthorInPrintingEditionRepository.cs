﻿using Dapper;
using Microsoft.Extensions.Configuration;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models;
using StoreApp.DataAccessLayer.Models.Response;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class AuthorInPrintingEditionRepository : BaseDapperRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository
    {
        public AuthorInPrintingEditionRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<IEnumerable<AuthorInPrintingEdition>> GetByAuthorIdAsync(long id)
        {
            using (var connection = CreateConnection())
            {
                string queryString = $"SELECT * FROM AuthorInPrintingEditions WHERE AuthorId=@Id";
                var AuthorsInPrintingEditions = await connection.QueryAsync<AuthorInPrintingEdition>(queryString, new { Id = id });
                return AuthorsInPrintingEditions.AsList();
            }
        }

        public async Task<IEnumerable<AuthorInPrintingEdition>> GetByPrintingEditionIdAsync(long id)
        {
            using (var connection = CreateConnection())
            {
                string queryString = $@"SELECT * FROM AuthorInPrintingEditions 
                                         LEFT JOIN Authors ON Authors.Id = AuthorInPrintingEditions.AuthorId 
                                         LEFT JOIN PrintingEditions ON AuthorInPrintingEditions.PrintingEditionId = PrintingEditions.Id 
                                      WHERE PrintingEditionId = {id}";

                var result = await connection.QueryAsync<Author, PrintingEdition, AuthorInPrintingEdition>(queryString,
                    (author, product) => {
                        var resultItem = new AuthorInPrintingEdition();
                        resultItem.PrintingEdition = product;
                        resultItem.Author = author;
                        return resultItem;
                });
                return result.AsList();
            }
        }

        public async Task<ResponseModel<List<Author>>> GetAuthorsAsync(FilterAuthorModel filterAuthorModel)
        {
            var allAuthors = new List<Author>();
            var count = 0;
            var queryAllAuthors = new StringBuilder(
                 $@"SELECT Authors.*, PrintingEditions.* FROM Authors 
                    LEFT JOIN AuthorInPrintingEditions ON Authors.Id = AuthorInPrintingEditions.AuthorId 
                    LEFT JOIN PrintingEditions ON AuthorInPrintingEditions.PrintingEditionId = PrintingEditions.Id 
                    WHERE Authors.Id IN (SELECT a.Id FROM(SELECT DISTINCT Authors.*
                    FROM Authors
                    LEFT JOIN AuthorInPrintingEditions ON Authors.Id = AuthorInPrintingEditions.AuthorId
                    LEFT JOIN PrintingEditions ON AuthorInPrintingEditions.PrintingEditionId = PrintingEditions.Id ");

            var filterQuery = new StringBuilder();
            filterQuery.Append($"WHERE UPPER(Name) LIKE UPPER('%");

            if (!string.IsNullOrWhiteSpace(filterAuthorModel.SearchFilter))
            {
                string searchString = filterAuthorModel.SearchFilter;

                filterQuery.Append($"{searchString}");
            }

            filterQuery.Append($"%') AND Authors.IsRemoved=0 ");
            queryAllAuthors.Append(filterQuery);
            queryAllAuthors.Append($"ORDER BY ");
            queryAllAuthors.Append($"Authors.{filterAuthorModel.SortColumn.ToString()} {filterAuthorModel.SortType.ToString()}");

            if(filterAuthorModel.PageIndex == 0)
            {
                filterAuthorModel.PageIndex = 1;
            }
            var skipNumber = (filterAuthorModel.PageIndex - 1) * filterAuthorModel.PageSize;

            var pageSize = filterAuthorModel.PageSize;
            queryAllAuthors.Append($" OFFSET {skipNumber} ROWS  FETCH NEXT {pageSize} ROWS ONLY) a);");

            var queryCount = new StringBuilder();
            queryCount.Append(" SELECT COUNT(DISTINCT Id) FROM Authors ");
            queryCount.Append(filterQuery);
            queryAllAuthors.Append(queryCount);

            using (var connection = CreateConnection())
            {
                var query = queryAllAuthors.ToString();
                using (var multi = await connection.QueryMultipleAsync(query))
                {
                    var authorDictionary = new Dictionary<long, Author>();
                    var allAuthorsItems = multi.Read<Author, PrintingEdition, Author>((author, printingEdition) =>
                      {
                          Author authorItem;
                          if (!authorDictionary.TryGetValue(author.Id, out authorItem))
                          {
                              authorItem = author;
                              authorItem.ProductTitles = new List<string>();
                              authorDictionary.Add(authorItem.Id, authorItem);
                          }
                          if (printingEdition == null)
                          {
                              printingEdition = new PrintingEdition();
                              printingEdition.Title = string.Empty;
                          }

                          authorItem.ProductTitles.Add(printingEdition.Title);
                          return authorItem;
                      },
                    splitOn: "Id").Distinct().ToList();
                    count = multi.Read<int>().First();
                    allAuthors = allAuthorsItems;
                }
            }
            return new ResponseModel<List<Author>>(allAuthors.ToList(), count);
        }

        public async Task<ResponseModel<List<PrintingEdition>>> GetPrintingEditionsAsync(FilterPrintingEditionModel filterPrintingEditionModel)
        {
            var allPrintingEditions = new List<PrintingEdition>();
            var count = 0;
            var queryAllPrintingEdition = new StringBuilder(
                $@"SELECT Authors.*, PrintingEditions.* FROM AuthorInPrintingEditions
                    INNER JOIN Authors ON Authors.Id = AuthorInPrintingEditions.AuthorId 
                    INNER JOIN PrintingEditions ON AuthorInPrintingEditions.PrintingEditionId = PrintingEditions.Id 
                  WHERE PrintingEditions.Id IN (SELECT a.Id FROM(SELECT DISTINCT PrintingEditions.* 
                  FROM AuthorInPrintingEditions 
                    INNER JOIN Authors ON Authors.Id = AuthorInPrintingEditions.AuthorId 
                    INNER JOIN PrintingEditions ON AuthorInPrintingEditions.PrintingEditionId = PrintingEditions.Id ");

            var filterQuery = new StringBuilder();
            filterQuery.Append($"WHERE UPPER(PrintingEditions.Title) LIKE UPPER('%");
            if (!string.IsNullOrWhiteSpace(filterPrintingEditionModel.SearchFilter))
            {
                string searchString = filterPrintingEditionModel.SearchFilter;

                filterQuery.Append($"{searchString}");
            }
            filterQuery.Append($"%') AND PrintingEditions.IsRemoved=0");

            var sortTypes = new List<Enums.ProductType>();

            foreach (Enums.ProductType type in (Enums.ProductType[])Enum.GetValues(typeof(Enums.ProductType)))
            {
                sortTypes.Add(type);
            }

            var resultCollection = sortTypes.Except(filterPrintingEditionModel.PrintingEditionCategoriesSort);

            foreach (var type in resultCollection)
            {
                var value = (int)type;
                filterQuery.Append($" AND PrintingEditions.Type!={value}");
            }

            filterQuery.Append($" AND PrintingEditions.Price >={filterPrintingEditionModel.PrintingEditionMinPrice}" +
                                           $" AND PrintingEditions.Price<={filterPrintingEditionModel.PrintingEditionMaxPrice}");

            queryAllPrintingEdition.Append(filterQuery);
            queryAllPrintingEdition.Append($" ORDER BY ");
            queryAllPrintingEdition.Append($"{filterPrintingEditionModel.SortColumn.ToString()} {filterPrintingEditionModel.SortType.ToString()}");

            if (filterPrintingEditionModel.PageIndex == 0)
            {
                filterPrintingEditionModel.PageIndex = 1;
            }
            var skipNumber = (filterPrintingEditionModel.PageIndex - 1) * filterPrintingEditionModel.PageSize;
            var pageSize = filterPrintingEditionModel.PageSize;
            queryAllPrintingEdition.Append($" OFFSET {skipNumber} ROWS  FETCH NEXT {pageSize} ROWS ONLY) a);");
            var queryCount = new StringBuilder();
            queryCount.Append(" SELECT COUNT(DISTINCT Id) FROM PrintingEditions ");
            queryCount.Append(filterQuery);
            queryAllPrintingEdition.Append(queryCount);

            using (var connection = CreateConnection())
            {
                var query = queryAllPrintingEdition.ToString();
                using (var multi = await connection.QueryMultipleAsync(query))
                {
                    var productDictionary = new Dictionary<long, PrintingEdition>();
                    var allProductsItems = multi.Read<Author, PrintingEdition, PrintingEdition>((author, printingEdition) =>
                    {
                        PrintingEdition productItem;
                        if (!productDictionary.TryGetValue(printingEdition.Id, out productItem))
                        {
                            productItem = printingEdition;
                            productItem.AuthorNames = new List<string>();
                            productDictionary.Add(productItem.Id, productItem);
                        }
                        productItem.AuthorNames.Add(author.Name);
                        return productItem;
                    },
                    splitOn: "Id").Distinct().ToList();
                    count = multi.Read<int>().First();
                    allPrintingEditions = allProductsItems;
                }
            }
            return new ResponseModel<List<PrintingEdition>>(allPrintingEditions.ToList(), count);
        }
    }
}
