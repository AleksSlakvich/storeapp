﻿using Microsoft.Extensions.Configuration;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;

namespace StoreApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class OrderItemRepository : BaseDapperRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(IConfiguration configuration) : base(configuration)
        {
        }
    }
}
