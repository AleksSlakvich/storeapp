﻿using Dapper;
using Microsoft.Extensions.Configuration;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Base;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoreApp.DataAccessLayer.Repositories.DapperRepositories
{
    public class AuthorRepository : BaseDapperRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(IConfiguration configuration): base(configuration)
        {
        }

        public async Task<List<Author>> GetAllAuthorsAsync()
        {
            using (var connection = CreateConnection())
            {
                var allAuthors = await connection.QueryAsync<Author>($"SELECT * FROM Authors WHERE IsRemoved=0");
                return allAuthors.AsList();
            }
        }
    }
}
