﻿using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models.Base;
using System.Collections.Generic;

namespace StoreApp.DataAccessLayer.Models
{
    public class FilterUserModel : FilterBaseModel
    {
        public List<Enums.UserStatus> UserStatusSort { get; set; }
        public Enums.SortType SortType { get; set; }
        public Enums.ColumnType SortColumn { get; set; }

        public FilterUserModel()
        {
                
        }
    }
}
