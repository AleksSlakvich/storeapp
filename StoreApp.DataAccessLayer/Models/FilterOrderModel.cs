﻿using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models.Base;
using System.Collections.Generic;

namespace StoreApp.DataAccessLayer.Models
{
    public class FilterOrderModel : FilterBaseModel
    {
        public List<Enums.OrderStatus> OrderStatusSort { get; set; }
        public Enums.SortType SortType { get; set; }
        public Enums.ColumnType SortColumn { get; set; }

        public FilterOrderModel()
        {

        }
    }
}
