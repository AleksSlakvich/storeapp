﻿using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models.Base;

namespace StoreApp.DataAccessLayer.Models
{
    public class FilterAuthorModel: FilterBaseModel
    {
        public Enums.SortType SortType { get; set; }
        public Enums.ColumnType SortColumn { get; set; }

        public FilterAuthorModel()
        {
                
        }
    }
}
