﻿namespace StoreApp.DataAccessLayer.Models.Base
{
    public class FilterBaseModel
    {
        public string SearchFilter { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
