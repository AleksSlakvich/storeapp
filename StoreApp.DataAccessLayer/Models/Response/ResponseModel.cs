﻿namespace StoreApp.DataAccessLayer.Models.Response
{
    public class ResponseModel<T> where T : class
    {
        public T Data { get; set; }
        public int ItemsCount { get; set; }

        public ResponseModel()
        {

        }

        public ResponseModel(T data, int count)
        {
            Data = data;
            ItemsCount = count;
        }
    }
}
