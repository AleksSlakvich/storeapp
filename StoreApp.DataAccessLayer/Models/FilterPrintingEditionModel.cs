﻿using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp.DataAccessLayer.Models
{
    public class FilterPrintingEditionModel: FilterBaseModel
    {
        public List<Enums.ProductType> PrintingEditionCategoriesSort { get; set; }
        public Enums.SortType SortType { get; set; }
        public Enums.ColumnType SortColumn { get; set; }
        public decimal PrintingEditionMinPrice { get; set; }
        public decimal PrintingEditionMaxPrice { get; set; }

        public FilterPrintingEditionModel()
        {

        }
    }
}
