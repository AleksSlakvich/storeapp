﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace StoreApp.DataAccessLayer.Extensions
{
    public class GeneratorListOfProperties
    {
        public static List<string> GenerateListOfProperties(IEnumerable<PropertyInfo> Properties)
        {
            return (from property in Properties
                    let attributes = property.GetCustomAttributes(typeof(DescriptionAttribute), false)
                    where attributes.Length <= 0 || (attributes[0] as DescriptionAttribute)?.Description != "ignore"
                    select property.Name).ToList();
        }
    }
}
