﻿using StoreApp.DataAccessLayer.Entities.Enums;
using System.Linq;

namespace StoreApp.DataAccessLayer.Extensions
{
    public class FilterSortExtension<T> where T : class
    {
        public static IQueryable<T> FilterSort(IQueryable<T> list, Enums.SortType sortType, Enums.ColumnType columnType)
        {
            if (sortType == Enums.SortType.ASC)
            {
                list = list.OrderBy(s => s.GetType().GetProperty(columnType.ToString()).GetValue(s));
            }
            if (sortType == Enums.SortType.DESC)
            {
                list = list.OrderByDescending(s => s.GetType().GetProperty(columnType.ToString()).GetValue(s));
            }
            return list;
        }
    }
}
