﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.User;
using StoreApp.BusinessLogicLayer.Services.Interfaces;

namespace StoreApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] UserModelItem model)
        {
            var result = await _userService.UpdateAsync(model);
            return Ok(result);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete([FromBody] UserModelItem model)
        {
            var result = await _userService.DeleteAsync(model);
            return Ok(result);
        }

        [HttpPost("block")]
        public async Task<IActionResult> Block([FromBody] UserModelItem model)
        {
            var result = await _userService.BlockAsync(model);
            return Ok(result);
        }

        [HttpPost("unblock")]
        public async Task<IActionResult> UnBlock([FromBody] UserModelItem model)
        {
            var result = await _userService.UnBlockAsync(model);
            return Ok(result);
        }

        [HttpPost("getusers")]
        public async Task<IActionResult> GetUsers([FromBody] UserFilterModel model)
        {
            var result = await _userService.GetFilteredAsync(model);
            return Ok(result);
        }
    }
}