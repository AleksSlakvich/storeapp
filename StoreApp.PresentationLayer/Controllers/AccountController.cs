﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using StoreApp.BusinessLogicLayer.Models.Account;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.PresentationLayer.Helpers;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IJwtHelper _jwtHelper;
        private readonly IConfiguration _configuration;

        public AccountController(IAccountService accountService, IJwtHelper jwtHelper, IConfiguration configuration)
        {
            _accountService = accountService;
            _jwtHelper = jwtHelper;
            _configuration = configuration;
        }

        [HttpPost("signin")]
        public async Task<IActionResult> SignIn(LogInModel model)
        {
            var result = await _accountService.SignInAsync(model);
            if (result.Id == 0)
            {
                return Ok(result as BaseModel);
            }
            var tokenModel = _jwtHelper.GenerateJwtToken(model.Email, result.Id);
            AddTokensToCookies(tokenModel);
            return Ok(result);
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationModel model)
        {
            var result = await _accountService.SignUpAsync(model);
            if (result.Id == 0)
            {
                return Ok(result as BaseModel);
            }
            var code = await _accountService.GenerateEmailConfirmationTokenAsync(result);

            var callbackUrl = Url.Action(
                Global.ConfirmEmail,
                Global.Account,
                new { userId = result.Id, userCode = code },
                protocol: HttpContext.Request.Scheme);

            await _accountService.SendConfirmEmailAsync(model.Email, callbackUrl);
            var tokenModel = _jwtHelper.GenerateJwtToken(model.Email, result.Id);
            AddTokensToCookies(tokenModel);
            return Ok(result);
        }


        [HttpGet("refreshTokens")]
        public IActionResult RefreshTokens()
        {
            var tokenModel = new JwtTokenModel();
            var refreshToken = Request.Cookies["RefreshToken"];

            if (string.IsNullOrWhiteSpace(refreshToken))
            {
                tokenModel.Errors.Add(BaseErrors.EmptyDataError);
                return Ok(tokenModel as BaseModel);
            }

            var userId = _jwtHelper.ReadJwtToken(refreshToken);
            if (userId == 0)
            {
                tokenModel.Errors.Add(UserErrors.IncorrectCode);
                return Ok(tokenModel as BaseModel);
            }
            var user = _accountService.GetByIdAsync(Convert.ToInt64(userId));

            tokenModel = _jwtHelper.RefreshJwtToken(refreshToken, user.Result.Email, user.Result.Id);

            if (tokenModel.Errors.Any())
            {
                _accountService.SignOutAsync();
                return Ok(tokenModel as BaseModel);
            }
            AddTokensToCookies(tokenModel);
            return Ok(tokenModel as BaseModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(long userId, string code)
        {
            await _accountService.ConfirmEmailAsync(userId, code);
            return Redirect(_configuration["SuccessRegistration"]);
        }

        [HttpPost("forgotpassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ResetPasswordModel model)
        {
            var result = await _accountService.ForgotPasswordAsync(model);
            return Ok(result);
        }

        [Authorize]
        [HttpGet("signout")]
        public async Task<IActionResult> SignOut()
        {
            var result = await _accountService.SignOutAsync();
            return Ok(result);
        }

        private void AddTokensToCookies (JwtTokenModel tokenModel)
        {
            HttpContext.Response.Cookies.Append(Global.AccessToken, tokenModel.AccessToken,
                                                new CookieOptions
                                                {
                                                    IsEssential = true,
                                                    Expires = DateTime.Now.AddMinutes(Convert.ToDouble(_configuration["JwtExpireMinutes"]))
                                                });
            HttpContext.Response.Cookies.Append(Global.RefreshToken, tokenModel.RefreshToken,
                                                new CookieOptions
                                                {
                                                    IsEssential = true,
                                                    Expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]))
                                                });
        }
    }
}