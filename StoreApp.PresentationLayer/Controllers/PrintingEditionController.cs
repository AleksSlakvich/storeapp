﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.PrintingEditions;
using StoreApp.BusinessLogicLayer.Services.Interfaces;

namespace StoreApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrintingEditionController : ControllerBase
    {
        private readonly IPrintingEditionService _printingEditionService;
        public PrintingEditionController(IPrintingEditionService printingEditionService)
        {
            _printingEditionService = printingEditionService;
        }

        [Authorize]
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] PrintingEditionModelItem model)
        {
            var result = await _printingEditionService.CreateAsync(model);
            return Ok(result);
        }

        [Authorize]
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] PrintingEditionModelItem model)
        {
            var result = await _printingEditionService.UpdateAsync(model);
            return Ok(result);
        }

        [Authorize]
        [HttpPost("delete")]
        public async Task<IActionResult> Delete([FromBody] PrintingEditionModelItem model)
        {
            var result = await _printingEditionService.DeleteAsync(model);
            return Ok(result);
        }

        [HttpPost("getprintingeditions")]
        public async Task<IActionResult> GetPrintingEditions([FromBody] PrintingEditionFilterModel model)
        {
            var result = await _printingEditionService.GetFilteredAsync(model);
            return Ok(result);
        }

        [HttpGet("getprintingedition")]
        public async Task<IActionResult> GetPrintingEdition(long productId)
        {
            var result = await _printingEditionService.GetByPrintingEditionIdAsync(productId);
            return Ok(result);
        }
    }
}