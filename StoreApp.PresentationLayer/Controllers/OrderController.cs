﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.Cart;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(CartModel model)
        {
            var result = await _orderService.CreateAsync(model);
            return Ok(result);
        }

        [HttpPost("getorders")]
        public async Task<IActionResult> GetOrders([FromBody] OrderFilterModel model)
        {
            var result = await _orderService.GetFilteredAsync(model);
            return Ok(result);
        }
    }
}