﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.Author;
using StoreApp.BusinessLogicLayer.Services.Interfaces;

namespace StoreApp.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;
        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] AuthorModelItem model)
        {
            var result = await _authorService.CreateAsync(model);
            return Ok(result);
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] AuthorModelItem model)
        {
            var result = await _authorService.UpdateAsync(model);
            return Ok(result);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete([FromBody] AuthorModelItem model)
        {
            var result = await _authorService.DeleteAsync(model);
            return Ok(result);
        }

        [HttpPost("getauthors")]
        public async Task<IActionResult> GetAuthors([FromBody] AuthorFilterModel model)
        {
            var result = await _authorService.GetFilteredAsync(model);
            return Ok(result);
        }

        [HttpGet("getallauthors")]
        public async Task<IActionResult> GetAllAuthors()
        {
            var result = await _authorService.GetAllAuthorsAsync();
            return Ok(result);
        }
    }
}