﻿using Microsoft.AspNetCore.Mvc;


namespace StoreApp.PresentationLayer.Controllers.Base
{
    public class BaseController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
