﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StoreApp.BusinessLogicLayer.Exeptions;
using StoreApp.BusinessLogicLayer.Models.Constants;

namespace StoreApp.PresentationLayer.Middlewares
{
    public class HttpStatusCodeExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<HttpStatusCodeExceptionMiddleware> _logger;
        private readonly ILogger _errorlogger;

        public HttpStatusCodeExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = loggerFactory?.CreateLogger<HttpStatusCodeExceptionMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
            _errorlogger = loggerFactory.CreateLogger("ErrorLogger");
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (HttpStatusCodeException ex)
            {
                await CheckContexResponse(context, ex);
            }
        }

        private async Task CheckContexResponse(HttpContext contextForCheck, HttpStatusCodeException exeption)
        { 
            if (contextForCheck.Response.HasStarted)
            {
                _logger.LogWarning(Constants.BaseErrors.LogWarning);
                throw exeption;
            }
            contextForCheck.Response.Clear();
            contextForCheck.Response.StatusCode = exeption.StatusCode;
            contextForCheck.Response.ContentType = exeption.ContentType;
            _errorlogger.LogError("Status Code:" + exeption.StatusCode + " , Message:" + exeption.Message + " , HelpLink:" + exeption.HelpLink);
            await contextForCheck.Response.WriteAsync(exeption.Message);
            return;
        }
    }
}
