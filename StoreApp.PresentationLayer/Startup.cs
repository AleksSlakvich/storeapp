using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StoreApp.BusinessLogicLayer.Common.Options;
using StoreApp.BusinessLogicLayer.Extensions;
using StoreApp.BusinessLogicLayer.Initialize;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Initialization;
using StoreApp.PresentationLayer.Extensions;
using StoreApp.PresentationLayer.Helpers;

namespace StoreApp.PresentationLayer
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");

            Initializer.Init(services, Configuration);

            services.Configure<EmailOptions>(Configuration.GetSection("EmailSettings"));
            services.Configure<PasswordGeneratorOptions>(Configuration.GetSection("PasswordSettings"));
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddScoped<IJwtHelper, JwtHelper>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwagger(Configuration);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, UserManager<ApplicationUser> userManager,
                              DataBaseInitialization dataBaseInitialization)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseHttpStatusCodeExceptionMiddleware();
            }
            app.UseCors(options => options.AllowAnyMethod().AllowAnyHeader().AllowCredentials().WithOrigins("http://localhost:4200"));
            app.UseCustomSwagger(Configuration);
            app.UseHttpStatusCodeExceptionMiddleware();
            app.UseExceptionHandler("/Error");
            app.UseHsts();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            
            loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "errorlogger.txt"));
            var logger = loggerFactory.CreateLogger("LoggerProvider");
            //var res = dataBaseInitialization.SeedData(userManager).ConfigureAwait(false).GetAwaiter().GetResult();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
