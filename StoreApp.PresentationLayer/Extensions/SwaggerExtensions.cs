﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;

namespace StoreApp.PresentationLayer.Extensions
{
    public static class SwaggerExtensions
    {
        public static void UseCustomSwagger(this IApplicationBuilder builder, IConfiguration configuration)
        {
            builder.UseSwagger();
            builder.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(configuration["SwaggerUIUrl"], configuration["SwaggerUIName"]);
                c.RoutePrefix = string.Empty;
            });
        }

        public static void AddSwagger (this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(configuration["SwaggerVersion"], new OpenApiInfo
                {
                    Version = configuration["SwaggerVersion"],
                    Title = configuration["SwaggerTitle"],
                    Description = configuration["SwaggerDescription"],
                    TermsOfService = new Uri(configuration["SwaggerTermsOfService"]),
                    Contact = new OpenApiContact
                    {
                        Name = configuration["SwaggerContactName"],
                        Email = configuration["SwaggerContactEmail"],
                        Url = new Uri(configuration["SwaggerContactUrl"]),
                    },
                    License = new OpenApiLicense
                    {
                        Name = configuration["SwaggerLicenseName"],
                        Url = new Uri(configuration["SwaggerLicenseUrl"]),
                    }
                });
            });
        }
    }
}
