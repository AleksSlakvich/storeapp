﻿using Microsoft.AspNetCore.Builder;
using StoreApp.PresentationLayer.Middlewares;

namespace StoreApp.PresentationLayer.Extensions
{
    public static class HttpStatusCodeExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseHttpStatusCodeExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HttpStatusCodeExceptionMiddleware>();
        }
    }
}
