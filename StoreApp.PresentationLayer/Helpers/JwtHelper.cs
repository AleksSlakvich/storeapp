﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using StoreApp.BusinessLogicLayer.Models.Account;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.PresentationLayer.Helpers
{
    public class JwtHelper : IJwtHelper
    {
        private readonly IConfiguration _configuration;

        public JwtHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public JwtTokenModel GenerateJwtToken(string email, long userId)
        {
            var accessClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, userId.ToString())
            };

            var refreshClaims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var accessExpires = DateTime.Now.AddMinutes(Convert.ToDouble(_configuration["JwtExpireMinutes"]));
            var refreshExpires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));
            var accessToken = Create(accessClaims, accessExpires, creds);
            var refreshToken = Create(refreshClaims, refreshExpires, creds);
            JwtTokenModel tokens = new JwtTokenModel
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(accessToken),
                RefreshToken = new JwtSecurityTokenHandler().WriteToken(refreshToken)
            };
            return tokens;
        }

        private JwtSecurityToken Create (List<Claim> claims, DateTime expires, SigningCredentials creds)
        {
            return new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
                );
        }

        public JwtTokenModel RefreshJwtToken(string token, string userEmail, long userId)
        {
            var newTokens = new JwtTokenModel();
            //if (string.IsNullOrWhiteSpace(token))
            //{
            //    newTokens.Errors.Add(BaseErrors.EmptyDataError);
            //    return newTokens;
            //}

            var refreshToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
            //var nameidentifier = refreshToken.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier).Value;
            //var user = _accountService.GetByIdAsync(Convert.ToInt64(nameidentifier));

            if (!(refreshToken.ValidTo > DateTime.Now))
            {
                //_accountService.SignOutAsync();
                newTokens.Errors.Add(TokenErrors.NotValidTokens);
                return newTokens;
            }
            newTokens = GenerateJwtToken(userEmail, userId);
            return newTokens;
        }

        public long ReadJwtToken(string token)
        {
            if (!string.IsNullOrWhiteSpace(token))
            {
                var refreshToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
                var nameidentifier = refreshToken.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier).Value;
                long userId = Convert.ToInt64(nameidentifier);
                return userId;
            }
            return 0;
        }
    }
}
