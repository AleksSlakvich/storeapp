﻿using StoreApp.BusinessLogicLayer.Models.Account;

namespace StoreApp.PresentationLayer.Helpers
{
    public interface IJwtHelper
    {
        JwtTokenModel GenerateJwtToken(string email, long userId);
        JwtTokenModel RefreshJwtToken(string token, string userEmail, long userId);
        long ReadJwtToken(string token);
    }
}
