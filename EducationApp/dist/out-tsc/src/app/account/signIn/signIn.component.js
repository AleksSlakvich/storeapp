import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
let SignInComponent = class SignInComponent {
    constructor(fb, apiService) {
        this.fb = fb;
        this.apiService = apiService;
        this.hide = true;
        this.emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
        this.passwordPattern = '^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$';
        this.createForm();
    }
    createForm() {
        this.angForm = this.fb.group({
            email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
            password: new FormControl('', [Validators.required, Validators.pattern(this.passwordPattern)])
        });
    }
    onClickSubmit() {
        alert('Your Email is : ' + this.angForm.controls.email.value + 'Your password is: ' + this.angForm.controls.password.value);
    }
    ngOnInit() {
    }
    TestSubmit() {
        debugger;
        this.apiService.test().subscribe((data) => {
            console.log(data);
        });
    }
};
SignInComponent = tslib_1.__decorate([
    Component({
        selector: 'app-signIn',
        templateUrl: './signIn.component.html',
        styleUrls: ['./signIn.component.css']
    })
], SignInComponent);
export { SignInComponent };
//# sourceMappingURL=signIn.component.js.map