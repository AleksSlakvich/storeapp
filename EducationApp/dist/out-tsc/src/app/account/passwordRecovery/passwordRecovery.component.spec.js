/* tslint:disable:no-unused-variable */
import { async, TestBed } from '@angular/core/testing';
import { PasswordRecoveryComponent } from './passwordRecovery.component';
describe('PasswordRecoveryComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PasswordRecoveryComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(PasswordRecoveryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=passwordRecovery.component.spec.js.map