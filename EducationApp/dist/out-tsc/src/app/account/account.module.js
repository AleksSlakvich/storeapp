import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';
import { FormsModule } from '@angular/forms';
import { ConfirmPasswordComponent } from './confirmPassword/confirmPassword.component';
import { PasswordRecoveryComponent } from './passwordRecovery/passwordRecovery.component';
import { SignInComponent } from './signIn/signIn.component';
import { SignUpComponent } from './signUp/signUp.component';
import { AccountRoutes } from './account.routing';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
let AccountModule = class AccountModule {
};
AccountModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            AccountRoutes,
            MatInputModule,
            MatFormFieldModule,
            ReactiveFormsModule,
            MatIconModule,
            MatCheckboxModule,
            MatButtonModule,
            FormsModule
        ],
        declarations: [
            AccountComponent,
            ConfirmPasswordComponent,
            PasswordRecoveryComponent,
            SignInComponent,
            SignUpComponent
        ]
    })
], AccountModule);
export { AccountModule };
//# sourceMappingURL=account.module.js.map