import { RouterModule } from '@angular/router';
import { ConfirmPasswordComponent } from './confirmPassword/confirmPassword.component';
import { PasswordRecoveryComponent } from './passwordRecovery/passwordRecovery.component';
import { SignInComponent } from './signIn/signIn.component';
import { SignUpComponent } from './signUp/signUp.component';
export const routes = [
    { path: 'signIn', component: SignInComponent },
    { path: 'signup', component: SignUpComponent },
    { path: 'passwordrecovery', component: PasswordRecoveryComponent },
    { path: 'confirmpassword', component: ConfirmPasswordComponent }
];
export const AccountRoutes = RouterModule.forChild(routes);
//# sourceMappingURL=account.routing.js.map