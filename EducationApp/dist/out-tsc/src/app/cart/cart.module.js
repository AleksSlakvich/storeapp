import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
let CartModule = class CartModule {
};
CartModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        declarations: [CartComponent]
    })
], CartModule);
export { CartModule };
//# sourceMappingURL=cart.module.js.map