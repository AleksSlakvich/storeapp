import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedComponent } from './shared.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
let SharedModule = class SharedModule {
};
SharedModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            MatToolbarModule
        ],
        declarations: [
            SharedComponent,
            HeaderComponent,
            FooterComponent
        ],
        exports: [HeaderComponent, FooterComponent]
    })
], SharedModule);
export { SharedModule };
//# sourceMappingURL=shared.module.js.map