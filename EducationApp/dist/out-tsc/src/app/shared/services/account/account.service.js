import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
let AccountService = class AccountService {
    constructor(httpService) {
        this.httpService = httpService;
    }
    getUser(id) {
        return this.httpService.get(`http://localhost:44327/user/${id}`)
            .pipe(tap(data => {
            debugger;
        }), catchError(this.errorHandler));
    }
    test() {
        return this.httpService.get(`https://localhost:44327/api/account/testtoken`);
    }
    errorHandler(error) {
        debugger;
        return Observable.throw(error.message || "server error.");
    }
};
AccountService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], AccountService);
export { AccountService };
//# sourceMappingURL=account.service.js.map