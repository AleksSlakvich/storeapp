/* tslint:disable:no-unused-variable */
import { TestBed, inject } from '@angular/core/testing';
import { AccountService } from './account.service';
describe('Service: Account', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AccountService]
        });
    });
    it('should ...', inject([AccountService], (service) => {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=account.service.spec.js.map