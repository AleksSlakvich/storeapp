import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let HeaderComponent = class HeaderComponent {
};
tslib_1.__decorate([
    Input()
], HeaderComponent.prototype, "title", void 0);
HeaderComponent = tslib_1.__decorate([
    Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.css']
    })
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map