import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
let UserModule = class UserModule {
};
UserModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        declarations: [UserComponent]
    })
], UserModule);
export { UserModule };
//# sourceMappingURL=user.module.js.map