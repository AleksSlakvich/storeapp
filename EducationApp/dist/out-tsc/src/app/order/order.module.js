import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
let OrderModule = class OrderModule {
};
OrderModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        declarations: [OrderComponent]
    })
], OrderModule);
export { OrderModule };
//# sourceMappingURL=order.module.js.map