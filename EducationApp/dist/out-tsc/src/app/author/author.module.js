import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorComponent } from './author.component';
let AuthorModule = class AuthorModule {
};
AuthorModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        declarations: [AuthorComponent]
    })
], AuthorModule);
export { AuthorModule };
//# sourceMappingURL=author.module.js.map