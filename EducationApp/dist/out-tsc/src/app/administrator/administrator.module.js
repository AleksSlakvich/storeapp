import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministratorComponent } from './administrator.component';
let AdministratorModule = class AdministratorModule {
};
AdministratorModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        declarations: [AdministratorComponent]
    })
], AdministratorModule);
export { AdministratorModule };
//# sourceMappingURL=administrator.module.js.map