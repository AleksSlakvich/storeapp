import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintingEditionComponent } from './printingEdition.component';
let PrintingEditionModule = class PrintingEditionModule {
};
PrintingEditionModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule
        ],
        declarations: [PrintingEditionComponent]
    })
], PrintingEditionModule);
export { PrintingEditionModule };
//# sourceMappingURL=printingEdition.module.js.map