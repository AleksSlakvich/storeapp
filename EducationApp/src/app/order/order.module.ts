import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { OrdersComponent } from './orders/orders.component';

import { OrderRoutes } from './order.routing';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    OrderRoutes,
    FormsModule,
    MaterialModule
  ],
  exports: [
  ],
  declarations: [
    OrdersComponent]
})
export class OrderModule { }
