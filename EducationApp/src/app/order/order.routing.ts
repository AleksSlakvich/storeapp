import { Routes, RouterModule } from '@angular/router';

import { OrdersComponent } from './orders/orders.component';

const routes: Routes = [
  { path: 'orders', component: OrdersComponent }
];

export const OrderRoutes = RouterModule.forChild(routes);