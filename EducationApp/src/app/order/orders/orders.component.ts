import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, Sort, MatCheckboxChange, } from '@angular/material';
import { PaginationModel } from 'src/app/shared/models/pagination/pagination.model';
import { OrderFilterModel } from 'src/app/shared/models/filters/orderFilter.model';
import { OrderModelItem } from 'src/app/shared/models/order/orderModelItem.model';
import { OrderService } from 'src/app/shared/services/order/order.service';
import { ColumnType } from 'src/app/shared/enums/columnType';
import { SortType } from 'src/app/shared/enums/sortType';
import { EnumConverter } from 'src/app/shared/helpers/enumConverter';
import { OrderStatus, ProductType } from 'src/app/shared/enums';
import { OrderItemModel } from 'src/app/shared/models/orderItem/orderItemModel.model';
import { LocalStorageService } from 'src/app/shared/services/localStorage/localStorage.service';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { OrderModel } from 'src/app/shared/models/order/orderModel.model';
import { General, UserRole, SortTypes, Order } from 'src/app/shared/constants'

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  currentUser: UserModelItem;
  public displayedColumns: string[];
  public orderFilterModel: OrderFilterModel;
  public dataSource: MatTableDataSource<OrderModelItem>;
  public paginationModel: PaginationModel;
  selectedButtonsStatus: boolean[];
  selectedStatuses: string[];
  statuses: string[];
  titles: string[];
  counts: number[];
  productTypes: string[];
  isLoadingResults: boolean;
  isRateLimitReached: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  
  constructor(private orderService: OrderService,
    public enumConverter: EnumConverter,
    private storageService: LocalStorageService) {
    this.storageService.currentUser.subscribe((user: UserModelItem) => this.currentUser = user);
    this.selectedButtonsStatus = Order.SelectedStatuses;
    this.selectedStatuses = [];
    this.statuses = this.enumConverter.convertEnumToString(OrderStatus);
    this.paginationModel = new PaginationModel();
    this.isLoadingResults = true;
    this.isRateLimitReached = false;
    this.dataSource = new MatTableDataSource<OrderModelItem>();
    this.dataSource.data = [];
  }

  ngOnInit() {
    this.createTableSettings();
    this.getOrders();
  }

  applyFilter(filterValue: string): void {
    this.isLoadingResults = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.orderFilterModel.searchString = filterValue;
    this.getOrders();
  }

  public handlePage(event: any): void {
    this.isLoadingResults = true;
    this.orderFilterModel.pageIndex = event.pageIndex + 1;
    this.orderFilterModel.pageSize = event.pageSize;
    this.getOrders();
  }

  sortData(sort: Sort): void {
    this.isLoadingResults = true;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0, this.orderFilterModel.pageIndex = 0);
    this.orderFilterModel.sortColumn = ColumnType[sort.active];
    if (sort.direction == SortTypes.Asc) {
      this.orderFilterModel.sortType = SortType.asc;
    }
    if (sort.direction == SortTypes.Desc) {
      this.orderFilterModel.sortType = SortType.desc;
    }
    this.getOrders();
  }

  checkTypeEvent(event: MatCheckboxChange, index: number): void {
    if (!this.selectedButtonsStatus.some(element => element === true)) {
      event.source.toggle();
      this.selectedButtonsStatus[index] = true;
    }
  }

  getTypesValues(): void {
    this.isLoadingResults = true;
    this.selectedStatuses = [];
    for (let i = 0; i < this.selectedButtonsStatus.length; i++) {
      if (this.selectedButtonsStatus[i] === true) {
        this.selectedStatuses.push(this.statuses[i]);
      }
    }
    this.orderFilterModel.orderStatuses = [];
    for (let i = 0; i < this.selectedStatuses.length; i++) {
      this.orderFilterModel.orderStatuses.push(OrderStatus[this.selectedStatuses[i]]);
    }
    this.getOrders();
  }

  getTypeName(object: OrderStatus): string {
    return OrderStatus[object];
  }

  getTitles(object: OrderItemModel): string[] {
    this.titles = [];
    for (let i = 0; i < object.items.length; i++) {
      this.titles.push(object.items[i].printingEdition.title);
    }
    return this.titles;
  }

  getCounts(object: OrderItemModel): number[] {
    this.counts = [];
    for (var i = 0; i < object.items.length; i++) {
      this.counts.push(object.items[i].count);
    }
    return this.counts;
  }

  getCategories(object: OrderItemModel): string[] {
    this.productTypes = [];
    for (let i = 0; i < object.items.length; i++) {
      this.productTypes.push(ProductType[object.items[i].printingEdition.category]);
    }
    return this.productTypes;
  }

  getOrders = (): void => {
    this.orderService.getFilteredOrders(this.orderFilterModel)
      .subscribe((response: OrderModel) => {
        this.dataSource.data = response.items;
        this.dataSource.sort = this.sort;
        this.paginator.length = response.count;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        if (response.errors.length > 0) {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
        }
      });
  }

  createTableSettings(): void {
    this.displayedColumns = Order.DispalyedUserCollumns;
    this.orderFilterModel = Order.OrderInitFilterModel;
    this.orderFilterModel.searchString = this.currentUser.email;
    if (this.currentUser.role == UserRole.Admin) {
      this.displayedColumns = Order.DispalyedAdminCollumns;
      this.orderFilterModel.searchString = General.EmptyString;
    }
  }
}

