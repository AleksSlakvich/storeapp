import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { CookieService } from 'ngx-cookie-service';
import { switchMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatDialogConfig } from '@angular/material';
import { AlertService } from '../services/alert/alert.service';
import { General } from '../constants';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  private isRefreshing: boolean;
  private dialogConfig: MatDialogConfig;

  constructor(
    private cookiesService: CookieService,
    private accountService: AccountService,
    private router: Router,
    private alertService: AlertService) {
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.isRefreshing = false;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const accessToken = this.cookiesService.get(General.AccessToken);
    const refreshToken = this.cookiesService.get(General.RefreshToken);

    if (accessToken) {
      request = this.addToken(request, accessToken);
    }
    const sessionValue = new Date(this.cookiesService.get(General.SessionTime));
    this.checkSessionValueREsponse(sessionValue, request, next, refreshToken);
    return this.checkRequestResponse(request, next, refreshToken);
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      return this.accountService.refreshTokens().pipe(
        switchMap((token: any) => {
          this.checkTokenResponse(token);
          token = this.cookiesService.get(General.AccessToken);
          this.isRefreshing = false;
          return next.handle(this.addToken(request, token));
        }
        ));
    }
  }

  private checkTokenResponse (token: any): void{
    if (token.errors.length > 0) {
      token.errors.forEach(element => {
        this.alertService.error(element);
      });
      this.cookiesService.deleteAll();
      this.accountService.logOut();
      this.router.navigate(['/signIn']);
    }
  }

  private checkSessionValueREsponse (sessionValue: Date, request: HttpRequest<any>, next: HttpHandler, refreshToken: string){
    if (sessionValue instanceof Date && !isNaN(sessionValue.getTime())) {
      const nowValue = new Date();
      if (sessionValue > nowValue) {
        return this.checkRequestResponse(request, next, refreshToken);
      }
      this.cookiesService.deleteAll();
      this.accountService.logOut();
      this.router.navigate(['/signIn']);
      return null;
    }
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
  }

  private checkRequestResponse(request: HttpRequest<any>, next: HttpHandler, refreshToken: string) {
    return next.handle(request).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && error.status == 401) {
        if (refreshToken) {
          return this.handle401Error(request, next);
        }
        this.accountService.logOut();
        this.router.navigate(['/signIn']);
        return null;
      }
      return null;
    }));
  }
}