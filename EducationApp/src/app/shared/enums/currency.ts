export enum Currency {
    USD = 1,
    EUR = 2,
    GBP = 3,
    CHF = 4,
    JPY = 5,
    UAH = 6
  }
