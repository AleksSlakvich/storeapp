import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { ErrorDialogComponent } from './dialogs/error-dialog/error-dialog.component';
import { SuccesDialogComponent } from './dialogs/succes-dialog/succes-dialog.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material/material.module';
import { AvatarModule } from 'ngx-avatar';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    AvatarModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    ErrorDialogComponent,
    SuccesDialogComponent

  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    FlexLayoutModule,
    ErrorDialogComponent,
    SuccesDialogComponent
  ],
  entryComponents: [
    ErrorDialogComponent,
    SuccesDialogComponent
  ]
})
export class SharedModule { }
