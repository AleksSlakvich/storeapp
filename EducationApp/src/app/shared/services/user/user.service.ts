import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModelItem } from '../../models/user/userModelItem.model';
import { BaseModel } from '../../models/base/base.model';
import { tap, catchError } from 'rxjs/operators';
import { UserFilterModel } from '../../models/filters/userFilter.model';
import { UserModel } from '../../models/user/userModel.model';
import { BaseService } from '../base/base.service';
import { General } from 'src/app/shared/constants'
import { AlertService } from '../alert/alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

  apiURL: string;

  constructor(
    private httpClient: HttpClient,
    public alertService: AlertService,
    router: Router) {
    super(alertService, router);
    this.apiURL = General.ApiURL;
  }

  updateUser(updateModel: UserModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}user/update`, updateModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  deleteUser(deleteModel: UserModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}user/delete`, deleteModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  getFilteredUsers(filterModel: UserFilterModel): Observable<UserModel> {
    return this.httpClient.post<UserModel>(`${this.apiURL}user/getusers`, filterModel)
      .pipe(tap(data => {
        this.checkErrors(data as BaseModel);
      }));
  }

  blockUser(blockModel: UserModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}user/block`, blockModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  unblockUser(unblockModel: UserModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}user/unblock`, unblockModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

}
