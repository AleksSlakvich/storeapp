import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { ErrorDialogComponent } from 'src/app/shared/dialogs/error-dialog/error-dialog.component'
import { MatDialog } from '@angular/material';
import { SuccesDialogComponent } from '../../dialogs/succes-dialog/succes-dialog.component';

@Injectable({ providedIn: 'root' })
export class AlertService {

    private subject: Subject<any>;
    private keepAfterNavigationChange: boolean;
    public dialogConfig;

    constructor(private router: Router, private dialog: MatDialog) {
        this.keepAfterNavigationChange = false;
        this.subject = new Subject<any>();
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    this.keepAfterNavigationChange = false;
                }
                this.subject.next();
            }
        });
    }

    success(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
        this.dialogConfig.data = { 'successMessage': message };
        this.dialog.open(SuccesDialogComponent, this.dialogConfig);
    }

    error(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
        this.dialogConfig.data = { 'errorMessage': message };
        this.dialog.open(ErrorDialogComponent, this.dialogConfig);
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}