import { Injectable, Predicate } from '@angular/core';
import { General } from 'src/app/shared/constants'

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  
  transactionId: string = General.EmptyString;

  loadStripe() {
    if (!window.document.getElementById('stripe-script')) {
      let s = window.document.createElement("script");
      s.id = "stripe-script";
      s.type = "text/javascript";
      s.src = "https://checkout.stripe.com/checkout.js";
      window.document.body.appendChild(s);
    }
  }

  pay(amount: number, callback: Predicate<string>): void {
    let handler = (window as any).StripeCheckout.configure({
      key: 'pk_test_Y0vCDfsxSrTgrHNcHdJOapG800p87SeHPP',
      locale: 'auto',
      token: ((data) => {
        callback(data.id);
      }),
    });
    handler.open({
      name: 'Demo Site',
      description: '2 widgets',
      amount: amount * 100
    });
  }

}
