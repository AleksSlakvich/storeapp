import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PrintingEditionModel } from '../../models/printingEdition/printingEditionModel.model';
import { Observable } from 'rxjs';
import { BaseModel } from '../../models/base/base.model';
import { catchError, tap } from 'rxjs/operators';
import { PrintingEditionFilterModel } from '../../models/filters/printingEditionFilter.model';
import { PrintingEditionModelItem } from '../../models/printingEdition/printingEditionModelItem.model';
import { BaseService } from '../base/base.service';
import { General } from 'src/app/shared/constants'
import { AlertService } from '../alert/alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PrintingEditionService extends BaseService {

  apiURL: string;

  constructor(
    private httpClient: HttpClient,
    public alertService: AlertService,
    router: Router) {
    super(alertService, router);
    this.apiURL = General.ApiURL;
  }

  createPrintingEdition(createModel: PrintingEditionModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}printingedition/create`, createModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  updatePrintingEdition(updateModel: PrintingEditionModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}printingedition/update`, updateModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  deletePrintingEdition(deleteModel: PrintingEditionModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}printingedition/delete`, deleteModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  getFilteredPrintingEditions(filterModel: PrintingEditionFilterModel): Observable<PrintingEditionModel> {
    return this.httpClient.post<PrintingEditionModel>(`${this.apiURL}printingedition/getprintingeditions`, filterModel)
      .pipe(tap(data => {
        this.checkErrors(data as BaseModel);
      }));
  }

  getPrintingEdition(productId: number): Observable<PrintingEditionModelItem> {
    return this.httpClient.get<PrintingEditionModelItem>(`${this.apiURL}printingedition/getprintingedition?productId=` + productId)
      .pipe(tap(data => {
        this.checkErrors(data as BaseModel);
      }));
  }
}
