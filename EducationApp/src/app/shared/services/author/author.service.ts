import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseModel } from '../../models/base/base.model';
import { AuthorModelItem } from '../../models/author/authorModelItem.model';
import { tap } from 'rxjs/operators';
import { AuthorFilterModel } from '../../models/filters/authorFilter.model';
import { AuthorModel } from '../../models/author/authorModel.model';
import { BaseService } from '../base/base.service';
import { General } from 'src/app/shared/constants'
import { AlertService } from '../alert/alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthorService extends BaseService {

  apiURL: string;

  constructor(
    private httpClient: HttpClient,
    alertService: AlertService,
    router: Router) {
    super(alertService, router);
    this.apiURL = General.ApiURL;
  }

  createAuthor(createModel: AuthorModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}author/create`, createModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  deleteAuthor(deleteModel: AuthorModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}author/delete`, deleteModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  updateAuthor(updateModel: AuthorModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}author/update`, updateModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  getFilteredAuthors(filterModel: AuthorFilterModel): Observable<AuthorModel> {
    return this.httpClient.post<AuthorModel>(`${this.apiURL}author/getauthors`, filterModel)
      .pipe(tap(data => {
        this.checkErrors(data as BaseModel);
      }));
  }

  getAllAuthors(): Observable<AuthorModel> {
    return this.httpClient.get<AuthorModel>(`${this.apiURL}author/getallauthors`)
      .pipe(tap(data => {
        this.checkErrors(data as BaseModel);
      }));
  }

}
