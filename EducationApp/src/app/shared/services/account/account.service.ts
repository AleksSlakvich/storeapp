import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { LogInModel } from 'src/app/shared/models/account/logIn.model';
import { catchError, tap, map } from 'rxjs/operators';
import { RegistrationModel } from '../../models/account/registration.model';
import { ResetPasswordModel } from '../../models/account/resetPassword.model';
import { UserModelItem } from '../../models/user/userModelItem.model';
import { CookieService } from 'ngx-cookie-service';
import { LocalStorageService } from '../localStorage/localStorage.service';
import { BaseService } from '../base/base.service';
import { General } from 'src/app/shared/constants'
import { AlertService } from '../alert/alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountService extends BaseService {

  apiURL: string;

  constructor(private httpClient: HttpClient,
    private cookiesService: CookieService,
    private storageService: LocalStorageService,
    public alertService: AlertService,
    router: Router) {
    super(alertService, router);
    this.apiURL = General.ApiURL;
  }

  signIn(logInModel: LogInModel): Observable<UserModelItem> {
    return this.httpClient.post<UserModelItem>(`${this.apiURL}account/signin`, logInModel, {
      withCredentials: true
    }).pipe(map(user => {
      if (user) {
        this.storageService.setUser(user);
        this.checkInputRememberMe(logInModel);
      }
      this.checkErrors(user as BaseModel);
      return user;
    }));
  }

  signUp(signUpModel: RegistrationModel): Observable<UserModelItem> {
    return this.httpClient.post<UserModelItem>(`${this.apiURL}account/register`, signUpModel, {
      withCredentials: true
    }).pipe(tap(user => {
      if (user) {
        this.storageService.setUser(user);
      }
      this.checkErrors(user as BaseModel);
      return user;
    }));
  }

  update(updateModel: UserModelItem): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}user/update`, updateModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.storageService.removeUser();
      this.storageService.setUser(updateModel);
      this.checkErrors(data as BaseModel);
    }));
  }

  forgotPassword(resetPasswordModel: ResetPasswordModel): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}account/forgotpassword`, resetPasswordModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  signOut(): Observable<BaseModel> {
    return this.httpClient.get<BaseModel>(`${this.apiURL}account/signout`, { withCredentials: true })
      .pipe(tap(data => {
        this.logOut();
        this.storageService.removeCart();
        this.cookiesService.deleteAll();
        this.checkErrors(data as BaseModel);
      }));
  }

  logOut() {
    this.storageService.removeUser();
  }

  refreshTokens(): Observable<BaseModel> {
    return this.httpClient.get<BaseModel>(`${this.apiURL}account/refreshTokens`, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }

  private checkInputRememberMe (logInModel: LogInModel){
    if (logInModel.rememberMe === true) {
      let current = new Date();
      let followingDay = new Date(current.getTime() + 3600000);
      this.cookiesService.set(General.SessionTime, followingDay.toISOString());
    }
  }
}
