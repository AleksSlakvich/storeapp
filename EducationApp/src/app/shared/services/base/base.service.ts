import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { AlertService } from '../alert/alert.service';
import { BaseModel } from '../../models/base/base.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(
    public alertService: AlertService,
    private router: Router) { }

  public handleServiceError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    }
    else {
      console.error(
        `Returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };

  checkErrors(errorModel: BaseModel): void {
    if (errorModel.errors.length === 0) {
      return;
    }
    errorModel.errors.forEach((element: string) => {
      this.alertService.error(element);
    });
  }

  inputSuccessMessage(data: BaseModel, successMessage: string, navigationPage: string) {
    if (data.errors.length === 0) {
      this.alertService.success(successMessage, true);
      this.router.navigate([navigationPage]);
    }
  }
}
