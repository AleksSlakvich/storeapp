import { Injectable } from '@angular/core';
import { CartModelItem } from '../../models/cart/cartModelItem.model';
import { CartModel } from '../../models/cart/cartModel.model';
import { LocalStorageService } from '../localStorage/localStorage.service';
import { General } from 'src/app/shared/constants'

@Injectable({
  providedIn: 'root'
})
export class CartService {

  apiURL: string;
  public cartModel: CartModel;

  constructor(private storageService: LocalStorageService) {
    this.cartModel = new CartModel();
    this.cartModel.items = [];
    this.apiURL = General.ApiURL;
  }

  createCartModel(cartModelItem: CartModelItem, userId: number) {
    this.cartModel.userId = userId;
    this.cartModel.items.push(cartModelItem);
    this.storageService.setCart(this.cartModel);
  }

  updateCartModel(updateModel: CartModel) {
    this.storageService.removeCart();
    this.storageService.setCart(updateModel);
  }

  refreshCartModel() {
    this.storageService.removeCart();
    this.cartModel.items = [];
  }
}
