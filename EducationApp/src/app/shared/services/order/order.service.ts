import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrderFilterModel } from '../../models/filters/orderFilter.model';
import { OrderModel } from '../../models/order/orderModel.model';
import { tap, catchError } from 'rxjs/operators';
import { BaseModel } from '../../models/base/base.model';
import { CartModel } from '../../models/cart/cartModel.model';
import { BaseService } from '../base/base.service';
import { General } from 'src/app/shared/constants'
import { AlertService } from '../alert/alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OrderService extends BaseService {
  apiURL: string;

  constructor(
    private httpClient: HttpClient,
    public alertService: AlertService,
    router: Router) {
    super(alertService, router);
    this.apiURL = General.ApiURL;
  }

  getFilteredOrders(filterModel: OrderFilterModel): Observable<OrderModel> {
    return this.httpClient.post<OrderModel>(`${this.apiURL}order/getorders`, filterModel)
      .pipe(tap(data => {
        this.checkErrors(data as BaseModel);
      }));
  }

  createOrder(createModel: CartModel): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${this.apiURL}order/create`, createModel, {
      withCredentials: true
    }).pipe(tap(data => {
      this.checkErrors(data as BaseModel);
    }));
  }
}
