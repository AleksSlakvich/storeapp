import { Injectable } from '@angular/core';
import { UserModelItem } from '../../models/user/userModelItem.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { CartModel } from '../../models/cart/cartModel.model';
import { General } from 'src/app/shared/constants'

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  public currentUserSubject: BehaviorSubject<UserModelItem>;
  public currentUser: Observable<UserModelItem>;
  private currentCartModelSubject: BehaviorSubject<CartModel>;
  public currentCartModel: Observable<CartModel>;
  
  constructor() { 
    this.currentUserSubject = new BehaviorSubject<UserModelItem>(JSON.parse(localStorage.getItem(General.CurrentUser)));
    this.currentUser = this.currentUserSubject.asObservable();
    this.currentCartModelSubject = new BehaviorSubject<CartModel>(JSON.parse(localStorage.getItem(General.CurrentCart)));
    this.currentCartModel = this.currentCartModelSubject.asObservable();
  }

  public get currentUserValue(): UserModelItem {
    return this.currentUserSubject.value;
  }

  setUser(user: UserModelItem) {
    localStorage.setItem(General.CurrentUser, JSON.stringify(user));
    this.currentUserSubject.next(user);
  }

  getUser() {
    let user = JSON.parse(localStorage.getItem(General.CurrentUser));
    return user;
  }

  removeUser(): void {
    localStorage.removeItem(General.CurrentUser);
    this.currentUserSubject.next(null);
  }

  setCart(cart: CartModel): void {
    localStorage.setItem(General.CurrentCart, JSON.stringify(cart));
    this.currentCartModelSubject.next(cart);
  }

  getCartModel(): Observable<CartModel>{
    return this.currentCartModelSubject.asObservable();
  }

  removeCart(){
    localStorage.removeItem(General.CurrentCart);
    this.currentCartModelSubject.next(null);
  }
}
