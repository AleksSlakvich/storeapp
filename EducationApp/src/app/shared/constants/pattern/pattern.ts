export class Pattern {
    static readonly NamePattern = '^[a-zA-Z\'-]*$';
    static readonly EmailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    static readonly PasswordPattern = '^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$';
    static readonly AuthorNamePattern = '^[a-zA-Z]+[\-\'\s]?[a-zA-Z ]{1,40}$';
}
