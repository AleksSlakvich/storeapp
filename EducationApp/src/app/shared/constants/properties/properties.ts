export class Properties {
    static readonly FirstName = 'firstname';
    static readonly LastName = 'lastname';
    static readonly Email = 'email';
    static readonly Avatar = 'avatar'
}
