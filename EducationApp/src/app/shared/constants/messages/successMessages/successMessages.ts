export class SuccessMessages {
    static readonly AccountUpdate = 'Profile update successful';
    static readonly PasswordRecovery = 'Password recovery successful';
    static readonly LogIn = 'You logged in';
    static readonly Logout = 'You logged out';
    static readonly AccountAdd = 'Account add successful';
    static readonly AuthorAdd = 'Author add successfull';
    static readonly AuthorUpdate = 'Author update successfull';
    static readonly AuthorDelete = 'Author delete successfull';
    static readonly ProductAdd = 'Product Add to your Cart';
    static readonly PEAdd = 'PrintingEdition add successfull';
    static readonly PEUpdate = 'PrintingEdition update successfull';
    static readonly PEDelete = 'PrintingEdition delete successfull';
    static readonly UserBlock = 'User block successful';
    static readonly UserUnBlock = 'User unblock successful';
    static readonly UserUpdate = 'User update successfull';
    static readonly UserDelete = 'User delete successfull';
}
