export class ErrorMessages {
    static readonly ErrorLogIn = 'Please Log In';
    static readonly EmptyCart = 'Your cart is empty';
}
