import { MatDialogConfig } from '@angular/material';

export class DialogConfig {
    static readonly Height = '200px';
    static readonly Width = '400px';
    static readonly AccountDialogConfig: MatDialogConfig<any> = {
        height: DialogConfig.Height,
        width: DialogConfig.Width,
        disableClose: true,
        data: {}
      };
}
