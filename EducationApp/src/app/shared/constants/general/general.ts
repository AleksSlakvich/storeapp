export class General {
    static readonly CollumnId = 'id';
    static readonly CollumnName = 'name';
    static readonly CollumnStatus = 'status';
    static readonly CollumnAction = 'action';
    static readonly EmptyString = '';
    static readonly One = '1';
    static readonly ApiURL = 'http://localhost:5000/api/';
    static readonly CurrentUser = 'currentUser';
    static readonly CurrentCart = 'currentCartModel';
    static readonly Zero = 0;
    static readonly AccessToken = 'AccessToken';
    static readonly RefreshToken = 'RefreshToken';
    static readonly SessionTime = 'sessionTime';
}
