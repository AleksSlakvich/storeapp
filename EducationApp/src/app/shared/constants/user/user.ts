import { General } from '../general/general';
import { UserFilterModel } from '../../models';

export class User {
    static readonly CollumnEmail = 'email';
    static readonly HeightUser = '500px';
    static readonly WidthUser = '400px';
    static readonly DispalyedCollumns: string[] = [General.CollumnName, User.CollumnEmail, General.CollumnStatus, General.CollumnAction];
    static readonly SelectedButtonsStatus: boolean[] = [true, true];
    static readonly UsersInitFilter: UserFilterModel = { pageIndex: 0, pageSize: 5, searchString: General.EmptyString, statuses: [1, 2], sortColumn: 8, sortType: 1 };
}
