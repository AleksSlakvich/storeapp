export class Actions {
    static readonly Action = 'action';
    static readonly Update = 'Update';
    static readonly Cancel ='Cancel';
    static readonly Add ='Add';
    static readonly Delete = 'Delete';

}
