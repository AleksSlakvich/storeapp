import { General } from '../general/general';
import { AuthorFilterModel, AuthorModel, AuthorModelItem } from '../../models';

export class Author {
    static readonly HeightAuthor = '300px';
    static readonly WidthAuthor = '600px';
    static readonly CollumnPETitle = 'printingEditionTitles';
    static readonly DispalyedCollumns: string[] = [General.CollumnId, General.CollumnName, Author.CollumnPETitle, General.CollumnAction];
    static readonly AuthorInitFilterModel: AuthorFilterModel = {
        pageIndex: 0,
        pageSize: 5,
        searchString: General.EmptyString,
        sortColumn: 1,
        sortType: 1
      };
    static readonly DefaultAuthorModel: AuthorModel = { errors: null, count: 0, items: AuthorModelItem[0] };
}
