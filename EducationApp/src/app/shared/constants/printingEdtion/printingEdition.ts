import { PrintingEditionFilterModel, PaginationModel } from '../../models';
import { General } from '../general/general';
import { SortTypes } from '../sortType/sortType';

export class PrintingEdition {
    static readonly CollumnType = 'category';
    static readonly CollumnDescription = 'description';
    static readonly CollumnAuthNames = 'authorNames';
    static readonly CollumnPrice = 'price';
    static readonly MaxPrice = 100000000;
    static readonly HeightPE = '650px';
    static readonly WidthPE = '900px';
    static readonly ProductsInitPaginationModel: PaginationModel = { pageIndex: 0, pageSize: 6, pageSizeOptions: [12, 6, 24], lenght: 0 };
    static readonly ProductInitFilter: PrintingEditionFilterModel = {
        sortByMaxPrice: 1000000, sortByMinPrice: 0, pageIndex: 0, pageSize: 6,
        searchString: General.EmptyString, categories: [1, 2, 3], sortColumn: 1, sortType: 1, sortCurrency: 1
    };
    static readonly SortTypes: string[] = [SortTypes.PriceLowHigh, SortTypes.PriceHighLow, SortTypes.NameLowHigh, SortTypes.NameHighLow];
    static readonly SelectedButtonTypes: boolean[] = [true, true, true];
    static readonly DisplayedColumns: string[] = [General.CollumnId, General.CollumnName, PrintingEdition.CollumnDescription, PrintingEdition.CollumnType,
        PrintingEdition.CollumnAuthNames, PrintingEdition.CollumnPrice, General.CollumnAction]; 
}
