import { General } from '../general/general';
import { Cart } from '../cart/cart';
import { Author } from '../author/author';
import { User } from '../user/user';
import { PrintingEdition } from '../printingEdtion/printingEdition';
import { OrderFilterModel } from '../../models';

export class Order {
    static readonly CollumnCreateDate = 'creationDate';
    static readonly DispalyedUserCollumns: string[] = [General.CollumnId, Order.CollumnCreateDate, PrintingEdition.CollumnType, Author.CollumnPETitle, Cart.CollumnQuantity, Cart.CollumnAmount,
        General.CollumnStatus];
    static readonly DispalyedAdminCollumns: string[] = [General.CollumnId, Order.CollumnCreateDate, General.CollumnName, User.CollumnEmail, PrintingEdition.CollumnType,
        Author.CollumnPETitle, Cart.CollumnQuantity, Cart.CollumnAmount, General.CollumnStatus];
    static readonly SelectedStatuses: boolean[] = [true, true];
    static readonly OrderInitFilterModel: OrderFilterModel = {
        pageIndex: 0,
        pageSize: 5,
        searchString: '',
        sortColumn: 1,
        sortType: 1,
        orderStatuses: [1, 2]
      };
}
