export class SortTypes {
    static readonly Asc = 'asc';
    static readonly Desc = 'desc';
    static readonly PriceLowHigh = 'Price: Low to High';
    static readonly PriceHighLow = 'Price: High to Low';
    static readonly NameLowHigh = 'Name: A - Z';
    static readonly NameHighLow = 'Name: Z - A';
}
