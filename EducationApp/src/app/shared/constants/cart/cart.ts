import { PrintingEdition } from '../printingEdtion/printingEdition';
import { General } from '../general/general';
import { Author } from '../author/author';

export class Cart {
    static readonly HeightCart = '600px';
    static readonly WidthCart = '700px';
    static readonly CollumnQuantity = 'quantity';
    static readonly CollumnAmount = 'amount';
    static readonly DispalyedCollumns: string[] = [Author.CollumnPETitle, PrintingEdition.CollumnPrice, Cart.CollumnQuantity, Cart.CollumnAmount, General.CollumnAction];
}
