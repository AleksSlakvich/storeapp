import { Component, AfterViewInit, HostBinding } from '@angular/core';
import { UserModelItem } from '../../models/user/userModelItem.model';
import { Router } from '@angular/router';
import { AccountService } from '../../services/account/account.service';
import { AlertService } from '../../services/alert/alert.service';
import { CartModel } from '../../models/cart/cartModel.model';
import { CheckOutDialogComponent } from 'src/app/cart/checkOutDialog/checkOutDialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { LocalStorageService } from '../../services/localStorage/localStorage.service';
import { General, SuccessMessages, ErrorMessages, DialogConfig, Cart, UserRole } from 'src/app/shared/constants'
import { fromEvent } from 'rxjs';
import { throttleTime, map, pairwise, distinctUntilChanged, share, filter } from 'rxjs/operators';
import { Direction } from '../../enums/direction';
import { VisibilityState } from '../../enums/visibilityState';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { DialogHelper } from '../../helpers/dialogHelper';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    trigger('toggle', [
      state(
        VisibilityState.Hidden,
        style({ opacity: 0, transform: 'translateY(-100%)' })
      ),
      state(
        VisibilityState.Visible,
        style({ opacity: 1, transform: 'translateY(0)' })
      ),
      transition('* => *', animate('200ms ease-in'))
    ])
  ]
})

export class HeaderComponent implements AfterViewInit{

  private isVisible = true;
  currentUser: UserModelItem;
  currentCartModel: CartModel;
  private dialogConfig: MatDialogConfig;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private accountService: AccountService,
    private alertService: AlertService,
    private storageService: LocalStorageService,
    private dialogHelper: DialogHelper<CheckOutDialogComponent>
  ) {
    this.storageService.currentUser.subscribe((user: UserModelItem) => this.currentUser = user);
    this.storageService.currentCartModel.subscribe((model: CartModel) => this.currentCartModel = model);
    this.alertService.dialogConfig = { ...this.dialogConfig };
  }

  @HostBinding('@toggle')
  get toggle(): VisibilityState {
    return this.isVisible ? VisibilityState.Visible : VisibilityState.Hidden;
  }

  ngAfterViewInit(): void {
    const scroll$ = fromEvent(window, 'scroll').pipe(
      throttleTime(10),
      map(() => window.pageYOffset),
      pairwise(),
      map(([y1, y2]): Direction => (y2 < y1 ? Direction.Up : Direction.Down)),
      distinctUntilChanged(),
      share()
    );

    const scrollUp$ = scroll$.pipe(
      filter(direction => direction === Direction.Up)
    );

    const scrollDown = scroll$.pipe(
      filter(direction => direction === Direction.Down)
    );

    scrollUp$.subscribe(() => (this.isVisible = true));
    scrollDown.subscribe(() => (this.isVisible = false));
  }

  logOut() {
    this.dialogConfig = {
      height: DialogConfig.Height,
      width: DialogConfig.Width,
      disableClose: true,
      data: {}
    }
    this.accountService.signOut().subscribe((data) => {
      this.accountService.inputSuccessMessage(data, SuccessMessages.Logout, '/signIn');
    });
  }

  openDialog(): void {
    if (!this.checkData(this.currentCartModel, this.currentUser)) {
      this.dialogHelper.openDialog(this.dialog, CheckOutDialogComponent, Cart.HeightCart, Cart.WidthCart, this.currentCartModel);
    }
  }

  getCartCount(): string {
    if (this.currentCartModel == null || this.currentCartModel.items.length==0) {
      return General.EmptyString;
    }
    return this.currentCartModel.items.length.toString();
  }

  checkData(data: CartModel, user: UserModelItem): boolean {
    if (user === null || data == null) {
      this.alertService.error(ErrorMessages.EmptyCart);
      this.router.navigate(['/catalog']);
      return true;
    }
    return false;
  }

  redirect(){
    if (this.currentUser.role == UserRole.Admin)
    {
      this.router.navigate(['/printingEditions']);
    }
    if(this.currentUser.role == UserRole.User)
    {
      this.router.navigate(['/catalog']);
    }
  }
}
