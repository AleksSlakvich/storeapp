import { BaseModel } from '../base/base.model';
import { AuthorModelItem } from './authorModelItem.model';

export class AuthorModel extends BaseModel{
    public items : Array<AuthorModelItem>;
    public count : number;
}
