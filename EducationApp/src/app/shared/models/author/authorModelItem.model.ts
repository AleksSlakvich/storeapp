import { BaseModel } from '../base/base.model';

export class AuthorModelItem extends BaseModel{
    public authorId: number;
    public authorName: string;
    public printingEditionTitles: Array<string>;
}
