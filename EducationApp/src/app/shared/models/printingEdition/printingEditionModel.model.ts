import { PrintingEditionModelItem } from './printingEditionModelItem.model';
import { BaseModel } from '../base/base.model';

export class PrintingEditionModel extends BaseModel{
    public items : Array<PrintingEditionModelItem>;
    public count : number;
}
