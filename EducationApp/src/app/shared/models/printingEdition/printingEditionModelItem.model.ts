import { BaseModel } from '../base/base.model';
import { Currency, ProductType } from 'src/app/shared/enums/index';
import { AuthorModel } from '../author/authorModel.model';


export class PrintingEditionModelItem extends BaseModel{
    public id: number;
    public title: string;
    public description: string;
    public price: number;
    public category: ProductType;
    public currency: Currency;
    public authorNames: Array<string>;
    public authors: AuthorModel;
}
