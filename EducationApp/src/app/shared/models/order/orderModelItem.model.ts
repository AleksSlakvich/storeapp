import { OrderStatus } from '../../enums';
import { BaseModel } from '../base/base.model';
import { OrderItemModel } from '../orderItem/orderItemModel.model';

export class OrderModelItem extends BaseModel{
    public id: number;
    public userName: string;
    public userEmail: string;
    public orderItemModel: OrderItemModel;
    public amount: number;
    public creationDate: Date;
    public status: OrderStatus;
}
