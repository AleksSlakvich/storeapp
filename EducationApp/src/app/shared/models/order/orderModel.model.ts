import { BaseModel } from '../base/base.model';
import { OrderModelItem } from './orderModelItem.model';

export class OrderModel extends BaseModel{
    public items : Array<OrderModelItem>;
    public count : number;
}