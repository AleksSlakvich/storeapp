import { BaseModel } from '../base/base.model';
import { CartModelItem } from './cartModelItem.model';
import { OrderStatus } from '../../enums';

export class CartModel extends BaseModel {
    public items: Array<CartModelItem>;
    public transactionId: string;
    public orderDescription: string;
    public userId: number;
    public orderStatus: OrderStatus;
}
