import { BaseModel } from '../base/base.model';
import { Currency } from '../../enums';

export class CartModelItem extends BaseModel {
    public printingEditionId: number;
    public quantity: number;
    public currency: Currency;
    public printingEditionTitle: string;
    public price: number;
    public amount: number;
}
