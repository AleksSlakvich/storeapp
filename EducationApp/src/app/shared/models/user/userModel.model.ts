import { BaseModel } from '../base/base.model';
import { UserModelItem } from './userModelItem.model';

export class UserModel extends BaseModel {
    public items: Array<UserModelItem>;
    public count: number;

}
