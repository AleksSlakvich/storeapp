import { BaseModel } from '../base/base.model';

export class UserModelItem extends BaseModel{
    
    public id: number;
    public firstName: string;
    public lastName: string;
    public email: string;
    public oldpassword: string;
    public newpassword: string;
    public status: boolean;
    public role: string;
    public image: string;
}
