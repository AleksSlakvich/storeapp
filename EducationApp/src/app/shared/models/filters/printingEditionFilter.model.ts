import { BaseFilterModel } from './base/baseFilter.model';
import {ProductType, ColumnType, SortType, Currency} from 'src/app/shared/enums/index';


export class PrintingEditionFilterModel extends BaseFilterModel{

    public categories : Array<ProductType>;
    public sortType : SortType;
    public sortColumn : ColumnType;
    public sortByMinPrice : number;
    public sortByMaxPrice : number;
    public sortCurrency: Currency;
}
