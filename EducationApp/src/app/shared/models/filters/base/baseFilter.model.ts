export class BaseFilterModel {

    public searchString: string;
    public pageSize: number;
    public pageIndex: number;
}
