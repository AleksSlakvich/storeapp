import { BaseFilterModel } from './base/baseFilter.model';
import { SortType, ColumnType } from '../../enums';

export class AuthorFilterModel extends BaseFilterModel{
    public sortType : SortType;
    public sortColumn : ColumnType;
}
