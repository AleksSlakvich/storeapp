import { BaseFilterModel } from './base/baseFilter.model';
import { SortType, ColumnType, OrderStatus } from '../../enums';

export class OrderFilterModel extends BaseFilterModel{
    public sortType : SortType;
    public sortColumn : ColumnType;
    public orderStatuses: Array<OrderStatus>;
}