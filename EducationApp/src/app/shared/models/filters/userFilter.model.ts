import { BaseFilterModel } from './base/baseFilter.model';
import { SortType, ColumnType, UserStatus } from '../../enums';

export class UserFilterModel extends BaseFilterModel{
    public sortType : SortType;
    public sortColumn : ColumnType;
    public statuses: Array<UserStatus>;
}
