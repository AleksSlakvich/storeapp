import { PrintingEditionModelItem } from '../printingEdition/printingEditionModelItem.model';
import { BaseModel } from '../base/base.model';

export class OrderItemModelItem extends BaseModel{
    public Id: number;
    public orderId: number;
    public count: number;
    public amount: number;
    public printingEdition: PrintingEditionModelItem;
}
