import { BaseModel } from '../base/base.model';
import { OrderItemModelItem } from './orderItemModelItem.model';

export class OrderItemModel extends BaseModel{
    public items : Array<OrderItemModelItem>;
}
