import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnumConverter {

  convertEnumToString(anyEnum: any) {
    let options: string[] = Object.keys(anyEnum);
    options = options.slice(options.length / 2);
    return options;
  }
}