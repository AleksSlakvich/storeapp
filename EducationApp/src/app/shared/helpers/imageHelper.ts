import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Properties } from '../constants/properties/properties';
import { General } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class ImageHelper {

    showPreview(event, input: FormGroup): FormGroup {
        let avatarUrl: string = General.EmptyString;
        const file = (event.target as HTMLInputElement).files[0];
        input.patchValue({
          avatar: file
        });
        input.get(Properties.Avatar).updateValueAndValidity()
        const reader = new FileReader();
        reader.onload = () => {
          avatarUrl = reader.result as string;
          input.patchValue({
            image: avatarUrl
          });
        }
        reader.readAsDataURL(file);
        return input;
      }
}
