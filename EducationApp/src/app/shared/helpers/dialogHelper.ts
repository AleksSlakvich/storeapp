import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ComponentType } from '@angular/cdk/portal';

@Injectable({
    providedIn: 'root'
})
export class DialogHelper<T> {

    openDialog(dialog: MatDialog, component: ComponentType<T>, dialogHeight: string, dialogWidth: string, dialogObject): MatDialogRef<T, any> {
        let dialogRef = dialog.open(component, {
            height: dialogHeight,
            width: dialogWidth,
            disableClose: true,
            data: dialogObject
        });
        return dialogRef;
    }
}
