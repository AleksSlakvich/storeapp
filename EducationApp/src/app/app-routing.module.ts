import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



export const routes: Routes = [
  { path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule)},
  { path: 'printingEdition', loadChildren: () => import('./printingEdition/printingEdition.module').then(m => m.PrintingEditionModule)},
  { path: 'author', loadChildren: () => import('./author/author.module').then(m => m.AuthorModule)},
  { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule)},
  { path: 'order', loadChildren: () => import('./order/order.module').then(m => m.OrderModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
