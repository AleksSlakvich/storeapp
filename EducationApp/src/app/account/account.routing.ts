import { Routes, RouterModule } from '@angular/router';

import { ConfirmPasswordComponent } from './confirmPassword/confirmPassword.component';
import { PasswordRecoveryComponent } from './passwordRecovery/passwordRecovery.component';
import { SignInComponent } from './signIn/signIn.component';
import { SignUpComponent } from './signUp/signUp.component';
import { ConfirmPageComponent } from './confirmPage/confirmPage.component';
import { SuccessRegistrationComponent } from './successRegistration/successRegistration.component';

export const routes: Routes = [
  { path: 'signIn', component: SignInComponent },
  { path: 'signUp', component: SignUpComponent },
  { path: 'passwordRecovery', component: PasswordRecoveryComponent },
  { path: 'confirmPassword', component: ConfirmPasswordComponent },
  { path: 'confirmPage', component: ConfirmPageComponent },
  { path: 'successRegistration', component: SuccessRegistrationComponent }
];

export const AccountRoutes = RouterModule.forChild(routes);
