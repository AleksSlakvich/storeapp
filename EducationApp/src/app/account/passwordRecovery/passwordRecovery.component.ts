import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { MatDialogConfig } from '@angular/material';
import { Pattern, General, DialogConfig, SuccessMessages } from 'src/app/shared/constants'

@Component({
  selector: 'app-passwordRecovery',
  templateUrl: './passwordRecovery.component.html',
  styleUrls: ['./passwordRecovery.component.css']
})
export class PasswordRecoveryComponent {

  inputForm: FormGroup;
  private dialogConfig: MatDialogConfig;
  emailPattern: string;

  constructor(private form: FormBuilder,
    private accountService: AccountService,
    private alertService: AlertService) {
    this.createForm();
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.emailPattern = Pattern.EmailPattern;
  }

  createForm(): void {
    this.inputForm = this.form.group({
      email: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.emailPattern)])
    });
  }

  continue(): void {
    this.dialogConfig = DialogConfig.AccountDialogConfig;
    this.accountService.forgotPassword({ email: this.inputForm.controls.email.value }).subscribe((data: BaseModel) => {
      this.accountService.inputSuccessMessage(data, SuccessMessages.PasswordRecovery, '/confirmPassword');
    });
  }
}
