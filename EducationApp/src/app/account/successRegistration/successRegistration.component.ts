import { Component } from '@angular/core';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { LocalStorageService } from 'src/app/shared/services/localStorage/localStorage.service';

@Component({
  selector: 'app-successRegistration',
  templateUrl: './successRegistration.component.html',
  styleUrls: ['./successRegistration.component.css']
})
export class SuccessRegistrationComponent {

  currentUser: UserModelItem;

  constructor(
    private storageService: LocalStorageService) {
    this.storageService.currentUser.subscribe((user: UserModelItem) => this.currentUser = user);
  }
}
