import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { MustMatch } from 'src/app/shared/helpers/passwordValidator';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { RegistrationModel } from 'src/app/shared/models/account/registration.model';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { MatDialogConfig } from '@angular/material';
import { Pattern, General, Validator, SuccessMessages } from 'src/app/shared/constants'
import { ImageHelper } from 'src/app/shared/helpers/imageHelper';
import { BaseModel } from 'src/app/shared/models';

@Component({
  selector: 'app-signUp',
  templateUrl: './signUp.component.html',
  styleUrls: ['./signUp.component.css']
})
export class SignUpComponent {
  inputForm: FormGroup;
  private dialogConfig: MatDialogConfig;
  signUpModel: RegistrationModel;
  hidePassword: boolean;
  namePattern: string;
  emailPattern: string;
  passwordPattern: string;

  constructor(private form: FormBuilder,
    private accountService: AccountService,
    private alertService: AlertService,
    private imageHelper: ImageHelper) {
    this.createForm();
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.emailPattern = Pattern.EmailPattern;
    this.namePattern = Pattern.NamePattern;
    this.passwordPattern = Pattern.PasswordPattern;
    this.hidePassword = true;
  }

  createForm(): void {
    this.inputForm = this.form.group({
      avatar: [null],
      image: General.EmptyString,
      firstname: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.namePattern)]),
      lastname: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.namePattern)]),
      email: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.emailPattern)]),
      password: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.passwordPattern)]),
      confirmPassword: new FormControl(General.EmptyString, [Validators.required])
    }, {
      validator: MustMatch(Validator.Password, Validator.ConfirmPassword)
    });
  }

  signUp(): void {
    this.accountService.signUp(this.inputForm.value)
      .subscribe(
        (data: UserModelItem) => {
          this.accountService.inputSuccessMessage(data as BaseModel, SuccessMessages.AccountAdd, '/confirmPage');
        });
  }

  showPreview(event): void {
    this.inputForm = this.imageHelper.showPreview(event, this.inputForm);
  }
}
