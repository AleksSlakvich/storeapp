import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { Router } from '@angular/router';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { MatDialogConfig } from '@angular/material';
import { Pattern, General, SuccessMessages, UserRole } from 'src/app/shared/constants'

@Component({
  selector: 'app-signIn',
  templateUrl: './signIn.component.html',
  styleUrls: ['./signIn.component.css']
})

export class SignInComponent {
  inputForm: FormGroup;
  private dialogConfig: MatDialogConfig;
  rememberMe: boolean;
  hidePassword: boolean;
  emailPattern: string;
  passwordPattern: string;

  constructor(
    private form: FormBuilder,
    private accountService: AccountService,
    private alertService: AlertService,
    private router: Router) {
    this.createForm();
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.rememberMe = false;
    this.hidePassword = true;
    this.emailPattern = Pattern.EmailPattern;
    this.passwordPattern = Pattern.PasswordPattern;
  }

  createForm() {
    this.inputForm = this.form.group({
      email: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.emailPattern)]),
      password: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.passwordPattern)])
    });
  }

  signIn(): void {
    this.accountService.signIn({ email: this.inputForm.controls.email.value, password: this.inputForm.controls.password.value, rememberMe: this.rememberMe })
      .subscribe((data: UserModelItem) => {
        this.alertService.success(SuccessMessages.LogIn, true);
        if (data.role === UserRole.User) {
          this.router.navigate(['/printingEdition/catalog']);
        }
        if (data.role === UserRole.Admin) {
          this.router.navigate(['/printingEdition/printingEditions']);
        }
      });
  }
}
