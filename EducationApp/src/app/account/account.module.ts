import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';

import { ConfirmPasswordComponent } from './confirmPassword/confirmPassword.component';
import { PasswordRecoveryComponent } from './passwordRecovery/passwordRecovery.component';
import { SignInComponent } from './signIn/signIn.component';
import { SignUpComponent } from './signUp/signUp.component';
import { ConfirmPageComponent } from './confirmPage/confirmPage.component';
import { SuccessRegistrationComponent } from './successRegistration/successRegistration.component';

import { AccountRoutes } from './account.routing';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    AccountRoutes,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    AvatarModule
  ],
  declarations: [
    ConfirmPasswordComponent,
    PasswordRecoveryComponent,
    SignInComponent,
    SignUpComponent,
    ConfirmPageComponent,
    SuccessRegistrationComponent
  ]
})
export class AccountModule { }
