import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrintingEditionsComponent } from './printingEditions/printingEditions.component';
import { CatalogComponent } from './catalog/catalog.component';
import { SelectedProductComponent } from './selectedProduct/selectedProduct.component';
import { PrintingEditionDialogComponent } from './printingEditionDialog/printingEditionDialog.component';

import { PrintingEditionRoutes } from './printingEdition.routing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    PrintingEditionRoutes,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [
  ],
  declarations: [
    PrintingEditionsComponent,
    PrintingEditionDialogComponent,
    CatalogComponent,
    SelectedProductComponent
  ],
  entryComponents: [
    PrintingEditionDialogComponent
  ]
})
export class PrintingEditionModule { }
