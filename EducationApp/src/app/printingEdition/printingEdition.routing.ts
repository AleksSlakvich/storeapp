import { Routes, RouterModule } from '@angular/router';

import { PrintingEditionsComponent } from './printingEditions/printingEditions.component';
import { CatalogComponent } from './catalog/catalog.component';
import { SelectedProductComponent } from './selectedProduct/selectedProduct.component';


export const routes: Routes = [
  { path: 'printingEditions', component: PrintingEditionsComponent },
  { path: 'catalog', component: CatalogComponent },
  { path: 'selected/:id', component: SelectedProductComponent}
];

export const PrintingEditionRoutes = RouterModule.forChild(routes);
