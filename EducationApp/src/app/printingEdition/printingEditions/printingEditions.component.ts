import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatCheckboxChange, Sort, MatDialogConfig, MatDialogRef } from '@angular/material';
import { PrintingEditionService } from 'src/app/shared/services/printingEdition/printingEdition.service';
import { PrintingEditionModelItem } from 'src/app/shared/models/printingEdition/printingEditionModelItem.model';
import { PrintingEditionDialogComponent } from '../printingEditionDialog/printingEditionDialog.component';
import { PrintingEditionFilterModel } from 'src/app/shared/models/filters/printingEditionFilter.model';
import { ProductType, ColumnType, SortType } from 'src/app/shared/enums/index';
import { PaginationModel } from 'src/app/shared/models/pagination/pagination.model';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { EnumConverter } from 'src/app/shared/helpers/enumConverter';
import { PrintingEditionModel } from 'src/app/shared/models/printingEdition/printingEditionModel.model';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { Actions, DialogConfig, SuccessMessages, PrintingEdition, SortTypes } from 'src/app/shared/constants'
import { DialogHelper } from 'src/app/shared/helpers/dialogHelper';

@Component({
  selector: 'app-catalog',
  templateUrl: './printingEditions.component.html',
  styleUrls: ['./printingEditions.component.css']
})
export class PrintingEditionsComponent implements OnInit {

  productTypes: string[];
  selectedProductTypes: string[];
  displayedColumns: string[];
  printingEditionFilter: PrintingEditionFilterModel;
  dataSource: MatTableDataSource<PrintingEditionModelItem>;
  dialogConfig: MatDialogConfig;
  paginationModel: PaginationModel;
  selectedButtonsOfType: boolean[];
  isLoadingResults: boolean;
  isRateLimitReached: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private printingEditionservice: PrintingEditionService,
    private dialog: MatDialog,
    private alertService: AlertService,
    public enumConverter: EnumConverter,
    private dialogHelper: DialogHelper<PrintingEditionDialogComponent>) {
    this.selectedProductTypes = [];
    this.productTypes = this.enumConverter.convertEnumToString(ProductType);
    this.selectedButtonsOfType = PrintingEdition.SelectedButtonTypes;
    this.paginationModel = new PaginationModel();
    this.printingEditionFilter = PrintingEdition.ProductInitFilter;
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.displayedColumns = PrintingEdition.DisplayedColumns; 
    this.isLoadingResults = true;
    this.isRateLimitReached = false;
    this.dataSource = new MatTableDataSource<PrintingEditionModelItem>();
    this.dataSource.data = [];
  }

  ngOnInit() {
    this.getPrintingEditions();
  }

  applyFilter(filterValue: string): void {
    this.isLoadingResults = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.printingEditionFilter.searchString = filterValue;
    this.getPrintingEditions();
  }

  public handlePage(event: any): void {
    this.isLoadingResults = true;
    this.printingEditionFilter.pageIndex = event.pageIndex + 1;
    this.printingEditionFilter.pageSize = event.pageSize;
    this.getPrintingEditions();
  }

  checkTypeEvent(event: MatCheckboxChange, index: number): void {
    if (!this.selectedButtonsOfType.some(element => element === true)) {
      event.source.toggle();
      this.selectedButtonsOfType[index] = true;
    }
  }

  sortData(sort: Sort): void {
    this.isLoadingResults = true;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0, this.printingEditionFilter.pageIndex = 0);
    this.printingEditionFilter.sortColumn = ColumnType[sort.active];
    if (sort.direction == SortTypes.Asc) {
      this.printingEditionFilter.sortType = SortType.asc;
    }
    if (sort.direction == SortTypes.Desc) {
      this.printingEditionFilter.sortType = SortType.desc;
    }
    this.getPrintingEditions();
  }

  getTypesValues(): void {
    this.isLoadingResults = true;
    this.selectedProductTypes = [];
    for (let i = 0; i < this.selectedButtonsOfType.length; i++) {
      if (this.selectedButtonsOfType[i] === true) {
        this.selectedProductTypes.push(this.productTypes[i]);
      }
    }
    this.printingEditionFilter.categories = [];

    for (let j = 0; j < this.selectedProductTypes.length; j++) {
      this.printingEditionFilter.categories.push(ProductType[this.selectedProductTypes[j]]);
    }
    this.getPrintingEditions();
  }

  getTypeName(object: ProductType): string {
    return ProductType[object];
  }

  getPrintingEditions = (): void => {
    this.printingEditionservice.getFilteredPrintingEditions(this.printingEditionFilter)
      .subscribe((response: PrintingEditionModel) => {
        this.dataSource.data = response.items;
        this.dataSource.sort = this.sort;
        this.paginator.length = response.count;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        if (response.errors.length > 0) {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
        }
      });
  }

  openDialog(action, object): void {
    object.action = action;
    let dialogRef: MatDialogRef<PrintingEditionDialogComponent, any>;
    if (action === Actions.Add || action === Actions.Update) {
      dialogRef = this.dialogHelper.openDialog(this.dialog, PrintingEditionDialogComponent, PrintingEdition.HeightPE, PrintingEdition.WidthPE, object);
    }
    if (action === Actions.Delete) {
      dialogRef = this.dialogHelper.openDialog(this.dialog, PrintingEditionDialogComponent, DialogConfig.Height, DialogConfig.Width, object);
    }

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == Actions.Add) {
        this.createPrintingEdition(result.data);
      }
      if (result.event == Actions.Update) {
        this.updatePrintingEdition(result.data);
      }
      if (result.event == Actions.Delete) {
        this.deletePrintingEdition(result.data);
      }
    });
  }

  updatePrintingEdition(printingEdition: PrintingEditionModelItem): void {
    this.printingEditionservice.updatePrintingEdition(printingEdition).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccessMessage(data, SuccessMessages.PEUpdate));
  }

  deletePrintingEdition(printingEdition: PrintingEditionModelItem): void {
    this.printingEditionservice.deletePrintingEdition(printingEdition).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccessMessage(data, SuccessMessages.PEDelete));
  }

  createPrintingEdition(printingEdition: PrintingEditionModelItem): void {
    this.printingEditionservice.createPrintingEdition(printingEdition).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccessMessage(data, SuccessMessages.PEAdd));
  }

  private outputSuccessMessage(data: BaseModel, successMessage: string): void {
    if (data.errors.length === 0) {
      this.alertService.success(successMessage, true);
      this.getPrintingEditions();
    }
  }
}





