import { Component, OnInit, ViewChild } from '@angular/core';
import { PrintingEditionFilterModel } from 'src/app/shared/models/filters/printingEditionFilter.model';
import { MatPaginator, MatCheckboxChange } from '@angular/material';
import { PrintingEditionModelItem } from 'src/app/shared/models/printingEdition/printingEditionModelItem.model';
import { PaginationModel } from 'src/app/shared/models/pagination/pagination.model';
import { PrintingEditionService } from 'src/app/shared/services/printingEdition/printingEdition.service';
import { EnumConverter } from 'src/app/shared/helpers/enumConverter';
import { ProductType, SortType, ColumnType, Currency } from 'src/app/shared/enums';
import { Router } from '@angular/router';
import { PrintingEditionModel } from 'src/app/shared/models/printingEdition/printingEditionModel.model';
import { SortTypes, PrintingEdition } from 'src/app/shared/constants'

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  productTypes: string[];
  currencies: string[];
  sortTypes: string[];
  selectedProductTypes: string[];
  printingEditions: PrintingEditionModelItem[];
  printingEditionFilter: PrintingEditionFilterModel;
  paginationModel: PaginationModel;
  selectedButtonTypes: boolean[];
  maxPrice: number;
  minPrice: number;
  selectedCurrency: string;
  selectedSortParameters: string;
  isLoadingResults: boolean;
  isRateLimitReached: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private printingEditionservice: PrintingEditionService,
    public converter: EnumConverter,
    private router: Router) {
    this.selectedProductTypes = [];
    this.productTypes = this.converter.convertEnumToString(ProductType);
    this.currencies = this.converter.convertEnumToString(Currency);
    this.sortTypes = PrintingEdition.SortTypes;
    this.selectedButtonTypes = PrintingEdition.SelectedButtonTypes;
    this.paginationModel = PrintingEdition.ProductsInitPaginationModel;
    this.printingEditionFilter = PrintingEdition.ProductInitFilter;
    this.isLoadingResults = true;
    this.isRateLimitReached = false;
  }

  ngOnInit() {
    this.getPrintingEditions();
  }

  applyFilter(filterValue: string): void {
    this.isLoadingResults = true;
    this.printingEditionFilter.searchString = filterValue.trim().toLowerCase();;
    this.getPrintingEditions();
  }

  public handlePage(event: any): void {
    this.isLoadingResults = true;
    this.printingEditionFilter.pageIndex = event.pageIndex + 1;
    this.printingEditionFilter.pageSize = event.pageSize;
    this.getPrintingEditions();
  }

  setMinMaxPrice(): void {
    this.isLoadingResults = true;
    this.printingEditionFilter.sortByMaxPrice = this.maxPrice;
    this.printingEditionFilter.sortByMinPrice = this.minPrice;
    this.getPrintingEditions();
  }

  applyCurrency(): void {
    this.isLoadingResults = true;
    this.printingEditionFilter.sortCurrency = Currency[this.selectedCurrency];
    this.getPrintingEditions();
  }

  applySortParameters(): void {
    this.isLoadingResults = true;
    if (this.selectedSortParameters == SortTypes.PriceLowHigh) {
      this.mapSortParameters(SortType.asc, ColumnType.Price);
    }
    if (this.selectedSortParameters == SortTypes.PriceHighLow) {
      this.mapSortParameters(SortType.desc, ColumnType.Price);
    }
    if (this.selectedSortParameters == SortTypes.NameLowHigh) {
      this.mapSortParameters(SortType.asc, ColumnType.Title);
    }
    if (this.selectedSortParameters == SortTypes.NameHighLow) {
      this.mapSortParameters(SortType.desc, ColumnType.Title);
    }
    this.getPrintingEditions();
  }

  checkTypeEvent(event: MatCheckboxChange, index: number): void {
    this.isLoadingResults = true;
    if (!this.selectedButtonTypes.some(element => element === true)) {
      event.source.toggle();
      this.selectedButtonTypes[index] = true;
    }
    this.selectedProductTypes = [];
    for (let i = 0; i < this.selectedButtonTypes.length; i++) {
      if (this.selectedButtonTypes[i] === true) {
        this.selectedProductTypes.push(this.productTypes[i]);
      }
    }
    this.printingEditionFilter.categories = [];
    for (let i = 0; i < this.selectedProductTypes.length; i++) {
      this.printingEditionFilter.categories.push(ProductType[this.selectedProductTypes[i]]);
    }
    this.getPrintingEditions();
  }

  getTypeName(object: ProductType): string {
    return ProductType[object];
  }

  getPrintingEditions = (): void => {
    this.printingEditionservice.getFilteredPrintingEditions(this.printingEditionFilter)
      .subscribe((response: PrintingEditionModel) => {
        this.paginator.length = response.count;
        this.printingEditions = response.items;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        if (response.errors.length > 0) {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
        }
      });
  }

  getPirntingEdition(printingEditionId: number, currency: Currency): void {
    this.router.navigate(['/printingEdition/selected', printingEditionId], { state: { selectedCurrency: currency } });
  }

  private mapSortParameters(type: SortType, column: ColumnType) {
    this.printingEditionFilter.sortType = type;
    this.printingEditionFilter.sortColumn = column;
  }
}
