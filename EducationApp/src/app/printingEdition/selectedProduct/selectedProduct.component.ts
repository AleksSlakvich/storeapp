import { Component, OnInit } from '@angular/core';
import { Currency } from 'src/app/shared/enums';
import { ActivatedRoute } from '@angular/router';
import { PrintingEditionModelItem } from 'src/app/shared/models/printingEdition/printingEditionModelItem.model';
import { PrintingEditionService } from 'src/app/shared/services/printingEdition/printingEdition.service';
import { CartModelItem } from 'src/app/shared/models/cart/cartModelItem.model';
import { CartModel } from 'src/app/shared/models/cart/cartModel.model';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { FormControl, Validators } from '@angular/forms';
import { CartService } from 'src/app/shared/services/cart/cart.service';
import { LocalStorageService } from 'src/app/shared/services/localStorage/localStorage.service';
import { MatDialogConfig } from '@angular/material';
import { General, SuccessMessages, ErrorMessages } from 'src/app/shared/constants'

@Component({
  selector: 'app-selectedProduct',
  templateUrl: './selectedProduct.component.html',
  styleUrls: ['./selectedProduct.component.css']
})
export class SelectedProductComponent implements OnInit {

  private dialogConfig: MatDialogConfig;
  currentUser: UserModelItem;
  selectedCurrency: Currency;
  currentCurrency: string;
  quantity: FormControl;
  selectedPrintingEdition: PrintingEditionModelItem;
  cartModelItem: CartModelItem;
  cartModel: CartModel;
  isDisabledButton: boolean;
  
  constructor(private activatedRoute: ActivatedRoute,
    private storageService: LocalStorageService,
    private cartService: CartService,
    private alertService: AlertService,
    private printingEditionservice: PrintingEditionService) {
    this.storageService.currentUser.subscribe((user: UserModelItem) => this.currentUser = user);
    this.selectedCurrency = history.state.selectedCurrency;
    this.currentCurrency = Currency[this.selectedCurrency];
    this.quantity = new FormControl(General.One, [Validators.required]);
    this.selectedPrintingEdition = new PrintingEditionModelItem();
  }

  ngOnInit() {
    this.selectedPrintingEdition = this.getPrintingEdition(this.activatedRoute.snapshot.params.id);
    this.isDisabledButton = false;
  }

  getPrintingEdition(id: number): PrintingEditionModelItem {
    this.printingEditionservice.getPrintingEdition(id)
      .subscribe((response: PrintingEditionModelItem) => {
        this.selectedPrintingEdition = response;
      });
    return this.selectedPrintingEdition;
  }

  createCartIrtem(): void {
    if (this.currentUser !== null) {
      this.cartModelItem = new CartModelItem();
      this.cartModelItem.printingEditionId = this.selectedPrintingEdition.id;
      this.cartModelItem.quantity = this.quantity.value;
      this.cartModelItem.currency = this.selectedPrintingEdition.currency;
      this.cartModelItem.printingEditionTitle = this.selectedPrintingEdition.title;
      this.cartModelItem.price = this.selectedPrintingEdition.price;
      this.cartModelItem.amount = this.selectedPrintingEdition.price * this.cartModelItem.quantity;
      this.cartModelItem.errors = [];
      this.cartService.createCartModel(this.cartModelItem, this.currentUser.id);
      this.isDisabledButton = true;
      this.alertService.success(SuccessMessages.ProductAdd, true);
    }
    if (this.currentUser == null) {
      this.alertService.dialogConfig = { ...this.dialogConfig };
      this.alertService.error(ErrorMessages.ErrorLogIn, true);
    }
  }
}
