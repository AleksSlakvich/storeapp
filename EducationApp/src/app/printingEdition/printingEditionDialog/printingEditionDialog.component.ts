import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PrintingEditionModelItem } from 'src/app/shared/models/printingEdition/printingEditionModelItem.model';
import { AuthorService } from 'src/app/shared/services/author/author.service';
import { AuthorModelItem } from 'src/app/shared/models/author/authorModelItem.model';
import { EnumConverter } from 'src/app/shared/helpers/enumConverter';
import { ProductType, Currency } from 'src/app/shared/enums';
import { AuthorModel } from 'src/app/shared/models/author/authorModel.model';
import { Actions, Author } from 'src/app/shared/constants'

@Component({
  selector: 'app-printingEditionDialog',
  templateUrl: './printingEditionDialog.component.html',
  styleUrls: ['./printingEditionDialog.component.css']
})
export class PrintingEditionDialogComponent {

  buttonAction: string;
  dialogResponse: any;
  categories: string[];
  currencies: string[];
  selectedCategory: string;
  selectedCurrency: string;
  authors: AuthorModelItem[];
  authorModel: AuthorModel;

  constructor(public dialogRef: MatDialogRef<PrintingEditionDialogComponent>,
    private authorService: AuthorService,
    public enumConverter: EnumConverter,
    @Inject(MAT_DIALOG_DATA) public data: PrintingEditionModelItem) {
    this.dialogResponse = { ...data };
    this.buttonAction = this.dialogResponse.action;
    this.authors = this.getAllAuthors();
    this.categories = this.enumConverter.convertEnumToString(ProductType);
    this.currencies = this.enumConverter.convertEnumToString(Currency);
    this.selectedCurrency = Currency[data.currency];
    this.selectedCategory = ProductType[data.category];
    this.authorModel = Author.DefaultAuthorModel;
  }

  doAction(): void {
    this.dialogResponse.authors = this.getselectedAuthors(this.dialogResponse.authorNames);
    this.dialogResponse.currency = Currency[this.selectedCurrency];
    this.dialogResponse.category = ProductType[this.selectedCategory];
    this.dialogRef.close({ event: this.buttonAction, data: this.dialogResponse });
  }

  closeDialog(): void {
    this.dialogRef.close({ event: Actions.Cancel });
  }

  getAllAuthors(): AuthorModelItem[] {
    this.authorService.getAllAuthors().subscribe(
      (data: AuthorModel) => {
        if (data.errors.length === 0) {
          this.authors = data.items;
        }
      });
    return this.authors;
  }

  getselectedAuthors(authorNames: string[]): AuthorModel {
    this.authorModel.items = [];
    for (let j = 0; j < authorNames.length; j++) {
      for (let i = 0; i < this.authors.length; i++) {
        if (this.authors[i].authorName === authorNames[j])
          this.authorModel.items.push(this.authors[i]);
      }
    }
    return this.authorModel;
  }
}
