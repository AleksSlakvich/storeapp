import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckOutDialogComponent } from './checkOutDialog/checkOutDialog.component';
import { SuccessPaymentDialogComponent } from './successPaymentDialog/successPaymentDialog.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [CheckOutDialogComponent,
    SuccessPaymentDialogComponent
  ],
  entryComponents: [
    CheckOutDialogComponent,
    SuccessPaymentDialogComponent
  ]
})
export class CartModule { }
