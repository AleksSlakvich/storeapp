import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-successPaymentDialog',
  templateUrl: './successPaymentDialog.component.html',
  styleUrls: ['./successPaymentDialog.component.css']
})
export class SuccessPaymentDialogComponent {

  constructor(private router: Router,
    public dialogRef: MatDialogRef<SuccessPaymentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogResponse: number) {
  }

  closeDialog(): void {
    this.dialogRef.close();
    this.router.navigate(['/orders']);
  }
}
