import { Component, OnInit, Inject, Predicate } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatDialog } from '@angular/material';
import { CartModel } from 'src/app/shared/models/cart/cartModel.model';
import { CartModelItem } from 'src/app/shared/models/cart/cartModelItem.model';
import { CartService } from 'src/app/shared/services/cart/cart.service';
import { OrderStatus } from 'src/app/shared/enums';
import { SuccessPaymentDialogComponent } from '../successPaymentDialog/successPaymentDialog.component';
import { PaymentService } from 'src/app/shared/services/payment/payment.service';
import { OrderService } from 'src/app/shared/services/order/order.service';
import { first } from 'rxjs/operators';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { General, Actions, Cart } from 'src/app/shared/constants'
import { DialogHelper } from 'src/app/shared/helpers/dialogHelper';

@Component({
  selector: 'app-checkOutDialog',
  templateUrl: './checkOutDialog.component.html',
  styleUrls: ['./checkOutDialog.component.css']
})
export class CheckOutDialogComponent implements OnInit {

  responseModel: CartModel = new CartModel();
  displayedColumns: string[]; 
  dataSource: MatTableDataSource<CartModelItem>;
  totalAmount: number;
  transactionId: string;
  orderDescription: string;
  paymentCallBack: Predicate<string>;

  constructor(
    public dialogRef: MatDialogRef<CheckOutDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CartModel,
    private cartService: CartService,
    private dialog: MatDialog,
    private paymentService: PaymentService,
    private orderService: OrderService,
    private dialogHelper: DialogHelper<SuccessPaymentDialogComponent>) {
    this.responseModel = { ...data };
    this.displayedColumns = Cart.DispalyedCollumns;
    this.transactionId = General.EmptyString;
    this.orderDescription = General.EmptyString;
  }

  ngOnInit() {
    this.paymentService.loadStripe();
    this.dataSource = new MatTableDataSource<CartModelItem>();
    this.dataSource.data = this.data.items;
    this.totalAmount = this.getTotalAmount();
    this.paymentCallBack = this.createOrder.bind(this);
  }

  closeDialog(): void {
    this.dialogRef.close({ event: Actions.Cancel });
  }

  deleteProduct(product: CartModelItem): void {
    this.data.items = this.data.items.filter(item => item.printingEditionId !== product.printingEditionId);
    this.cartService.updateCartModel(this.data);
    this.dataSource.data = this.data.items;
    this.totalAmount = this.totalAmount - product.amount;
  }

  getTotalAmount(): number {
    this.totalAmount = 0;
    for (var i = 0; i < this.dataSource.data.length; i++) {
      this.totalAmount = this.totalAmount + (this.dataSource.data[i].quantity * this.dataSource.data[i].price);
    }
    return this.totalAmount;
  }

  createOrder(transactionId: any): void {
    this.responseModel.transactionId = transactionId;
    this.responseModel.userId = this.data.userId;
    this.responseModel.orderDescription = new Date().toLocaleDateString();
    this.responseModel.items = this.dataSource.data;
    this.responseModel.errors = [];
    this.responseModel.orderStatus = OrderStatus.Paid;
    this.orderService.createOrder(this.responseModel).pipe(first())
      .subscribe(
        (data: BaseModel) => {
          if (data.errors.length === 0) {
            this.cartService.refreshCartModel();
            this.dialogRef.close();
            this.createSuccesDialog();
          }
        });
  }

  buy(): void {
    this.paymentService.pay(this.totalAmount, this.paymentCallBack);
  }

  createSuccesDialog(): void {
    this.dialogHelper.openDialog(this.dialog, SuccessPaymentDialogComponent, Cart.HeightCart, Cart.WidthCart, this.responseModel.orderDescription);
  }
}

