import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import { SharedModule } from './shared/shared.module';
import { AccountModule } from './account/account.module';
import { PrintingEditionModule } from './printingEdition/printingEdition.module';
import { AuthorModule } from './author/author.module';
import { UserModule } from './user/user.module';
import { OrderModule } from './order/order.module';
import { CartModule } from './cart/cart.module';

import { AppComponent } from './app.component';
import { CookieService } from 'ngx-cookie-service';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../app/shared/interceptors/JwtInterceptors';
import { ErrorInterceptor } from '../app/shared/interceptors/HttpErrorInterceptor';

import { Module as StripeModule } from "stripe-angular"
import { MaterialModule } from './material/material.module';
import { AvatarModule } from 'ngx-avatar';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    AccountModule,
    BrowserAnimationsModule,
    HttpClientModule,
    PrintingEditionModule,
    AuthorModule,
    UserModule,
    OrderModule,
    CartModule,
    StripeModule.forRoot(),
    MaterialModule,
    AvatarModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    CookieService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
