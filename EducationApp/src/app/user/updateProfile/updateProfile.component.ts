import { Component } from '@angular/core';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { LocalStorageService } from 'src/app/shared/services/localStorage/localStorage.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { MustMatch } from 'src/app/shared/helpers/passwordValidator';
import { MatDialogConfig } from '@angular/material';
import { Pattern, General, Validator, Actions, Properties, DialogConfig, SuccessMessages } from 'src/app/shared/constants'
import { ImageHelper } from 'src/app/shared/helpers/imageHelper';

@Component({
  selector: 'app-updateProfile',
  templateUrl: './updateProfile.component.html',
  styleUrls: ['./updateProfile.component.css']
})
export class UpdateProfileComponent {
  inputForm: FormGroup;
  private dialogConfig: MatDialogConfig;
  currentUser: UserModelItem;
  buttonAction: string;
  hidePassword: boolean;
  namePattern: string;
  emailPattern: string;
  passwordPattern: string;

  constructor(
    private form: FormBuilder,
    private accountService: AccountService,
    private alertService: AlertService,
    private storageService: LocalStorageService,
    private imageHelper: ImageHelper
  ) {
    this.createForm();
    this.storageService.currentUser.subscribe((user: UserModelItem) => this.currentUser = user);
    this.buttonAction = General.EmptyString;
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.namePattern = Pattern.NamePattern;
    this.emailPattern = Pattern.EmailPattern;
    this.passwordPattern = Pattern.PasswordPattern;
    this.hidePassword = true;
  }

  createForm(): void {
    this.inputForm = this.form.group({
      avatar: [null],
      image: General.EmptyString,
      firstname: new FormControl({ value: General.EmptyString, disabled: true }, [Validators.required, Validators.pattern(this.namePattern)]),
      lastname: new FormControl({ value: General.EmptyString, disabled: true }, [Validators.required, Validators.pattern(this.namePattern)]),
      email: new FormControl({ value: General.EmptyString, disabled: true }, [Validators.required, Validators.pattern(this.emailPattern)]),
      oldpassword: new FormControl(General.EmptyString, [Validators.pattern(this.passwordPattern)]),
      newpassword: new FormControl(General.EmptyString, [Validators.pattern(this.passwordPattern)]),
      confirmPassword: new FormControl(General.EmptyString, [Validators.required])
    }, {
      validator: MustMatch(Validator.Password, Validator.ConfirmPassword)
    });
  }

  editProfile(): void {
    this.buttonAction = Actions.Update;
    this.inputForm.get(Properties.FirstName).enable();
    this.inputForm.get(Properties.LastName).enable();
    this.inputForm.get(Properties.Email).enable();
  }

  updateProfile(): void {
    this.dialogConfig = DialogConfig.AccountDialogConfig;
    let updateModel = {
      errors: null,
      id: this.currentUser.id,
      firstName: this.inputForm.controls.firstname.value,
      lastName: this.inputForm.controls.lastname.value,
      email: this.inputForm.controls.email.value,
      oldpassword: this.inputForm.controls.oldpassword.value,
      newpassword: this.inputForm.controls.newpassword.value,
      status: this.currentUser.status,
      role: this.currentUser.role,
      image: this.currentUser.image
    };

    if (this.inputForm.controls.image.value !== General.EmptyString) {
      updateModel.image = this.inputForm.controls.image.value;
    }

    this.accountService.update(updateModel).pipe()
      .subscribe(
        (data: BaseModel) => {
          if (data.errors.length === 0) {
            this.alertService.success(SuccessMessages.AccountUpdate, true);
            this.disableForm();
          }
        });
  }

  cancel(): void {
    this.currentUser = this.storageService.getUser();
    this.disableForm();
    this.inputForm.controls.image.setValue(General.EmptyString);
  }

  showPreview(event): void {
    this.inputForm = this.imageHelper.showPreview(event, this.inputForm);
  }

  private disableForm(): void {
    this.inputForm.get(Properties.FirstName).disable();
    this.inputForm.get(Properties.LastName).disable();
    this.inputForm.get(Properties.Email).disable();
    this.buttonAction = General.EmptyString;
  }
}
