import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EnumConverter } from 'src/app/shared/helpers/enumConverter';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Pattern, General, Actions } from 'src/app/shared/constants'

@Component({
  selector: 'app-userDialog',
  templateUrl: './userDialog.component.html',
  styleUrls: ['./userDialog.component.css']
})
export class UserDialogComponent {

  inputForm: FormGroup;
  buttonAction: string;
  dialogResponse: any;
  namePattern: string;
  emailPattern: string;

  constructor(private form: FormBuilder,
    public dialogRef: MatDialogRef<UserDialogComponent>,
    public enumConverter: EnumConverter,
    @Inject(MAT_DIALOG_DATA) public data: UserModelItem) {
    this.dialogResponse = { ...data };
    this.buttonAction = this.dialogResponse.action;
    this.createForm();
    this.namePattern = Pattern.NamePattern;
    this.emailPattern = Pattern.EmailPattern;
  }

  doAction(): void {
    this.dialogRef.close({ event: this.buttonAction, data: this.dialogResponse });
  }

  closeDialog(): void {
    this.dialogRef.close({ event: Actions.Cancel });
  }

  createForm(): void {
    this.inputForm = this.form.group({
      firstname: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.namePattern)]),
      lastname: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.namePattern)]),
      email: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.emailPattern)])
    });
  }
}
