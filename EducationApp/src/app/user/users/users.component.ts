import { Component, OnInit, ViewChild } from '@angular/core';
import { UserFilterModel } from 'src/app/shared/models/filters/userFilter.model';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatCheckboxChange, Sort, MatDialogConfig } from '@angular/material';
import { UserModelItem } from 'src/app/shared/models/user/userModelItem.model';
import { PaginationModel } from 'src/app/shared/models/pagination/pagination.model';
import { UserService } from 'src/app/shared/services/user/user.service';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { ColumnType, SortType, UserStatus } from 'src/app/shared/enums';
import { first } from 'rxjs/operators';
import { UserDialogComponent } from '../userDialog/userDialog.component';
import { EnumConverter } from 'src/app/shared/helpers/enumConverter';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { UserModel } from 'src/app/shared/models/user/userModel.model';
import { Actions, SuccessMessages, User, SortTypes } from 'src/app/shared/constants'
import { DialogHelper } from 'src/app/shared/helpers/dialogHelper';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[];
  userFilter: UserFilterModel;
  dataSource: MatTableDataSource<UserModelItem>;
  dialogConfig: MatDialogConfig;
  paginationModel: PaginationModel;
  selectedButtonsStatus: boolean[];
  selectedStatuses: string[];
  statuses: string[];
  isLoadingResults: boolean;
  isRateLimitReached: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private userService: UserService,
    private dialog: MatDialog,
    private alertService: AlertService,
    public enumConverter: EnumConverter,
    private dialogHelper: DialogHelper<UserDialogComponent>) {
    this.selectedButtonsStatus = User.SelectedButtonsStatus;
    this.selectedStatuses = [];
    this.statuses = this.enumConverter.convertEnumToString(UserStatus);
    this.paginationModel = new PaginationModel();
    this.userFilter = User.UsersInitFilter;
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.displayedColumns = User.DispalyedCollumns;
    this.isLoadingResults = true;
    this.isRateLimitReached = false;
    this.dataSource = new MatTableDataSource<UserModelItem>();
    this.dataSource.data = [];
  }

  ngOnInit() {
    this.getUsers();
  }

  applyFilter(filterValue: string): void {
    this.isLoadingResults = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.userFilter.searchString = filterValue;
    this.getUsers();
  }

  public handlePage(event: any): void {
    this.isLoadingResults = true;
    this.userFilter.pageIndex = event.pageIndex + 1;
    this.userFilter.pageSize = event.pageSize;
    this.getUsers();
  }

  checkTypeEvent(event: MatCheckboxChange, index: number): void {
    if (!this.selectedButtonsStatus.some(element => element == true)) {
      event.source.toggle();
      this.selectedButtonsStatus[index] = true;
    }
  }

  sortData(sort: Sort): void {
    this.isLoadingResults = true;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0, this.userFilter.pageIndex = 0);
    this.userFilter.sortColumn = ColumnType[sort.active];
    if (sort.direction == SortTypes.Asc) {
      this.userFilter.sortType = SortType.asc;
    }
    if (sort.direction == SortTypes.Desc) {
      this.userFilter.sortType = SortType.desc;
    }
    this.getUsers();
  }

  getTypesValues(): void {
    this.isLoadingResults = true;
    this.selectedStatuses = [];
    for (let i = 0; i < this.selectedButtonsStatus.length; i++) {
      if (this.selectedButtonsStatus[i] === true) {
        this.selectedStatuses.push(this.statuses[i]);
      }
    }
    this.userFilter.statuses = [];
    for (let j = 0; j < this.selectedStatuses.length; j++) {
      this.userFilter.statuses.push(UserStatus[this.selectedStatuses[j]]);
    }
    this.getUsers();
  }

  setValue(event, object): void {
    if (event.checked) {
      this.userService.unblockUser(object).pipe(first())
        .subscribe(
          (data: BaseModel) => this.outputSuccessMessage(data, SuccessMessages.UserUnBlock));
    }
    this.userService.blockUser(object).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccessMessage(data, SuccessMessages.UserBlock));
  }

  getUsers = (): void => {
    this.userService.getFilteredUsers(this.userFilter)
      .subscribe(
        (response: UserModel) => {
          this.dataSource.data = response.items;
          this.dataSource.sort = this.sort;
          this.paginator.length = response.count;
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          if (response.errors.length > 0) {
            this.isLoadingResults = false;
            this.isRateLimitReached = true;
          }
        });
  }

  openDialog(action, object): void {
    object.action = action;
    const dialogRef = this.dialogHelper.openDialog(this.dialog, UserDialogComponent, User.HeightUser, User.WidthUser, object);
    dialogRef.afterClosed().subscribe(result => {
      if (result.event === Actions.Delete) {
        this.deleteUser(result.data);
      }
      if (result.event === Actions.Update) {
        this.updateUser(result.data);
      }
    });
  }

  updateUser(userModelItem: UserModelItem): void {
    this.userService.updateUser(userModelItem).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccessMessage(data, SuccessMessages.UserUpdate));
  }

  deleteUser(userModelItem: UserModelItem): void {
    this.userService.deleteUser(userModelItem).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccessMessage(data, SuccessMessages.UserDelete));
  }

  private outputSuccessMessage(data: BaseModel, successMessage: string): void {
    if (data.errors.length === 0) {
      this.alertService.success(successMessage, true);
      this.getUsers();
    }
  }
}
