import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UpdateProfileComponent } from './updateProfile/updateProfile.component';


export const routes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'updateProfile', component: UpdateProfileComponent}
];

export const UserRoutes = RouterModule.forChild(routes);
