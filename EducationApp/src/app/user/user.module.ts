import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvatarModule } from 'ngx-avatar';

import { UsersComponent } from './users/users.component';
import { UserDialogComponent } from './userDialog/userDialog.component';

import { UserRoutes } from './user.routing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { UpdateProfileComponent } from './updateProfile/updateProfile.component';

@NgModule({
  imports: [
    CommonModule,
    UserRoutes,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AvatarModule
  ],
  exports: [
  ],
  declarations: [
    UsersComponent,
    UserDialogComponent,
    UpdateProfileComponent],
  entryComponents: [
    UserDialogComponent
  ]
})
export class UserModule { }
