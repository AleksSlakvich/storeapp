import { Routes, RouterModule } from '@angular/router';

import { AuthorsComponent } from './authors/authors.component';

const routes: Routes = [
  { path: 'authors', component: AuthorsComponent },
];

export const AuthorRoutes = RouterModule.forChild(routes);
