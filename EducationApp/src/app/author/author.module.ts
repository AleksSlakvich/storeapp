import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';

import { AuthorsComponent } from './authors/authors.component';
import { AuthorDialogComponent } from './authorDialog/authorDialog.component';

import { AuthorRoutes } from './author.routing';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    AuthorRoutes

  ],
  declarations: [AuthorsComponent,
                 AuthorDialogComponent
                ],
  entryComponents: [AuthorDialogComponent
                ]
})
export class AuthorModule { }
