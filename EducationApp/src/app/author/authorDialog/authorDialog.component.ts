import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthorModelItem } from 'src/app/shared/models/author/authorModelItem.model';
import { Pattern, General, Actions } from 'src/app/shared/constants'

@Component({
  selector: 'app-author',
  templateUrl: './authorDialog.component.html',
  styleUrls: ['./authorDialog.component.css']
})
export class AuthorDialogComponent {

  inputForm: FormGroup;
  buttonAction: string;
  dialogResponse: any;
  namePattern: string;

  constructor(public dialogRef: MatDialogRef<AuthorDialogComponent>,
    private form: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: AuthorModelItem) {
    this.dialogResponse = { ...data };
    this.buttonAction = this.dialogResponse.action;
    this.namePattern = Pattern.AuthorNamePattern;
    this.createForm();
  }

  createForm(): void {
    this.inputForm = this.form.group({
      name: new FormControl(General.EmptyString, [Validators.required, Validators.pattern(this.namePattern)])
    });
  }

  doAction(): void {
    this.dialogRef.close({ event: this.buttonAction, data: this.dialogResponse });
  }

  closeDialog(): void {
    this.dialogRef.close({ event: Actions.Cancel });
  }
}
