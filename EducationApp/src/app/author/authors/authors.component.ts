import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, Sort, MatDialogConfig } from '@angular/material';
import { AuthorModelItem } from 'src/app/shared/models/author/authorModelItem.model';
import { AuthorFilterModel } from 'src/app/shared/models/filters/authorFilter.model';
import { PaginationModel } from 'src/app/shared/models/pagination/pagination.model';
import { AuthorService } from 'src/app/shared/services/author/author.service';
import { ColumnType } from 'src/app/shared/enums/columnType';
import { SortType } from 'src/app/shared/enums/sortType';
import { AuthorDialogComponent } from '../authorDialog/authorDialog.component';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { first } from 'rxjs/operators';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { AuthorModel } from 'src/app/shared/models/author/authorModel.model';
import { Actions, SuccessMessages, Author, SortTypes } from 'src/app/shared/constants'
import { DialogHelper } from 'src/app/shared/helpers/dialogHelper';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  displayedColumns: string[];
  authorFilterModel: AuthorFilterModel;
  dataSource: MatTableDataSource<AuthorModelItem>;
  dialogConfig: MatDialogConfig;
  paginationModel: PaginationModel;
  isLoadingResults: boolean;
  isRateLimitReached: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private authorService: AuthorService,
    private dialog: MatDialog,
    private alertService: AlertService,
    private dialogHelper: DialogHelper<AuthorDialogComponent>) {
    this.paginationModel = new PaginationModel();
    this.authorFilterModel = Author.AuthorInitFilterModel;
    this.alertService.dialogConfig = { ...this.dialogConfig };
    this.isLoadingResults = true;
    this.isRateLimitReached = false;
    this.displayedColumns = Author.DispalyedCollumns;
    this.dataSource = new MatTableDataSource<AuthorModelItem>();
    this.dataSource.data = [];
  }

  ngOnInit() {
    this.getAuthors();
  }

  applyFilter(filterValue: string): void {
    this.isLoadingResults = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.authorFilterModel.searchString = filterValue;
    this.getAuthors();
  }

  public handlePage(event: any): void {
    this.isLoadingResults = true;
    this.authorFilterModel.pageIndex = event.pageIndex + 1;
    this.authorFilterModel.pageSize = event.pageSize;
    this.getAuthors();
  }

  sortData(sort: Sort): void {
    this.isLoadingResults = true;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0, this.authorFilterModel.pageIndex = 0);
    this.authorFilterModel.sortColumn = ColumnType[sort.active];
    if (sort.direction == SortTypes.Asc) {
      this.authorFilterModel.sortType = SortType.asc;
    }
    if (sort.direction == SortTypes.Desc) {
      this.authorFilterModel.sortType = SortType.desc;
    }
    this.getAuthors();
  }

  getAuthors = (): void => {
    this.authorService.getFilteredAuthors(this.authorFilterModel)
      .subscribe((response: AuthorModel) => {
        this.dataSource.data = response.items;
        this.dataSource.sort = this.sort;
        this.paginator.length = response.count;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        if (response.errors.length > 0) {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
        }
      });
  }

  openDialog(action, object): void {
    object.action = action;
    const dialogRef = this.dialogHelper.openDialog(this.dialog, AuthorDialogComponent, Author.HeightAuthor, Author.WidthAuthor, object);
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == Actions.Add) {
        this.createAuthor(result.data);
      }
      if (result.event == Actions.Update) {
        this.updateAuthor(result.data);
      }
      if (result.event == Actions.Delete) {
        this.deleteAuthor(result.data);
      }
    });
  }

  createAuthor(authorModelItem: AuthorModelItem): void {
    this.authorService.createAuthor(authorModelItem).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccesMessage(data, SuccessMessages.AuthorAdd));
  }

  updateAuthor(authorModelItem: AuthorModelItem): void {
    this.authorService.updateAuthor(authorModelItem).pipe(first())
      .subscribe(
        (data: BaseModel) => this.outputSuccesMessage(data, SuccessMessages.AuthorUpdate));
  }

  deleteAuthor(authorModelItem: AuthorModelItem): void {
    this.authorService.deleteAuthor(authorModelItem).subscribe(
      (data: BaseModel) => this.outputSuccesMessage(data, SuccessMessages.AuthorDelete));
  }

  private outputSuccesMessage(data: BaseModel, successMessage: string): void {
    if (data.errors.length === 0) {
      this.alertService.success(successMessage, true);
      this.getAuthors();
    }
  }
}
