﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Mappers.Filters;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.User;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static StoreApp.BusinessLogicLayer.Mappers.Mappers;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userReposotiry;

        public UserService(IUserRepository userReposotiry)
        {
            _userReposotiry = userReposotiry;
        }

        public async Task<BaseModel> UpdateAsync(UserModelItem model)
        {
            var resultModel = await CheckAsync(model);
            var existedUser = await _userReposotiry.FindByIdAsync(model.Id);
            var existedEmailUser = await _userReposotiry.FindByEmailAsync(model.Email);

            if (existedEmailUser.Id != model.Id)
            {
                resultModel.Errors.Add(UserErrors.EngagedEmail);
                return resultModel;
            }

            existedUser = UserMapper.MapModelToExistedUser(existedUser, model);

            if (!model.Status)
            {
                existedUser.Status = Enums.UserStatus.Blocked;
            }

            var result = await _userReposotiry.UpdateAsync(existedUser);

            if (!result.Succeeded)
            {
                resultModel.Errors.Add(UserErrors.NotUpdate);
                return resultModel;
            }

            if (string.IsNullOrWhiteSpace(model.OldPassword) && string.IsNullOrWhiteSpace(model.NewPassword))
            {
                return resultModel;
            }

            resultModel = await ChangePasswordAsync(resultModel, model, existedUser);
            return resultModel;
        }

        private async Task<BaseModel> CheckAsync(UserModelItem model)
        {
            var resultModel = new BaseModel();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
            }
            var existedUser = await _userReposotiry.FindByIdAsync(model.Id);
            if (existedUser == null)
            {
                resultModel.Errors.Add(UserErrors.NotFind);
            }
            if (existedUser.IsRemoved)
            {
                resultModel.Errors.Add(UserErrors.DeletedUser);
            }
            return resultModel;
        }

        private async Task<BaseModel> ChangePasswordAsync(BaseModel baseModel, UserModelItem model, ApplicationUser user)
        {
            var confirmPassword = await _userReposotiry.CheckPasswordSignInAsync(user, model.OldPassword);
            if (!confirmPassword.Succeeded)
            {
                baseModel.Errors.Add(UserErrors.IncorrectPassword);
                return baseModel;
            }
            var resultAction = await _userReposotiry.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!resultAction.Succeeded)
            {
                baseModel.Errors.Add(UserErrors.PasswordNotChanged);
            }
            return baseModel;
        }

        public async Task<BaseModel> BlockAsync(UserModelItem model)
        {
            var resultModel = await CheckAsync(model);
            var user = await _userReposotiry.FindByIdAsync(model.Id);
            var result = await _userReposotiry.SetLockoutEndDateAsync(user, DateTime.MaxValue);
            if (!result.Succeeded)
            {
                resultModel.Errors.Add(UserErrors.NotBlock);
            }
            return resultModel;
        }

        public async Task<BaseModel> UnBlockAsync(UserModelItem model)
        {
            var resultModel = await CheckAsync(model);
            var user = await _userReposotiry.FindByIdAsync(model.Id);
            user.LockoutEnd = null;
            user.Status = Enums.UserStatus.Active;
            var result = await _userReposotiry.UpdateAsync(user);
            if (!result.Succeeded)
            {
                resultModel.Errors.Add(UserErrors.NotUnBlock);
                return resultModel;
            }
            return resultModel;
        }

        public async Task<BaseModel> DeleteAsync(UserModelItem model)
        {
            var resultModel = await CheckAsync(model);
            var user = await _userReposotiry.FindByIdAsync(model.Id);
            user.IsRemoved = true;
            var result = await _userReposotiry.UpdateAsync(user);
            if (!result.Succeeded)
            {
                resultModel.Errors.Add(UserErrors.NotDelete);
            }
            return resultModel;
        }

        public async Task<UserModel> GetFilteredAsync(UserFilterModel userFilterModel)
        {
            var model = UserFilter.MapUserFilterModelToFilterUser(userFilterModel);
            var filteredModel = await _userReposotiry.GetUsersAsync(model);
            var response = Mappers.Filters.Mappers.ResponseDataMapper<List<ApplicationUser>>.MapResponseModelToResponseDataModel(filteredModel);
            var result = new UserModel();
            result.Count = response.ItemsCount;
            foreach (var user in response.Data)
            {
                var userModelItem = UserMapper.MapEntityToUserModelItem(user);
                result.Items.Add(userModelItem);
            }
            return result;
        }
    }
}
