﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Helpers.Interfaces;
using StoreApp.BusinessLogicLayer.Mappers.Filters;
using StoreApp.BusinessLogicLayer.Models.Author;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.Enumes;
using StoreApp.BusinessLogicLayer.Models.PrintingEditions;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static StoreApp.BusinessLogicLayer.Mappers.Mappers;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.BusinessLogicLayer.Services
{
    public class PrintingEditionService : IPrintingEditionService
    {
        private readonly IPrintingEditionRepository _printingeditionReposotiry;
        private readonly IAuthorInPrintingEditionRepository _authorInPrintingEditionRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly ICurrencyConverter _currencyCoverter;
        public PrintingEditionService(IPrintingEditionRepository printingeditionReposotiry, IAuthorRepository authorRepository,
                                      IAuthorInPrintingEditionRepository authorInPrintingEditionRepository, ICurrencyConverter currencyCoverter)
        {
            _printingeditionReposotiry = printingeditionReposotiry;
            _authorInPrintingEditionRepository = authorInPrintingEditionRepository;
            _authorRepository = authorRepository;
            _currencyCoverter = currencyCoverter;
        }

        public async Task<BaseModel> CreateAsync(PrintingEditionModelItem model)
        {
            var resultModel = new BaseModel();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }

            var printingEdition = PrintingEditionMapper.MapPrintingEditionModelItemToEntity(model);
            printingEdition.Price = _currencyCoverter.ConvertCurrency(model.Currency, Enumes.Currency.USD, model.Price);
            var result = await _printingeditionReposotiry.CreateAsync(printingEdition);

            if (!result)
            {
                resultModel.Errors.Add(PrintingEditionErrors.NotCreate);
                return resultModel;
            }

            resultModel = await CreateAuthorInPrintingEditionAsync(model.Authors.Items, printingEdition, resultModel);
            return resultModel;
        }

        public async Task<BaseModel> DeleteAsync(PrintingEditionModelItem model)
        {
            var resultModel = new BaseModel();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }
            var printingEdition = await _printingeditionReposotiry.GetByIdAsync(model.Id);

            if (printingEdition == null)
            {
                resultModel.Errors.Add(PrintingEditionErrors.NotFind);
            }
            var result = await _printingeditionReposotiry.DeleteAsync(printingEdition);
            if (!result)
            {
                resultModel.Errors.Add(PrintingEditionErrors.NotDelete);
                return resultModel;
            }

            var authorInPrintingEditions = await _authorInPrintingEditionRepository.GetByPrintingEditionIdAsync(printingEdition.Id);
            resultModel = await DeleteAuthorInPrintigEditionAsync(authorInPrintingEditions, resultModel);
            return resultModel;
        }

        public async Task<BaseModel> UpdateAsync(PrintingEditionModelItem model)
        {
            var resultModel = new BaseModel();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }

            var printingEdition = await _printingeditionReposotiry.GetByIdAsync(model.Id);

            if (printingEdition == null)
            {
                resultModel.Errors.Add(PrintingEditionErrors.NotFind);
            }

            if (printingEdition.IsRemoved)
            {
                resultModel.Errors.Add(PrintingEditionErrors.DeletedPI);
                return resultModel;
            }
            printingEdition.Currency = (Enums.Currency)model.Currency;
            printingEdition.Description = model.Description;
            printingEdition.Price = _currencyCoverter.ConvertCurrency(model.Currency, Enumes.Currency.USD, model.Price);
            printingEdition.Title = model.Title;
            printingEdition.Type = (Enums.ProductType)model.Category;
            var result = await _printingeditionReposotiry.UpdateAsync(printingEdition);

            if (!result)
            {
                resultModel.Errors.Add(PrintingEditionErrors.NotUpdate);
                return resultModel;
            }

            var authorInPrintingEditions = await _authorInPrintingEditionRepository.GetByPrintingEditionIdAsync(printingEdition.Id);
            resultModel = await DeleteAuthorInPrintigEditionAsync(authorInPrintingEditions, resultModel);
            resultModel = await CreateAuthorInPrintingEditionAsync(model.Authors.Items, printingEdition, resultModel);
            return resultModel;
        }

        private async Task<BaseModel> DeleteAuthorInPrintigEditionAsync(IEnumerable<AuthorInPrintingEdition> authorInPrintingEditions,
                                                                   BaseModel resultModel)
        {
            foreach (var authorInPrintingEdition in authorInPrintingEditions)
            {
                var resultItem = await _authorInPrintingEditionRepository.RemoveAsync(authorInPrintingEdition);
                if (!resultItem)
                {
                    resultModel.Errors.Add(PrintingEditionErrors.NotDeleteAIP + authorInPrintingEdition.Id.ToString());
                    continue;
                }
            }
            return resultModel;
        }

        private async Task<BaseModel> CreateAuthorInPrintingEditionAsync(ICollection<AuthorModelItem> authorModels, PrintingEdition printingEdition,
                                                                         BaseModel resultModel)
        {
            foreach (var modelAuthor in authorModels)
            {
                int counter = 0;
                var author = await _authorRepository.GetByIdAsync(modelAuthor.AuthorId);
                if (author == null)
                {
                    resultModel.Errors.Add($"{BaseErrors.EmptyDataError} in model {counter}");
                    continue;
                }
                var authorInPrintingEdition = AuthorInPrintingEditionMapper.MapAuthorPrintingEditionToAuthorInPrintingEdition(printingEdition, author);
                var authorResult = await _authorInPrintingEditionRepository.CreateAsync(authorInPrintingEdition);
                if (!authorResult)
                {
                    resultModel.Errors.Add($"{PrintingEditionErrors.NotCreateAIP} {counter}");
                    continue;
                }
            }
            return resultModel;
        }

        public async Task<PrintingEditionModel> GetFilteredAsync(PrintingEditionFilterModel printingEditionFilterModel)
        {
            var model = PrintingEditionFilter.MapPrintingEditionFilterModelToFilterPrintingEdition(printingEditionFilterModel);
            var filteredModel = await _authorInPrintingEditionRepository.GetPrintingEditionsAsync(model);
            var response = Mappers.Filters.Mappers.ResponseDataMapper<List<PrintingEdition>>.MapResponseModelToResponseDataModel(filteredModel);
            var result = new PrintingEditionModel();
            result.Count = response.ItemsCount;
            foreach (var product in response.Data)
            {
                var printingEditionModelItem = PrintingEditionMapper.MapEntityWithAuthorsModelToPrintingEditionModelItem(product);
                printingEditionModelItem.Price = _currencyCoverter.ConvertCurrency(Enumes.Currency.USD, printingEditionFilterModel.SortCurrency, printingEditionModelItem.Price);
                printingEditionModelItem.Currency = printingEditionFilterModel.SortCurrency;
                result.Items.Add(printingEditionModelItem);
            }
            if (!result.Items.Any())
            {
                result.Errors.Add($"{PrintingEditionErrors.NotFound}");
            }
            return result;
        }

        public async Task<PrintingEditionModelItem> GetByPrintingEditionIdAsync(long productId)
        {
            var printingEdition = await _printingeditionReposotiry.GetByIdAsync(productId);
            var result = PrintingEditionMapper.MapEntityWithAuthorsModelToPrintingEditionModelItem(printingEdition);
            var authorInPrintingEditions = await _authorInPrintingEditionRepository.GetByPrintingEditionIdAsync(productId);
            result.AuthorNames = new List<string>();
            foreach (var authorInPrintingEdition in authorInPrintingEditions)
            {
                var author = AuthorMapper.MapEntityToAuthorsModelItem(authorInPrintingEdition.Author);
                result.AuthorNames.Add(author.AuthorName);
            }
            if (result.Errors.Any())
            {
                result.Errors.Add($"{PrintingEditionErrors.NotFind}");
            }
            return result;
        }
    }
}
