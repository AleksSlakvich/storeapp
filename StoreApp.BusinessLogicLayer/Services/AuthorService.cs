﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Mappers.Filters;
using StoreApp.BusinessLogicLayer.Models.Author;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static StoreApp.BusinessLogicLayer.Mappers.Mappers;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.BusinessLogicLayer.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorReposotiry;
        private readonly IAuthorInPrintingEditionRepository _authorInPrintingEditionRepository;
        public AuthorService(IAuthorRepository authorReposotiry, IAuthorInPrintingEditionRepository authorInPrintingEditionRepository)
        {
            _authorReposotiry = authorReposotiry;
            _authorInPrintingEditionRepository = authorInPrintingEditionRepository;
        }

        public async Task<BaseModel> CreateAsync(AuthorModelItem model)
        {          
            var resultModel = new BaseModel();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }

            var author = AuthorMapper.MapAuthorsModelItemToAuthor(model);
            var result = await _authorReposotiry.CreateAsync(author);

            if (!result)
            {
                resultModel.Errors.Add(AuthorErrors.NotCreate);
            }
            return resultModel;
        }

        public async Task<BaseModel> DeleteAsync(AuthorModelItem model)
        {
            var resultModel = await CheckAsync(model);
            var authorItem = await _authorReposotiry.GetByIdAsync(model.AuthorId);
            var result = await _authorReposotiry.DeleteAsync(authorItem);
            if (!result)
            {
                resultModel.Errors.Add(AuthorErrors.NotDelete);
            }
            return resultModel;
        }

        public async Task<BaseModel> UpdateAsync(AuthorModelItem model)
        {
            var resultModel = await CheckAsync(model);
            var author = await _authorReposotiry.GetByIdAsync(model.AuthorId);
            if (!author.IsRemoved)
            {
                resultModel.Errors.Add(AuthorErrors.DeletedAuthor);
                return resultModel;
            }
            author.Name = model.AuthorName;
            var result = await _authorReposotiry.UpdateAsync(author);
            if (!result)
            {
                resultModel.Errors.Add(AuthorErrors.NotUpdate);
            }
            return resultModel;
        }

        public async Task<AuthorModel> GetFilteredAsync(AuthorFilterModel authorFilterModel)
        {
            var model = AuthorFilterMapper.MapAuthorFilterModelToFilterAuthor(authorFilterModel);
            var filteredModel = await _authorInPrintingEditionRepository.GetAuthorsAsync(model);
            var resultCollection = Mappers.Filters.Mappers.ResponseDataMapper<List<Author>>.MapResponseModelToResponseDataModel(filteredModel);
            var result = new AuthorModel();
            result.Count = resultCollection.ItemsCount;
            foreach (var resultItem in resultCollection.Data)
            {
                var authorModelItem = AuthorMapper.MapEntityToAuthorsModelItem(resultItem);
                result.Items.Add(authorModelItem);
            }
            if (!result.Items.Any())
            {
                result.Errors.Add($"{AuthorErrors.NotFoundAuthors}");
            }
            return result;
        }

        public async Task<AuthorModel> GetAllAuthorsAsync()
        {
            var resultCollection = await _authorReposotiry.GetAllAuthorsAsync();
            var result = new AuthorModel();
            result.Count = resultCollection.Count();
            foreach (var resultItem in resultCollection)
            {
                var authorModelItem = AuthorMapper.MapEntityToAuthorsModelItem(resultItem);
                result.Items.Add(authorModelItem);
            }
            if (!result.Items.Any())
            {
                result.Errors.Add($"{AuthorErrors.NotFoundAuthors}");
            }
            return result;
        }

        private async Task<BaseModel> CheckAsync (AuthorModelItem model)
        {
            var resultModel = new BaseModel();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }
            var author = await _authorReposotiry.GetByIdAsync(model.AuthorId);
            if (author == null)
            {
                resultModel.Errors.Add(AuthorErrors.NotFoundAuthor);
            }
            return resultModel;
        }
    }
}
