﻿using Microsoft.AspNetCore.Mvc;
using StoreApp.BusinessLogicLayer.Helpers;
using StoreApp.BusinessLogicLayer.Models.Account;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.User;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using System.Threading.Tasks;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;
using Mapper = StoreApp.BusinessLogicLayer.Mappers.Mappers.UserMapper;

namespace StoreApp.BusinessLogicLayer.Services
{
    public class AccountService : ControllerBase, IAccountService
    {
        private readonly IUserRepository _userReposotiry;
        private readonly IPasswordGeneratorHelper _passwordGenerator;
        private readonly IEmailHelper _emailSender;

        public AccountService(IUserRepository userReposotiry, IPasswordGeneratorHelper passwordGenerator, IEmailHelper emailSender)
        {
            _userReposotiry = userReposotiry;
            _passwordGenerator = passwordGenerator;
            _emailSender = emailSender;
        }

        public async Task<UserModelItem> SignInAsync(LogInModel model)
        {
            var resultModel = new UserModelItem();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }
            if (string.IsNullOrWhiteSpace(model.Email))
            {
                resultModel.Errors.Add(UserErrors.EmptyEmail);
                return resultModel;
            }
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                resultModel.Errors.Add(UserErrors.EmptyPassword);
                return resultModel;
            }
            var user = await _userReposotiry.FindByEmailAsync(model.Email);
            if (user == null)
            {
                resultModel.Errors.Add(UserErrors.NotFind);
                return resultModel;
            }
            var result = await _userReposotiry.CheckPasswordSignInAsync(user, model.Password);
            if (!result.Succeeded)
            {
                resultModel.Errors.Add(UserErrors.IncorrectPassword);
                return resultModel;
            }
            resultModel = Mapper.MapEntityToUserModelItem(user);
            resultModel.Role = await _userReposotiry.GetRoleAsync(user);;
            return resultModel;
        }

        public async Task<BaseModel> SignOutAsync()
        {
            var resultModel = new BaseModel();
            await _userReposotiry.SignOutAsync();
            return resultModel;
        }

        public async Task<UserModelItem> GetByIdAsync(long id)
        {
            var resultModel = new UserModelItem();
            var user = await _userReposotiry.FindByIdAsync(id);
            if (user == null)
            {
                resultModel.Errors.Add(UserErrors.NotFind);
                return resultModel;
            }
            resultModel = Mapper.MapEntityToUserModelItem(user);
            return resultModel;
        }

        public async Task<UserModelItem> SignUpAsync(RegistrationModel model)
        {
            var resultModel = new UserModelItem();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }
            if (string.IsNullOrWhiteSpace(model.Email))
            {
                resultModel.Errors.Add(UserErrors.EmptyEmail);
                return resultModel;
            }
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                resultModel.Errors.Add(UserErrors.EmptyPassword);
                return resultModel;
            }
            var existedUser = await _userReposotiry.FindByEmailAsync(model.Email);
            if (existedUser != null)
            {
                resultModel.Errors.Add(UserErrors.ExistedUser);
                return resultModel;
            }

            var user = Mapper.MapRegistrationModelToApplicationUser(model);
            var result = await _userReposotiry.CreateAsync(user, model.Password);
            if (!result.Succeeded)                      
            {
                resultModel.Errors.Add(UserErrors.NotSignUp);
                return resultModel;
            }
            await _userReposotiry.AddToRoleAsync(user, UserRoles.User);
            resultModel = Mapper.MapEntityToUserModelItem(user);
            
            return resultModel;
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(UserModelItem model)
        {
            var user = await _userReposotiry.FindByEmailAsync(model.Email);
            return await _userReposotiry.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<BaseModel> ConfirmEmailAsync(long userId, string code)
        {
            var resultModel = new BaseModel();
            if (userId == 0)
            {
                resultModel.Errors.Add(UserErrors.IncorrectUserId);
                return resultModel;
            }
            if (string.IsNullOrWhiteSpace(code))
            {
                resultModel.Errors.Add(UserErrors.IncorrectCode);
                return resultModel;
            }
            var user = await _userReposotiry.FindByIdAsync(userId);
            if (user == null)
            {
                resultModel.Errors.Add(UserErrors.NotFind);
                return resultModel;
            }
            var result = await _userReposotiry.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                await _userReposotiry.SignInAsync(user);
                return resultModel;
            }
            resultModel.Errors.Add(UserErrors.EmailNotConfirmed);
            return resultModel;
        }

        public async Task<BaseModel> ForgotPasswordAsync(ResetPasswordModel model)
        {
            var resultModel = new BaseModel();
            if (model == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }
            if (string.IsNullOrWhiteSpace(model.Email))
            {
                resultModel.Errors.Add(UserErrors.EmptyEmail);
                return resultModel;
            }
            var user = await _userReposotiry.FindByEmailAsync(model.Email);
            if (user == null)
            {
                resultModel.Errors.Add(UserErrors.NotFind);
                return resultModel;
            }
            var newpassword = _passwordGenerator.GenerateRandomPassword();

            var token = await _userReposotiry.GeneratePasswordResetTokenAsync(user);
            var result = await _userReposotiry.ResetPasswordAsync(user, token, newpassword);
            if (!result.Succeeded)
            {
                resultModel.Errors.Add(UserErrors.PasswordNotChanged);
                return resultModel;
            }
            await _emailSender.SendEmailAsync(model.Email, EmailMessage.PasswordChanged, $"{EmailMessage.NewPassword} {newpassword}");
            return resultModel;
        }

        public async Task SendConfirmEmailAsync(string email, string callbackUrl)
        {
            await _emailSender.SendEmailAsync(email, EmailMessage.ConfirmAcc,
                $"{EmailMessage.CompleteReg} <a href='{callbackUrl}'>link</a>");
        }
    }
}
