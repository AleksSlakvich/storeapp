﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Mappers.Filters;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.Cart;
using StoreApp.BusinessLogicLayer.Models.Orders;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static StoreApp.BusinessLogicLayer.Mappers.Mappers;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IUserRepository _userRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IPrintingEditionRepository _printingEditionRepository;

        public OrderService(IOrderRepository orderReposotiry, IPaymentRepository paymentRepository,
                            IUserRepository userRepository, IPrintingEditionRepository printingEditionRepository, IOrderItemRepository orderItemRepository)
        {
            _orderRepository = orderReposotiry;
            _paymentRepository = paymentRepository;
            _userRepository = userRepository;
            _orderItemRepository = orderItemRepository;
            _printingEditionRepository = printingEditionRepository;
        }

        public async Task<BaseModel> CreateAsync(CartModel cartModel)
        {
            var resultModel = CheckModel(cartModel);
            if (resultModel.Errors.Any())
            {
                return resultModel;
            }
            var user = await _userRepository.FindByIdAsync(cartModel.UserId);
            if (user == null)
            {
                resultModel.Errors.Add(UserErrors.NotFind);
            }
            var payment = CartMapper.MapCartModelToPayment(cartModel);
            var paymentResult = await _paymentRepository.CreateAsync(payment);
            if (!paymentResult)
            {
                resultModel.Errors.Add(OrderErrors.NotCreatePay);
                return resultModel;
            }

            var order = CartMapper.MapCartModelToOrder(cartModel);
            order.User = user;
            order.Payment = payment;
            order.OrderAmount = 0;
            foreach (var cartModelItem in cartModel.Items)
            {
                order.OrderAmount += cartModelItem.Amount;
            }
            var resultOrder = await _orderRepository.CreateAsync(order);
            if (!resultOrder)
            {
                resultModel.Errors.Add(OrderErrors.NotCreateOrder);
            }
            resultModel = await CreateOrderItemsAsync(cartModel.Items, order);
            
            return resultModel;
        }
        public async Task<OrderModel> GetFilteredAsync(OrderFilterModel orderFilterModel)
        {
            var model = OrderFilterMapper.MapOrderFilterModelToFilterOrder(orderFilterModel);
            var responseDataModel = await _orderRepository.GetOrdersAsync(model); 
            var orderResponse = Mappers.Filters.Mappers.ResponseDataMapper<List<Order>>.MapResponseModelToResponseDataModel(responseDataModel);
            var result = new OrderModel();
            result.Count = orderResponse.ItemsCount;
            foreach (var order in orderResponse.Data)
            {
                var orderModelItem = OrderMapper.MapEntityToOrderModelItem(order);
                foreach (var orderItem in order.OrderItems)
                {
                    var orderItemModelItem = OrderItemMapper.MapEntityToOrderItemModelItem(orderItem);
                    orderModelItem.OrderItemModel.Items.Add(orderItemModelItem);
                }
                result.Items.Add(orderModelItem);
            }

            if (!result.Items.Any())
            {
                result.Errors.Add($"{OrderErrors.NotFoundOrders}");
            }
            return result;
        }

        private BaseModel CheckModel(CartModel cartModel)
        {
            var resultModel = new BaseModel();
            if (cartModel == null)
            {
                resultModel.Errors.Add(BaseErrors.EmptyDataError);
                return resultModel;
            }
            if (string.IsNullOrWhiteSpace(cartModel.TransactionId))
            {
                resultModel.Errors.Add(OrderErrors.NotValidTrans);
            }
            return resultModel;
        }

        private async Task<BaseModel> CreateOrderItemsAsync(ICollection<CartModelItem> cartModelItems, Order order)
        {
            var resultModel = new BaseModel();
            foreach (var cartModelItem in cartModelItems)
            {
                int counter = 0;
                if (cartModelItem == null)
                {
                    resultModel.Errors.Add($"{BaseErrors.EmptyDataError} OrderItem {counter}");
                    continue;
                }
                var orderItem = CartMapper.MapCartModelItemToOrderItem(cartModelItem);
                orderItem.Order = order;
                var printingEdition = await _printingEditionRepository.GetByIdAsync(cartModelItem.PrintingEditionId);
                orderItem.PrintingEdition = printingEdition;
                orderItem.Amount = orderItem.Count * orderItem.PrintingEdition.Price;
                var resultItem = await _orderItemRepository.CreateAsync(orderItem);
                if (!resultItem)
                {
                    resultModel.Errors.Add($"{OrderErrors.NotCreateOrderItem} {counter}");
                    continue;
                }
            }
            return resultModel;
        }
    }
}
