﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.Author;
using StoreApp.BusinessLogicLayer.Models.Base;
using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IAuthorService
    {
        Task<BaseModel> CreateAsync(AuthorModelItem model);
        Task<BaseModel> DeleteAsync(AuthorModelItem model);
        Task<BaseModel> UpdateAsync(AuthorModelItem model);
        Task<AuthorModel> GetFilteredAsync(AuthorFilterModel authorFilterModel);
        Task<AuthorModel> GetAllAuthorsAsync();
    }
}
