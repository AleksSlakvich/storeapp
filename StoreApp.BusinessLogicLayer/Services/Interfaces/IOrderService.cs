﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.Cart;
using StoreApp.BusinessLogicLayer.Models.Orders;
using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IOrderService
    {
        Task<BaseModel> CreateAsync(CartModel cartModel);
        Task<OrderModel> GetFilteredAsync(OrderFilterModel orderFilterModel);
    }
}
