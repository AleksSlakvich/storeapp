﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.PrintingEditions;
using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IPrintingEditionService
    {
        Task<BaseModel> CreateAsync(PrintingEditionModelItem model);
        Task<BaseModel> DeleteAsync(PrintingEditionModelItem model);
        Task<BaseModel> UpdateAsync(PrintingEditionModelItem model);
        Task<PrintingEditionModel> GetFilteredAsync(PrintingEditionFilterModel printingEditionFilterModel);
        Task<PrintingEditionModelItem> GetByPrintingEditionIdAsync(long productId);
    }
}
