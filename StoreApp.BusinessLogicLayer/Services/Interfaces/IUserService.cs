﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.User;
using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IUserService
    {
        Task<BaseModel> BlockAsync(UserModelItem userModel);
        Task<BaseModel> UnBlockAsync(UserModelItem userModel);
        Task<BaseModel> DeleteAsync(UserModelItem userModel);
        Task<UserModel> GetFilteredAsync(UserFilterModel userFilterModel);
        Task<BaseModel> UpdateAsync(UserModelItem model);
    }
}
