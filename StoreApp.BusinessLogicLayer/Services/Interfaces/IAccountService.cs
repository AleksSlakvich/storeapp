﻿using StoreApp.BusinessLogicLayer.Models.Account;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.User;
using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Services.Interfaces
{
    public interface IAccountService
    {
        Task<UserModelItem> SignInAsync(LogInModel model);
        Task<UserModelItem> GetByIdAsync(long id);
        Task<UserModelItem> SignUpAsync(RegistrationModel model);
        Task<BaseModel> ConfirmEmailAsync(long userId, string code);
        Task<string> GenerateEmailConfirmationTokenAsync(UserModelItem model);
        Task<BaseModel> ForgotPasswordAsync(ResetPasswordModel model);
        Task<BaseModel> SignOutAsync();
        Task SendConfirmEmailAsync(string email, string callbackUrl);
    }
}
