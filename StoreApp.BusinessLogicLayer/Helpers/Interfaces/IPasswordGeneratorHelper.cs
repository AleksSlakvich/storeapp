﻿namespace StoreApp.BusinessLogicLayer.Helpers
{
    public interface IPasswordGeneratorHelper
    {
        string GenerateRandomPassword();
    }
}
