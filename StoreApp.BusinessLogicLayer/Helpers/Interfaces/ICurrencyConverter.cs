﻿namespace StoreApp.BusinessLogicLayer.Helpers.Interfaces
{
    public interface ICurrencyConverter
    {
        decimal ConvertCurrency(Models.Enumes.Enumes.Currency currentCurrency, Models.Enumes.Enumes.Currency returnCurrency, decimal resultPrice);
    }
}
