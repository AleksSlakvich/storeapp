﻿using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Helpers
{
    public interface IEmailHelper
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
