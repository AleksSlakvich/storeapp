﻿using Microsoft.Extensions.Options;
using StoreApp.BusinessLogicLayer.Common.Options;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace StoreApp.BusinessLogicLayer.Helpers
{
    public class EmailHelper : IEmailHelper
    {
        private readonly EmailOptions _emailSettings;
        public EmailHelper(IOptions<EmailOptions> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var credentials = new NetworkCredential(_emailSettings.Sender, _emailSettings.Password);
            var emailMessage = new MailMessage()
            {
                From = new MailAddress(_emailSettings.Sender, _emailSettings.SenderName),
                Subject = subject,
                Body = message,
                IsBodyHtml = true
            };
            emailMessage.To.Add(new MailAddress(email));

            using (var client = new System.Net.Mail.SmtpClient()
            {
                Port = _emailSettings.MailPort,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = _emailSettings.MailServer,
                EnableSsl = true,
                Credentials = credentials
            }) 
            {
                await client.SendMailAsync(emailMessage);
            }
        }
    }
}
