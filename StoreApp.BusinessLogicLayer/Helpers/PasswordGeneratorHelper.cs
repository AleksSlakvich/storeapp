﻿using Microsoft.Extensions.Options;
using StoreApp.BusinessLogicLayer.Common.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using static StoreApp.BusinessLogicLayer.Models.Constants.Constants;

namespace StoreApp.BusinessLogicLayer.Helpers
{
    public class PasswordGeneratorHelper: IPasswordGeneratorHelper
    {
        private readonly PasswordGeneratorOptions _passwordGeneratorSettings;
        public PasswordGeneratorHelper(IOptions<PasswordGeneratorOptions> passwordGeneratorSettings)
        {
            _passwordGeneratorSettings = passwordGeneratorSettings.Value;
        }
        public string GenerateRandomPassword()
        { 
            string[] randomChars = new[] {
                RandomChars.UpperCase,    
                RandomChars.LowerCase,    
                RandomChars.Digits,                   
                RandomChars.NonAlphanumeric                      
                };

            var rand = new Random(Environment.TickCount);
            var chars = new List<char>();

            if (_passwordGeneratorSettings.RequireUppercase)
            {
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);
            }

            if (_passwordGeneratorSettings.RequireLowercase)
            {
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);
            }

            if (_passwordGeneratorSettings.RequireDigit)
            {
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);
            }

            if (_passwordGeneratorSettings.RequireNonAlphanumeric)
            {
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);
            }

            for (int i = chars.Count; i < _passwordGeneratorSettings.RequiredLength
                || chars.Distinct().Count() < _passwordGeneratorSettings.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }
            return new string(chars.ToArray());
        }
    }
}
