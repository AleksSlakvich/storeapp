﻿using StoreApp.BusinessLogicLayer.Helpers.Interfaces;
using StoreApp.BusinessLogicLayer.Models.Constants;
using StoreApp.DataAccessLayer.Entities.Enums;

namespace StoreApp.BusinessLogicLayer.Helpers
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public decimal ConvertCurrency (Models.Enumes.Enumes.Currency currentCurrency, Models.Enumes.Enumes.Currency returnCurrency,
                                        decimal resultPrice)
        {
            decimal currentValue = 0;
            decimal returnValue = 0;
            foreach(var currencyRate in Constants.CurrencyRates.CurrencyConverterDictionary)
            {
                if (currencyRate.Key == (Enums.Currency)currentCurrency)
                {
                    currentValue = currencyRate.Value;
                }

                if (currencyRate.Key == (Enums.Currency)returnCurrency)
                {
                    returnValue = currencyRate.Value;
                }
            }
            return currentValue / returnValue * resultPrice;
        }
    }
}
