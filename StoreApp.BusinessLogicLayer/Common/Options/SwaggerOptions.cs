﻿namespace StoreApp.BusinessLogicLayer.Common.Options
{
    public class SwaggerOptions
    {
        public string SwaggerVersion { get; set; }
        public string SwaggerTitle { get; set; }
        public string SwaggerDescription { get; set; }
        public string SwaggerTermsOfService { get; set; }
        public string SwaggerContactName { get; set; }
        public string SwaggerContactEmail { get; set; }
        public string SwaggerContactUrl { get; set; }
        public string SwaggerLicenseName { get; set; }
        public string SwaggerLicenseUrl { get; set; }
        public string SwaggerUIUrl { get; set; }
        public string SwaggerUIName { get; set; }
    }
}
