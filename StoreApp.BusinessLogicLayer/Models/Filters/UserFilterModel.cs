﻿using StoreApp.BusinessLogicLayer.Filters.BaseModel;
using StoreApp.BusinessLogicLayer.Models.Enumes;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Filters
{
    public class UserFilterModel : BaseFilterModel
    {
        public Enumes.SortType SortType { get; set; }
        public Enumes.ColumnType SortColumn { get; set; }
        public List<Enumes.UserStatus> Statuses { get; set; }

        public UserFilterModel(Enumes.SortType sortType, List<Enumes.UserStatus> userStatusSort, Enumes.ColumnType sortColumn, string currentFilter,
                               int pageSize, int pageIndex) : base(currentFilter, pageSize, pageIndex)
        {
            SortColumn = sortColumn;
            Statuses = userStatusSort;
            SortType = sortType;
        }
    }
}
