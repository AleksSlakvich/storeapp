﻿using StoreApp.BusinessLogicLayer.Filters.BaseModel;
using StoreApp.BusinessLogicLayer.Models.Enumes;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Filters
{
    public class OrderFilterModel : BaseFilterModel
    {
        public Enumes.SortType SortType { get; set; }
        public Enumes.ColumnType SortColumn { get; set; }
        public List<Enumes.OrderStatus> OrderStatuses { get; set; }

        public OrderFilterModel(string searchString, int pageSize, int pageIndex, List<Enumes.OrderStatus> orderStatusesSort,
                                Enumes.SortType sortType, Enumes.ColumnType columnType) : base(searchString, pageSize, pageIndex)
        {
            OrderStatuses = orderStatusesSort;
            SortColumn = columnType;
            SortType = sortType;
        }
    }
}
