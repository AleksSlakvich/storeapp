﻿using StoreApp.BusinessLogicLayer.Filters.BaseModel;
using StoreApp.BusinessLogicLayer.Models.Enumes;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Filters
{
    public class PrintingEditionFilterModel : BaseFilterModel
    {
        public List<Enumes.ProductType> Categories { get; set; }
        public Enumes.SortType SortType { get; set; }
        public Enumes.ColumnType SortColumn { get; set; }
        public decimal SortByMinPrice { get; set; }
        public decimal SortByMaxPrice { get; set; }
        public Enumes.Currency SortCurrency { get; set; }

        public PrintingEditionFilterModel(string searchString, int pageSize, int pageIndex, Enumes.SortType sortType, Enumes.Currency currencySort,
                                          Enumes.ColumnType sortColumn, List<Enumes.ProductType> printingEditionCategoriesSort = null,
                                          long printingEditionMinPrice = 0, long printingEditionMaxPrice = 0) : base(searchString, pageSize, pageIndex)
        {
            SortType = sortType;
            SortColumn = sortColumn;
            SortCurrency = currencySort;
            Categories = printingEditionCategoriesSort;
            SortByMinPrice = printingEditionMinPrice;
            SortByMaxPrice = printingEditionMaxPrice;
        }
    }
}
