﻿namespace StoreApp.BusinessLogicLayer.Filters.BaseModel
{
    public class BaseFilterModel
    {
        public string SearchString { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }

        public BaseFilterModel(string searchString, int pageSize, int pageIndex)
        {
            SearchString = searchString;
            PageSize = pageSize;
            PageIndex = pageIndex;
        }
    }
}
