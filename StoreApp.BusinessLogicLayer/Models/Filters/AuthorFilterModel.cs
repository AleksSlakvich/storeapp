﻿using StoreApp.BusinessLogicLayer.Filters.BaseModel;
using StoreApp.DataAccessLayer.Entities.Enums;

namespace StoreApp.BusinessLogicLayer.Filters
{
    public class AuthorFilterModel : BaseFilterModel
    {
        public Models.Enumes.Enumes.SortType SortType { get; set; }
        public Models.Enumes.Enumes.ColumnType SortColumn { get; set; }
        public AuthorFilterModel(string searchString, int pageSize, int pageIndex, Enums.SortType sortType,
                                 Enums.ColumnType sortColumn) : base(searchString, pageSize, pageIndex)
        {

            SortType = (Models.Enumes.Enumes.SortType)sortType;
            SortColumn = (Models.Enumes.Enumes.ColumnType)sortColumn;
        }
    }
}
