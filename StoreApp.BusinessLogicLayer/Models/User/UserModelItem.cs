﻿using StoreApp.BusinessLogicLayer.Models.Base;

namespace StoreApp.BusinessLogicLayer.Models.User
{
    public class UserModelItem : BaseModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public bool Status { get; set; }
        public string Role { get; set; }
        public string Image { get; set; }
        public UserModelItem()
        {
        }

        public UserModelItem(string firstName, string lastName, string email, bool userStatus)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Status = userStatus;
        }
    }
}
