﻿using StoreApp.BusinessLogicLayer.Models.Base;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.User
{
    public class UserModel : BaseModel
    {
        public ICollection<UserModelItem> Items = new List<UserModelItem>();
        public long Count { get; set; }
    }
}
