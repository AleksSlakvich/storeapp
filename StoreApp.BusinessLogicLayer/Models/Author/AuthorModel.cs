﻿using StoreApp.BusinessLogicLayer.Models.Base;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.Author
{
    public class AuthorModel : BaseModel
    {
        public ICollection<AuthorModelItem> Items = new List<AuthorModelItem>();
        public long Count { get; set; }
    }
}
