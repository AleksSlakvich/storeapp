﻿using StoreApp.BusinessLogicLayer.Models.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StoreApp.BusinessLogicLayer.Models.Author
{
    public class AuthorModelItem: BaseModel
    {
        public long AuthorId { get; set; }

        [Required]
        public string AuthorName { get; set; }
        public List<string> PrintingEditionTitles { get; set; }

        public AuthorModelItem()
        {
                
        }

        public AuthorModelItem(string name)
        {
            AuthorName = name;
        }

        public AuthorModelItem(long authorId,string name)
        {
            AuthorId = authorId;
            AuthorName = name;
        }
    }

}
