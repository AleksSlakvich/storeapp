﻿namespace StoreApp.BusinessLogicLayer.Models.Response
{
    public class ResponseDataModel<T> where T : class
    {
        public T Data { get; set; }
        public int ItemsCount { get; set; }
    }
}
