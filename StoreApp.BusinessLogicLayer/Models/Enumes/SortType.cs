﻿namespace StoreApp.BusinessLogicLayer.Models.Enumes
{
    public partial class Enumes
    {
        public enum SortType
        {
            ASC = 1,
            DESC = 2
        }
    }
}
