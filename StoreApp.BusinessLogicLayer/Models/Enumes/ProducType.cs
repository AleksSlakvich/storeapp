﻿namespace StoreApp.BusinessLogicLayer.Models.Enumes
{
    public partial class Enumes
    {
        public enum ProductType
        {
            Book = 1,
            Journal = 2,
            Newspaper = 3
        }
    }
}
