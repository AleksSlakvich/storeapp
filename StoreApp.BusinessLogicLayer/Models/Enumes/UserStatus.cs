﻿namespace StoreApp.BusinessLogicLayer.Models.Enumes
{
    public partial class Enumes
    {
        public enum UserStatus
        {
            Active = 1,
            Blocked = 2
        }
    }
}
