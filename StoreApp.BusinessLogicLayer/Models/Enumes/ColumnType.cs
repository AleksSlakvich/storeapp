﻿namespace StoreApp.BusinessLogicLayer.Models.Enumes
{
    public partial class Enumes
    {
        public enum ColumnType
        {
            None = 0,
            Id = 1,
            Name = 2,
            Email = 3,
            Price = 4,
            OrderAmount = 5,
            CreationData = 6,
            Title = 7,
            LastName = 8
        }
    }
}
