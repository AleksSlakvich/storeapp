﻿namespace StoreApp.BusinessLogicLayer.Models.Enumes
{
    public partial class Enumes
    {
        public enum OrderStatus
        {
            Paid = 1,
            Unpaid = 2
        }
    }
}
