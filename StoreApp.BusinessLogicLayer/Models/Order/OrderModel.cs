﻿using StoreApp.BusinessLogicLayer.Models.Base;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.Orders
{
    public class OrderModel:BaseModel
    {
        public ICollection<OrderModelItem> Items = new List<OrderModelItem>();
        public long Count { get; set; }
    }
}
