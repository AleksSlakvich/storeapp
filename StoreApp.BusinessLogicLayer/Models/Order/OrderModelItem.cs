﻿using StoreApp.BusinessLogicLayer.Models.Base;
using System;

namespace StoreApp.BusinessLogicLayer.Models.Orders
{
    public class OrderModelItem:BaseModel
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public OrderItemModel OrderItemModel { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreationDate { get; set; }
        public Enumes.Enumes.OrderStatus Status { get; set; }
    }
}
