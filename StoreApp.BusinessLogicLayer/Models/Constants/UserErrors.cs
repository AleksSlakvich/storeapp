﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class UserErrors
        {
            public const string EmptyEmail = "Empty Email";
            public const string EngagedEmail = "Email engaged";
            public const string EmptyPassword = "Empty password";
            public const string NotFind = "User not find";
            public const string NotSignIn = "User not SignIn";
            public const string NotSignUp = "User not SignUp";
            public const string NotSignOut = "User not SignOut";
            public const string NotDelete = "User not delete";
            public const string NotUpdate = "User not update";
            public const string NotBlock = "Not block User";
            public const string NotUnBlock = "Not unblock User";
            public const string DeletedUser = "User deleted in DB";
            public const string IncorrectPassword = "Incorrect password";
            public const string IncorrectUserId = "Incorrect UserId";
            public const string IncorrectCode = "Incorrect Code";
            public const string EmailNotConfirmed = "Email not confirmed";
            public const string PasswordNotChanged = "Password not changed";
            public const string ExistedUser = "User exist in DB";
        }
    }
}
