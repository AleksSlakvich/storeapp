﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class UserRoles
        {
            public const string Admin = "Admin";
            public const string User = "User";
        }
    }
}
