﻿using StoreApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class CurrencyRates
        {
            public const decimal USDtoUSD = 1m;
            public const decimal EURtoUSD = 1.11105m;
            public const decimal GBPtoUSD = 1.28207m;
            public const decimal CHFtoUSD = 1.00836m;
            public const decimal JPYtoUSD = 0.00921m;
            public const decimal UAHtoUSD = 0.039748m;

            public static readonly Dictionary<Enums.Currency, decimal> CurrencyConverterDictionary = new Dictionary<Enums.Currency, decimal>()
            {
                {Enums.Currency.USD, USDtoUSD },
                {Enums.Currency.EUR, EURtoUSD },
                {Enums.Currency.GBP, GBPtoUSD },
                {Enums.Currency.CHF, CHFtoUSD },
                {Enums.Currency.JPY, JPYtoUSD },
                {Enums.Currency.UAH, UAHtoUSD }
            };
        }
    }
}
