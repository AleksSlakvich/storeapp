﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class BaseErrors
        {
            public const string EmptyDataError = "Any data";
            public const string LogWarning = "The response has already started, the http status code middleware will not be executed.";
        }
    }
}
