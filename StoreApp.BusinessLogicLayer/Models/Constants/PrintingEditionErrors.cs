﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class PrintingEditionErrors
        {
            public const string NotCreate = "Not create printingEdition";
            public const string NotDelete = "Not delete printingEdition";
            public const string NotUpdate = "Not update printingEdition";
            public const string NotFind = "Not find printingEdition";
            public const string NotFound = "Not found printingEditions";
            public const string DeletedPI = "Printing Edition is deleted";
            public const string NotDeleteAIP = "Not delete authorInPrintingEdition";
            public const string NotCreateAIP = "Not create authorInPrintingEdition";
        }
    }
}
