﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class EmailMessage
        {
            public const string PasswordChanged = "Your password is changed";
            public const string NewPassword = "Hello! Your new password  -";
            public const string ConfirmAcc = "Hello! Confirm your account";
            public const string CompleteReg = "To complete registration, Please, go --->> :";
        }
    }
}
