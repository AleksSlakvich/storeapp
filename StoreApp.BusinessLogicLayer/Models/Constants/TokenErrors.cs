﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class TokenErrors
        {
            public const string NotValidTokens = "You SignOut. Tokens not generated. Please Login again";
        }
    }
}
