﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class RandomChars
        {
            public const string UpperCase = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            public const string LowerCase = "abcdefghijkmnopqrstuvwxyz";
            public const string Digits = "0123456789";
            public const string NonAlphanumeric = "!@$?_-";
        }
    }
}
