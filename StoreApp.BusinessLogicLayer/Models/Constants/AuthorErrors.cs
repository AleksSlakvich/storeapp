﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class AuthorErrors
        {
            public const string NotCreate = "Not create author";
            public const string NotDelete = "Not delete author";
            public const string NotUpdate = "Not update author";
            public const string NotFoundAuthors = "Not found authors";
            public const string DeletedAuthor = "Author deleted";
            public const string NotFoundAuthor = "Not found author";
        }
    }
}
