﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class OrderErrors
        {
            public const string NotCreatePay = "Payment not create";
            public const string NotCreateOrder = "Order not create";
            public const string NotFindOrder = "Order not find";
            public const string NotCreateOrderItem = "OrderItem not create";
            public const string NotValidTrans = "Transaction not valid";
            public const string NotFoundOrders = "Orders not found";
        }
    }
}
