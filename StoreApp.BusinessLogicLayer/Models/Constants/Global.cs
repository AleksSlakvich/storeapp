﻿namespace StoreApp.BusinessLogicLayer.Models.Constants
{
    public partial class Constants
    {
        public class Global
        {
            public const string AccessToken = "AccessToken";
            public const string RefreshToken = "RefreshToken";
            public const string ConfirmEmail = "ConfirmEmail";
            public const string Account = "Account";
        }
    }
}
