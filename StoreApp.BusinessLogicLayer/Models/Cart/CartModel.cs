﻿using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.Cart
{
    public class CartModel
    {
        public ICollection<CartModelItem> Items = new List<CartModelItem>();
        public string TransactionId { get; set; }
        public string OrderDescription { get; set; }
        public long UserId { get; set; }
        public Enumes.Enumes.OrderStatus OrderStatus { get; set; }
    }
}
