﻿namespace StoreApp.BusinessLogicLayer.Models.Cart
{
    public class CartModelItem
    {
        public long PrintingEditionId { get; set; }
        public long Quantity { get; set; }
        public Enumes.Enumes.Currency Currency { get; set; }
        public string PrintingEditionTitle { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }

        public CartModelItem(long printingEditionId, long quantity, Enumes.Enumes.Currency currency, string printingEditionTitle, decimal price, decimal amount)
        {
            PrintingEditionId = printingEditionId;
            Quantity = quantity;
            Currency = currency;
            PrintingEditionTitle = printingEditionTitle;
            Price = price;
            Amount = amount;
        }
    }
}
