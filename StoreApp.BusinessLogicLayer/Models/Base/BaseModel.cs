﻿using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.Base
{
    public class BaseModel
    {
        public ICollection<string> Errors = new List<string>();
    }
}
