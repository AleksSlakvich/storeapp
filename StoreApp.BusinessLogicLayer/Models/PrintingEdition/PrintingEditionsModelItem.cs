﻿using StoreApp.BusinessLogicLayer.Models.Author;
using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StoreApp.BusinessLogicLayer.Models.PrintingEditions
{
    public class PrintingEditionModelItem : BaseModel
    {
        public long Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public Enumes.Enumes.ProductType Category { get; set; }
        [Required]
        public Enumes.Enumes.Currency Currency { get; set; }
        [Required]
        public AuthorModel Authors { get; set; } 
        public List<string> AuthorNames { get; set; }

        public PrintingEditionModelItem()
        {

        }

        public PrintingEditionModelItem(string title, string description, decimal price, Enums.ProductType category, Enums.Currency currency,
                                        AuthorModel authors)
        {
            Title = title;
            Description = description;
            Price = price;
            Category = (Enumes.Enumes.ProductType)category;
            Currency = (Enumes.Enumes.Currency)currency;
            Authors = authors;
        }
    }
}
