﻿using StoreApp.BusinessLogicLayer.Models.Base;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.PrintingEditions
{
    public class PrintingEditionModel:BaseModel
    {
        public ICollection<PrintingEditionModelItem> Items = new List<PrintingEditionModelItem>();
        public long Count { get; set; }
    }
}
