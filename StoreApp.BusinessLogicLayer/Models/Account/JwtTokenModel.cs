﻿using StoreApp.BusinessLogicLayer.Models.Base;

namespace StoreApp.BusinessLogicLayer.Models.Account
{
    public class JwtTokenModel : BaseModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
