﻿using System.ComponentModel.DataAnnotations;

namespace StoreApp.BusinessLogicLayer.Models.Account
{
    public class ResetPasswordModel
    {
        [Required]
        public string Email { get; set; }
    }
}
