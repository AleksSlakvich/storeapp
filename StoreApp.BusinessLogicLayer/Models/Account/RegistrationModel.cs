﻿using System.ComponentModel.DataAnnotations;

namespace StoreApp.BusinessLogicLayer.Models.Account
{
    public class RegistrationModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string Image { get; set; }
    }
}
