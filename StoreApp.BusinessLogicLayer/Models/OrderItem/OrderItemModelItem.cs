﻿using StoreApp.BusinessLogicLayer.Models.Base;
using StoreApp.BusinessLogicLayer.Models.PrintingEditions;

namespace StoreApp.BusinessLogicLayer.Models.Orders
{
    public class OrderItemModelItem:BaseModel
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long Count { get; set; }
        public decimal Amount { get; set; }
        public PrintingEditionModelItem PrintingEdition { get; set; }

        public OrderItemModelItem()
        {

        }

        public OrderItemModelItem(long orderId, long quantity, PrintingEditionModelItem printingEdition)
        {
            OrderId = orderId;
            Count = quantity;
            PrintingEdition = printingEdition;
            Amount = PrintingEdition.Price * quantity;
        }
    }
}
