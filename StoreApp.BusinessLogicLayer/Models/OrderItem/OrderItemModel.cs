﻿using StoreApp.BusinessLogicLayer.Models.Base;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Models.Orders
{
    public class OrderItemModel:BaseModel
    {
        public ICollection<OrderItemModelItem> Items = new List<OrderItemModelItem>();
    }
}
