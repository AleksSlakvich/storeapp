﻿using Microsoft.AspNetCore.Identity;
using StoreApp.BusinessLogicLayer.Services;
using StoreApp.BusinessLogicLayer.Services.Interfaces;
using StoreApp.DataAccessLayer.AppContext;
using StoreApp.DataAccessLayer.Entities;
using System;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using StoreApp.DataAccessLayer.Initialization;
using Microsoft.Extensions.Configuration;
using StoreApp.BusinessLogicLayer.Helpers;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using StoreApp.BusinessLogicLayer.Helpers.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;

namespace StoreApp.BusinessLogicLayer.Initialize
{
    public static class Initializer
    {
        public static void Init(IServiceCollection services, IConfiguration Configuration)
        {

            services.AddDbContext<ApplicationContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            RepositoryInitializer.InitializeEF(services);
            services.AddIdentity<ApplicationUser, IdentityRole<long>>()
             .AddEntityFrameworkStores<ApplicationContext>()
             .AddDefaultTokenProviders();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero
                    };
                })
                .AddGoogle(options =>
                {
                    options.ClientId = Configuration["Authentication:Google:ClientId"];
                    options.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                })
                .AddFacebook(facebookOptions =>
                {
                    facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                    facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                }); 


            services.AddScoped<DataBaseInitialization>();

            services.AddScoped<IEmailHelper, EmailHelper>();
            services.AddScoped<IPasswordGeneratorHelper, PasswordGeneratorHelper>();
            services.AddScoped<ICurrencyConverter, CurrencyConverter>();

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IPrintingEditionService, PrintingEditionService>();
            services.AddScoped<IOrderService, OrderService>();
        }
    }
}
