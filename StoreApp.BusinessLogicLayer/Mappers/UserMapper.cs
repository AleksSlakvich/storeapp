﻿using StoreApp.BusinessLogicLayer.Models.Account;
using StoreApp.BusinessLogicLayer.Models.User;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.BusinessLogicLayer.Models.Constants;
using StoreApp.BusinessLogicLayer.Extensions.Image;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class UserMapper
        {
            public static UserModelItem MapEntityToUserModelItem(ApplicationUser user)
            {
                var model = new UserModelItem();
                model.Id = user.Id;
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
                model.Email = user.Email;
                model.Status = false;
                model.Role = Constants.UserRoles.User;
                model.Image = System.Text.Encoding.UTF8.DecodeBase64(user.Image);
                if (user.Status == Enums.UserStatus.Active)
                {
                    model.Status = true;
                }
                return model;
            }

            public static ApplicationUser MapUserModelItemToApplicationUser(UserModelItem modelItem)
            {
                var user = new ApplicationUser();
                user.Id = modelItem.Id;
                user.FirstName = modelItem.FirstName;
                user.LastName = modelItem.LastName;
                user.Email = modelItem.Email;
                user.UserName = modelItem.Email;
                user.Status = Enums.UserStatus.Active;
                if (!modelItem.Status)
                {
                    user.Status = Enums.UserStatus.Blocked;
                }
                user.Image = System.Text.Encoding.UTF8.EncodeBase64(modelItem.Image);
                return user;
            }

            public static ApplicationUser MapRegistrationModelToApplicationUser(RegistrationModel modelItem)
            {
                var user = new ApplicationUser();
                user.FirstName = modelItem.FirstName;
                user.LastName = modelItem.LastName;
                user.Email = modelItem.Email;
                user.UserName = modelItem.Email;
                user.Status = Enums.UserStatus.Active;
                user.Image = System.Text.Encoding.UTF8.EncodeBase64(modelItem.Image);
                return user;
            }
            public static ApplicationUser MapModelToExistedUser(ApplicationUser existedUser, UserModelItem model)
            {
                existedUser.FirstName = model.FirstName;
                existedUser.LastName = model.LastName;
                existedUser.Email = model.Email;
                existedUser.Status = Enums.UserStatus.Active;
                existedUser.Image = System.Text.Encoding.UTF8.EncodeBase64(model.Image);
                return existedUser;
            }
        }
    }
}
