﻿using StoreApp.BusinessLogicLayer.Models.Orders;
using StoreApp.DataAccessLayer.Entities;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class OrderItemMapper
        {
            public static OrderItemModelItem MapEntityToOrderItemModelItem(OrderItem orderItem)
            {
                var model = new OrderItemModelItem();
                model.Id = orderItem.Id;
                model.OrderId = orderItem.Order.Id;
                model.Count = orderItem.Count;
                model.Amount = orderItem.Amount;
                model.PrintingEdition = PrintingEditionMapper.MapEntityWithAuthorsModelToPrintingEditionModelItem(orderItem.PrintingEdition);
                return model;
            }
        }
    }
}
