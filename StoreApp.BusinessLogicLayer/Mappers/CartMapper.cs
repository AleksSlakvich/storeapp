﻿using StoreApp.BusinessLogicLayer.Models.Cart;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using System;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class CartMapper
        {
            public static Payment MapCartModelToPayment(CartModel model)
            {
                var payment = new Payment();
                payment.TransactionId = model.TransactionId;
                return payment;
            }
            
            public static Order MapCartModelToOrder(CartModel model)
            {
                var order = new Order();
                order.Description = model.OrderDescription;
                order.CreationData = DateTime.UtcNow;
                order.Status = (Enums.OrderStatus)model.OrderStatus;
                return order;
            }

            public static OrderItem MapCartModelItemToOrderItem(CartModelItem modelItem)
            {
                var orderItem = new OrderItem();
                orderItem.Count = modelItem.Quantity;
                orderItem.CreationData = DateTime.UtcNow;
                orderItem.Currency = (Enums.Currency)modelItem.Currency;
                return orderItem;
            }
        }
    }
}
