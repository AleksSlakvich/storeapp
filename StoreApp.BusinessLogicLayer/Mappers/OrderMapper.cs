﻿using StoreApp.BusinessLogicLayer.Models.Orders;
using StoreApp.DataAccessLayer.Entities;
using System;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class OrderMapper
        {
            public static OrderModelItem MapEntityToOrderModelItem(Order order)
            {
                var model = new OrderModelItem();
                model.Id = order.Id;
                model.UserEmail = order.User.Email;
                model.UserName = $"{order.User.FirstName} {order.User.LastName}";
                model.Amount = order.OrderAmount;
                model.CreationDate = order.CreationData;
                model.Status = (Models.Enumes.Enumes.OrderStatus)order.Status;
                model.OrderItemModel = new OrderItemModel();
                return model;
            }

            public static Order MapOrderModelItemToOrder(OrderModelItem modelItem)
            {
                var order = new Order();
                order.Id = modelItem.Id;
                order.CreationData = DateTime.UtcNow;
                return order;
            }
        }
    }
}
