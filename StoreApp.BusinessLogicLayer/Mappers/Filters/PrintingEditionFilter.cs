﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models;
using System.Collections.Generic;
using static StoreApp.BusinessLogicLayer.Mappers.Mappers;

namespace StoreApp.BusinessLogicLayer.Mappers.Filters
{
    public class PrintingEditionFilter
    {
        public static FilterPrintingEditionModel MapPrintingEditionFilterModelToFilterPrintingEdition(PrintingEditionFilterModel
                                                                                                      printingEditionFilterModel)
        {
            var model = new FilterPrintingEditionModel();
            model.PageIndex = printingEditionFilterModel.PageIndex;
            model.PageSize = printingEditionFilterModel.PageSize;
            model.SearchFilter = printingEditionFilterModel.SearchString;
            model.PrintingEditionCategoriesSort = CategoriesMapper.EnumesProductTypeBLToEnumsProductTypeDA(printingEditionFilterModel.Categories);
            model.SortColumn = (Enums.ColumnType)printingEditionFilterModel.SortColumn;
            model.SortType = (Enums.SortType)printingEditionFilterModel.SortType;
            model.PrintingEditionMinPrice = printingEditionFilterModel.SortByMinPrice;
            model.PrintingEditionMaxPrice = printingEditionFilterModel.SortByMaxPrice;
            return model;
        }
    }
}
