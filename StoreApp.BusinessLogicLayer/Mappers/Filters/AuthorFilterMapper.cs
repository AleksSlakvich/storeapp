﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models;

namespace StoreApp.BusinessLogicLayer.Mappers.Filters
{
    public class AuthorFilterMapper
    {
        public static FilterAuthorModel MapAuthorFilterModelToFilterAuthor(AuthorFilterModel authorFilterModel)
        {
            var model = new FilterAuthorModel();
            model.PageIndex = authorFilterModel.PageIndex;
            model.PageSize = authorFilterModel.PageSize;
            model.SearchFilter = authorFilterModel.SearchString;
            model.SortType = (Enums.SortType)authorFilterModel.SortType;
            model.SortColumn = (Enums.ColumnType)authorFilterModel.SortColumn;
            return model;
        }
    }
}
