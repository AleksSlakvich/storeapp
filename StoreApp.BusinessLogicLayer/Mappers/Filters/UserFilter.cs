﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models;
using static StoreApp.BusinessLogicLayer.Mappers.Mappers;

namespace StoreApp.BusinessLogicLayer.Mappers.Filters
{
    public class UserFilter
    {
        public static FilterUserModel MapUserFilterModelToFilterUser(UserFilterModel userFilterModel)
        {
            var model = new FilterUserModel();
            model.PageIndex = userFilterModel.PageIndex;
            model.PageSize = userFilterModel.PageSize;
            model.SearchFilter = userFilterModel.SearchString;
            model.UserStatusSort = UserStatusesMapper.EnumesUserStatusBLToEnumsUserStatusDA(userFilterModel.Statuses);
            model.SortType = (Enums.SortType)userFilterModel.SortType;
            model.SortColumn = (Enums.ColumnType)userFilterModel.SortColumn;
            return model;
        }
    }
}
