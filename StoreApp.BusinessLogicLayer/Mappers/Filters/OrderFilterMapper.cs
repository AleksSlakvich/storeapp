﻿using StoreApp.BusinessLogicLayer.Filters;
using StoreApp.DataAccessLayer.Entities.Enums;
using StoreApp.DataAccessLayer.Models;
using static StoreApp.BusinessLogicLayer.Mappers.Mappers;

namespace StoreApp.BusinessLogicLayer.Mappers.Filters
{
    public class OrderFilterMapper
    {
        public static FilterOrderModel MapOrderFilterModelToFilterOrder(OrderFilterModel orderFilterModel)
        {
            var model = new FilterOrderModel();
            model.PageIndex = orderFilterModel.PageIndex;
            model.PageSize = orderFilterModel.PageSize;
            model.SearchFilter = orderFilterModel.SearchString;
            model.SortColumn = (Enums.ColumnType)orderFilterModel.SortColumn;
            model.SortType = (Enums.SortType)orderFilterModel.SortType;
            model.OrderStatusSort = OrderStatusesMapper.EnumesOrderStatusBLToEnumsOrderStatusDA(orderFilterModel.OrderStatuses);
            return model;
        }
    }
}
