﻿using StoreApp.BusinessLogicLayer.Models.Response;
using StoreApp.DataAccessLayer.Models.Response;

namespace StoreApp.BusinessLogicLayer.Mappers.Filters
{
    public partial class Mappers
    {
        public class ResponseDataMapper<T> where T : class
        {
            public static ResponseDataModel<T> MapResponseModelToResponseDataModel(ResponseModel<T> responseModel)
            {
                ResponseDataModel<T> model = new ResponseDataModel<T>();
                model.Data = responseModel.Data;
                model.ItemsCount = responseModel.ItemsCount;
                return model;
            }

            public static ResponseModel<T> MapResponseDataModelToResponseModel(ResponseDataModel<T> responseModel)
            {
                ResponseModel<T> model = new ResponseModel<T>();
                model.Data = responseModel.Data;
                model.ItemsCount = responseModel.ItemsCount;
                return model;
            }
        }
    }
}
