﻿using StoreApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class UserStatusesMapper
        {
            public static List<Enums.UserStatus> EnumesUserStatusBLToEnumsUserStatusDA(List<Models.Enumes.Enumes.UserStatus> userStatuses)
            {
                var sortStatuses = new List<Enums.UserStatus>();
                foreach (var type in userStatuses)
                {
                    sortStatuses.Add((Enums.UserStatus)type);
                }
                return sortStatuses;
            }

            public static List<Models.Enumes.Enumes.UserStatus> EnumsUserStatusDAToEnumesUserStatusBL(List<Enums.UserStatus> userStatuses)
            {
                var sortStatuses = new List<Models.Enumes.Enumes.UserStatus>();
                foreach (var type in userStatuses)
                {
                    sortStatuses.Add((Models.Enumes.Enumes.UserStatus)type);
                }
                return sortStatuses;
            }
        }
    }
}
