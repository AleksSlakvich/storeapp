﻿using StoreApp.BusinessLogicLayer.Models.Author;
using StoreApp.BusinessLogicLayer.Models.Enumes;
using StoreApp.BusinessLogicLayer.Models.PrintingEditions;
using StoreApp.DataAccessLayer.Entities;
using StoreApp.DataAccessLayer.Entities.Enums;
using System;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class PrintingEditionMapper
        {
            public static PrintingEditionModelItem MapEntityWithAuthorsModelToPrintingEditionModelItem(PrintingEdition printingEdition,
                                                                                                       AuthorModel authorsModel = null)
            {
                var model = new PrintingEditionModelItem();
                model.Id = printingEdition.Id;
                model.Title = printingEdition.Title;
                model.Description = printingEdition.Description;
                model.Category = (Enumes.ProductType)printingEdition.Type;
                model.Currency = (Enumes.Currency)printingEdition.Currency;
                model.Price = printingEdition.Price;
                model.Authors = authorsModel;
                model.AuthorNames = printingEdition.AuthorNames;
                return model;
            }

            public static PrintingEdition MapPrintingEditionModelItemToEntity(PrintingEditionModelItem model)
            {
                var printingEdition = new PrintingEdition();
                printingEdition.Id = model.Id;
                printingEdition.Title = model.Title;
                printingEdition.Description = model.Description;
                printingEdition.Price = model.Price;
                printingEdition.Type = (Enums.ProductType)model.Category;
                printingEdition.Currency = Enums.Currency.USD;
                printingEdition.CreationData = DateTime.UtcNow;
                return printingEdition;
            }
        }
    }
}
