﻿using StoreApp.BusinessLogicLayer.Models.Author;
using StoreApp.DataAccessLayer.Entities;
using System;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class AuthorMapper
        {
            public static AuthorModelItem MapEntityToAuthorsModelItem(Author author)
            {
                var model = new AuthorModelItem();
                model.AuthorId = author.Id;
                model.AuthorName = author.Name;
                model.PrintingEditionTitles = author.ProductTitles;
                return model;
            }

            public static Author MapAuthorsModelItemToAuthor(AuthorModelItem modelItem)
            {
                var author = new Author();
                author.Id = modelItem.AuthorId;
                author.Name = modelItem.AuthorName;
                author.CreationData = DateTime.UtcNow;
                return author;
            }
        }
    }
}
