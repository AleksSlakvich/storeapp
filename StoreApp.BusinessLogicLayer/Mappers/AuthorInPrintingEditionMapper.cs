﻿using StoreApp.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class AuthorInPrintingEditionMapper
        {
            public static AuthorInPrintingEdition MapAuthorPrintingEditionToAuthorInPrintingEdition(PrintingEdition printingEdition,
                                                                                                    Author author)
            {
                var authorInPrintingEdition = new AuthorInPrintingEdition();
                authorInPrintingEdition.PrintingEdition = printingEdition;
                authorInPrintingEdition.Author = author;
                authorInPrintingEdition.PrintingEditionId = printingEdition.Id;
                authorInPrintingEdition.AuthorId = author.Id;
                authorInPrintingEdition.CreationData = DateTime.UtcNow;
                return authorInPrintingEdition;
            }
        }
    }
}
