﻿using StoreApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class CategoriesMapper
        {
            public static List<Enums.ProductType> EnumesProductTypeBLToEnumsProductTypeDA(List<Models.Enumes.Enumes.ProductType> productTypes)
            {
                var sortTypes = new List<Enums.ProductType>();
                foreach (var type in productTypes)
                {
                    sortTypes.Add((Enums.ProductType)type);
                }
                return sortTypes;
            }

            public static List<Models.Enumes.Enumes.ProductType> EnumsProductTypeDAToEnumesProductTypeBL(List<Enums.ProductType> productTypes)
            {
                var sortTypes = new List<Models.Enumes.Enumes.ProductType>();
                foreach (var type in productTypes)
                {
                    sortTypes.Add((Models.Enumes.Enumes.ProductType)type);
                }
                return sortTypes;
            }
        }
    }
}
