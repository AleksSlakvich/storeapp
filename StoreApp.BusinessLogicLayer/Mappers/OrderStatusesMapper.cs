﻿using StoreApp.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;

namespace StoreApp.BusinessLogicLayer.Mappers
{
    public partial class Mappers
    {
        public class OrderStatusesMapper
        {
            public static List<Enums.OrderStatus> EnumesOrderStatusBLToEnumsOrderStatusDA(List<Models.Enumes.Enumes.OrderStatus> orderStatuses)
            {
                var sortStatuses = new List<Enums.OrderStatus>();
                foreach (var type in orderStatuses)
                {
                    sortStatuses.Add((Enums.OrderStatus)type);
                }
                return sortStatuses;
            }

            public static List<Models.Enumes.Enumes.OrderStatus> EnumsOrderStatusDAToEnumesOrderStatusBL(List<Enums.OrderStatus> orderStatuses)
            {
                var sortStatuses = new List<Models.Enumes.Enumes.OrderStatus>();
                foreach (var type in orderStatuses)
                {
                    sortStatuses.Add((Models.Enumes.Enumes.OrderStatus)type);
                }
                return sortStatuses;
            }
        }
    }
}
