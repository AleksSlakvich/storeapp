﻿using Microsoft.Extensions.Logging;
using StoreApp.BusinessLogicLayer.Common;

namespace StoreApp.BusinessLogicLayer.Extensions
{
    public static class FileLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string filePath)
        {
            factory.AddProvider(new LoggerProvider(filePath));
            return factory;
        }
    }
}
