﻿using System;
using System.Text;

namespace StoreApp.BusinessLogicLayer.Extensions.Image
{
    public static partial class ImageConverterExtension
    {
        public static string EncodeBase64(this Encoding encoding, string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var textAsBytes = encoding.GetBytes(text);
            return Convert.ToBase64String(textAsBytes);
        }
    }
}
