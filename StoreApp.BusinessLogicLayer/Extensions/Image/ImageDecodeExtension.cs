﻿using System;
using System.Text;

namespace StoreApp.BusinessLogicLayer.Extensions.Image
{
    public static partial class ImageConverterExtension
    {
        public static string DecodeBase64(this Encoding encoding, string encodedText)
        {
            if (string.IsNullOrWhiteSpace(encodedText))
            {
                return null;
            }

            var textAsBytes = Convert.FromBase64String(encodedText);
            return encoding.GetString(textAsBytes);
        }
    }
}
